<?php
$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . strstr($_SERVER['HTTP_HOST'], ':', true) . $_SERVER['REQUEST_URI'];
$url = explode('?', $url);
$url = $url[0];

include_once(__DIR__ . '/crest.php');
include_once(__DIR__ . '/Service/CallTranscription.php');
$result = $_REQUEST;
file_put_contents(__DIR__ . '/logs/main_logs/file.txt', print_r($result, true), FILE_APPEND);
if ($_REQUEST['event'] == 'ONVOXIMPLANTCALLEND') {

    // $dir = $_SERVER['DOCUMENT_ROOT'] . __DIR__ . '/tmp/'; try this depending on your server configuration
    $dir = 'tmp/';
    file_put_contents(__DIR__ . '/logs/main_logs/file.txt', "\n\r EVENT - TRUE\n\r ", FILE_APPEND);
    if (!file_exists($dir)) {
        file_put_contents(__DIR__ . '/logs/main_logs/file.txt', "\n\r EVENT - ПАПКИ НЕ СУЩЕСТВУЕТ\n\r ", FILE_APPEND);
        mkdir($dir, 0777, true);
    }

    $file_id = CallTranscription::getFileInfo($_REQUEST['data']['CALL_ID']);
    $file_url = CallTranscription::getUrlFile($file_id);
    $call_id = $_REQUEST['data']['CALL_ID'];
    $crm_id = $_REQUEST['data']['CRM_ACTIVITY_ID'];
    $reason_id = $_REQUEST['data']['CALL_FAILED_REASON'];
    $start = $_REQUEST['data']['CALL_START_DATE'];
    $duration = $_REQUEST['data']['CALL_DURATION'];

    if(isset($_REQUEST['member_id'])){
        $member_id = $_REQUEST['member_id'];
    } elseif(isset($_REQUEST['auth']['member_id'])){
        $member_id = $_REQUEST['auth']['member_id'];
    }

    $ar_result = [
        'UF_CALL_ID' => $call_id,
        'UF_CRM_ACTIVITY_ID' => $crm_id,
        'UF_CALL_FAILED_REASON' => $reason_id,
        'UF_RECORD_FILE' => $file_url,
        'UF_START_DATETIME' => $start,
        'UF_UPDATED' => false,
        'UF_CALL_DURATION' => $duration,
        'UF_MEMBER_ID' => $member_id
    ];

    function incomingHook($url, $fields)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => $fields
        ));
        //curl_setopt(CURLOPT_REFERER, 'http://google.com');
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        $result = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);
        return $result;
    }
    function writeToLog($message)
    {
        file_put_contents(__DIR__ . '/logs/main_logs/file.txt', date("Y-m-d H:i:s") . "; " . print_r($message, true) . ' \n', FILE_APPEND);
        file_put_contents(__DIR__ . '/logs/main_logs/file.txt', '--------------------------------', FILE_APPEND);
    }

    //writeToLog($ar_result);
    $url = 'https://wisper.itcenter.pro/post_api/event.php';
    $curl_res = incomingHook($url, $ar_result);
    writeToLog($curl_res);
}
//}

?>