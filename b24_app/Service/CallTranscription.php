<?php
include_once(__DIR__.'/cextrest.php');

class CallTranscription
{
    /*---Получение истории звонков и фильтрация по ID - voximplant.statistic.get---*/
    public static function getFileInfo($call_id){
        $array_info = [];
        $static_req_fields = [
            'FILTER' => ["ID" => '=' . $call_id],
            'SORT' => 'ID',
            'ORDER' => 'DESC'
        ];

        $get_static_result = CRestExt::call(
            'voximplant.statistic.get',
            $static_req_fields
        );
        $result_array = $get_static_result->result;
        foreach ($result_array as $item){
            if($item->CALL_ID == $call_id){
                $result = $item->CRM_ENTITY_TYPE;
                array_push($array_info,$result);
                $result = $item->CRM_ENTITY_ID;
                array_push($array_info,$result);
                $result = $item->RECORD_FILE_ID;
                array_push($array_info,$result);
                //$result = $item;
            }
        }

        return $array_info;
    }

    /*---Получение ссылки на файл по id - disk.file.get---*/
    public static function getUrlFile($file_id){
        // поля запроса
        $get_file_fields = [
            'id' => $file_id
        ];
        // Вызов ф-и с запросом
        $url_audio_file_obj = CRestExt::call(
            'disk.file.get',
            $get_file_fields
        );
        $url_download_audio = $url_audio_file_obj->result->DOWNLOAD_URL;
        return $url_download_audio;
    }
}