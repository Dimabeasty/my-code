<?php
ini_set('display_errors', 1);

ini_set('display_startup_errors', 1);

error_reporting(E_ALL);

include_once(__DIR__ . '/crest.php');
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/app.css">
    <!--link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"-->
    <script
            src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous"></script>

    <style>
        .background-tint {
            background-color: rgba(255,255,255,.8);
            background-blend-mode: screen;
        }
    </style>

    <title>Index</title>
</head>
<body class="container-fluid box background-tint">
<!-- MAIN_CONTENT-->
<?php
//CRest::setCurrentBitrix24($_REQUEST['member_id']);
$result = CRest::call(
    'crm.lead.list',
    [
    ]
);
foreach ($result['result'] as $res){
    $id = $res['ID'];
    $name = $res['NAME'];
    $last_name = $res['LAST_NAME'];
    $last_activity = $res['LAST_ACTIVITY_TIME'];
    $company = $res['COMPANY_TITLE'];
    ?>
<div>
    <p><a>Name: <?= $name;?></a><a> <?= $last_name;?> | </a><a>Company :<?= $company;?> | </a><a>Last activity : <?= $last_activity;?> | </a></p>
    <br>
</div>
<?php
}
//echo '<pre>';
//print_r($result);
//echo '</pre>';

?>

    <!-- Link to your app's JavaScript libraries -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  </body>
</html>