<?php
ini_set('display_errors', 1);

ini_set('display_startup_errors', 1);

error_reporting(E_ALL);

require_once(__DIR__ . '/crest.php');
//chmod(__DIR__ . "/logs/main_logs/file.txt", 0755);
$install_result = CRest::installApp();
$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . strstr($_SERVER['HTTP_HOST'], ':', true);
$url = explode('?', $url);
$url = $url[0];
// embedded for placement "placement.php"
$handlerBackUrl = $url . '/test/handler.php';

$result = CRest::call(
	'event.bind',
	[
		'EVENT' => 'on.voximplant.end',
		'HANDLER' => $handlerBackUrl,
        'EVENT_TYPE' => 'online'
	]
);
file_put_contents(__DIR__ . '/logs/main_logs/file.txt', print_r($result, true), FILE_APPEND);
//CRestExt::setLog(['update' => $result], 'installation');.
if(isset($_REQUEST['member_id'])){
    $member_id = $_REQUEST['member_id'];
} elseif(isset($_REQUEST['auth']['member_id'])){
    $member_id = $_REQUEST['auth']['member_id'];
}
$ar_result = [
    'domain' => $_REQUEST['DOMAIN'],
    'member_id' => $member_id
];
function incomingHook($url, $fields)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_POSTFIELDS => $fields
    ));
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    $result = curl_exec($curl);
    $info = curl_getinfo($curl);
    curl_close($curl);
    return $result;
}
$url = 'https://wisper.itcenter.pro/post_api/new_install.php';

$curl_res = incomingHook($url, $ar_result);
if($install_result['rest_only'] === false):?>
<head>
	<script src="//api.bitrix24.com/api/v1/"></script>
	<?if($install_result['install'] == true):?>
	<script>
		BX24.init(function(){
			BX24.installFinish();
		});
	</script>
	<?endif;?>
</head>
<body>
	<?if($install_result['install'] == true):?>
		installation has been finished
	<?else:?>
		installation error
	<?endif;?>
</body>
<?endif;