<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule('highloadblock');
\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;
use Bitrix\Iblock;
Loader::IncludeModule("iblock");

class UserRegistrationComponent extends StandardElementListComponent {

    protected function checkParams()
    {}

    public function getResult()
    {
        global $USER;

        $rsUser = $USER->GetByLogin($USER->GetLogin());
        $arUser = $rsUser->Fetch();

        $ar_user_info['PASSWORD'] = $arUser['PASSWORD'];
        $ar_user_info['ID'] = $arUser['ID'];


        $this->arResult = [
            'LOGIN' => $USER->GetLogin(),
            'EMAIL' => $USER->GetEmail(),
            'USER_ID' => $USER->GetID(),
            'PASSWORD' => $ar_user_info,
            'CHECKWORD' => $arUser['CHECKWORD'],
        ];
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
        ];
    }
}
