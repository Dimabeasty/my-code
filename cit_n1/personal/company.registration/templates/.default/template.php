<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
echo '<pre>';
//print_r($arResult);
echo '</pre>';
?>
<style>
    .personal-page__form-buttons{
        justify-content: flex-end;
    }
    .editable-text {
        cursor: pointer;
        border: 1px solid #e6e6e6;
        border-radius: 6px;
    }
    .dont_show {
        display: none;
    }
    .ajax_get_item{
        padding: 6px 6px 6px 6px;
        border-radius: 6px;
    }
    .ajax_get_item:hover{
        background: #ededed;
    }
    .ajax_get_item_bik {
        padding: 6px 6px 6px 6px;
        border-radius: 6px;
    }
    .ajax_get_item_bik:hover{
        background: #ededed;
    }
    .get_ajax_ul{
        list-style: none;
        padding: 0px;
    }
    .personal-list__item {
        align-items: center;
    }
</style>

<form class="reg_company" action="/ajax/add_company.php" method="post">
    <input type="hidden" value="<?=$arResult['EMAIL'];?>" name="email_user">
    <input type="hidden" value="<?=$arResult['USER_ID'];?>" name="user_id">
    
    <ul class="simple-list mb-24-64">
        <li class="mb-24-32">
            <h3 class="text-title text-blue mb-24">Для регистрации компании введите ИНН </h3>
            <ul class="personal-list">
                <li class="personal-list__item">
                    <p class="text-regular text-grey">ИНН</p>
                    <label>
                        <input class="editable-text__input get_inn_input js-validate-tin-number" type="text" name="inn" value="" required>
                        <p class="text-small text-danger js-validate-tin-number-error"></p>
                    </label>
                </li>
                <li class="personal-list__item js-personal-select-wrapper dont_show">
                    <p class="text-regular text-grey">Выберите...</p>
                    <div class="select">
                        <label class="visually-hidden" for="personal-select-tin">Выберите ИНН из списка.</label>
                        <select class="js-personal-select-tin" name="personal-select-tin" id="personal-select-tin">
                            <option value=""></option>
                        </select>
                    </div>
                </li>
            </ul>
        </li>
        <li class="mb-24-32 reg_item dont_show">
            <h3 class="text-title text-blue mb-24">Реквизиты</h3>
            <ul class="personal-list">
                <li class="personal-list__item">
                    <p class="text-regular text-grey">ОКТМО</p>
                    <label>
                        <input class="editable-text__input js-validate-oktmo-number" type="text" name="oktmo"  required >
                        <p class="text-small text-danger js-validate-oktmo-number-error"></p>
                    </label>
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey" >КПП</p>
                    <input class="editable-text__input" type="text" name="kpp">
                </li>
                <li class="personal-list__item" id="">
                    <p class="text-regular text-grey">КОСГУ</p>
                    <input class="editable-text__input" type="number" name="kosgu" required>
                </li>
                <li class="personal-list__item" >
                    <p class="text-regular text-grey">ОКВЭД</p>
                    <input class="editable-text__input" type="text" name="okved" required >
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey">ОГРН</p>
                    <input class="editable-text__input" type="text" name="ogrn" required >
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey">Код по ОКПО</p>
                    <input class="editable-text__input" type="number" name="kod_po_okpo" required >
                </li>
            </ul>
        </li>
        <li class="mb-24-32 reg_item dont_show">
            <h3 class="text-title text-blue mb-24">Об организации</h3>
            <ul class="personal-list">
                <li class="personal-list__item">
                    <p class="text-regular text-grey">Полное наименование организации</p>
                    <textarea style="resize: vertical;" class="editable-text__input" type="text" name="full_name" required ></textarea>
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey">Краткое наименование организации</p>
                  <input class="editable-text__input" type="text" name="short_name" required >
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey">ФИО руководителя</p>
                  <input class="editable-text__input" type="text" name="management_name" required >
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey ">Контактный телефон</p>
                  <input class="editable-text__input textfield js-validate-phone-number" type="tel" name="tel_number" required >
                    <p class="text-small text-danger js-validate-phone-number-error"></p>
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey" >Юридический адрес</p>
                    <textarea class="editable-text__input js-ur-address-input" type="text" rows="3" name="ur_address" required ></textarea>
                </li>
                <li class="personal-list__item personal-list__item--with-switcher">
                    <p class="text-regular text-grey">Фактический адрес совпадает с юридическим</p>
                    <label class="switcher">
                        <span class="switcher__text">Нет</span>
                        <input class="switcher__input js-fact-address-switcher visually-hidden" type="checkbox" name='fact_address_switcher' value="0">
                        <span class="switcher__toggle"></span>
                        <span class="switcher__text">Да</span>
                    </label>
                    <p class="text-regular text-grey js-fact-address-name" id="head_fact">Введите фактический адрес</p>
                  <input class="editable-text__input js-fact-address-input" type="text" name="fact_address" required>
                </li>
            </ul>
        </li>
        <li class="mb-24-32 reg_item dont_show">
            <h3 class="text-title text-blue mb-24">Ответственный от организации</h3>
            <ul class="personal-list">
                <li class="personal-list__item">
                    <p class="text-regular text-grey">ФИО ответственного</p>
                  <input class="editable-text__input" type="text" name="name-of-responsible" value="" required>
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey">Номер телефона ответственного лица</p>
                  <input class="editable-text__input js-validate-phone-number_2" type="tel" name="phone-of-responsible" required>
                    <p class="text-small text-danger js-validate-phone-number-error_2"></p>
                </li>
            </ul>
        </li>
        <li class="mb-24-32 reg_item dont_show">
            <h3 class="text-title text-blue mb-24">БИК</h3>
            <ul class="personal-list">
                <li class="personal-list__item">
                    <p class="text-regular text-grey">БИК</p>
                    <label>
                        <input class="editable-text__input get_bik_input js-validate-bik-number" type="text" type="text" name="bik" required>
                        <p class="text-small text-danger js-validate-bik-number-error"></p>

                    </label>
                </li>
            </ul>
        </li>
        <li class="mb-24-32 reg_item_bik dont_show">
            <h3 class="text-title text-blue mb-24">Расчётный счёт</h3>
            <ul class="personal-list">
                <li class="personal-list__item">
                    <p class="text-regular text-grey">Наименование банка</p>
                  <input class="editable-text__input" id="name_bik" type="text"  name="bank-name" value="" required >
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey">Корпоративный счёт</p>
                  <input class="editable-text__input" id="correspondent_account"  type="number" maxlength="20" type="text" name="correspondent-account" value="" required >
                </li>
                <li class="personal-list__item">
                    <p class="text-regular text-grey">Расчётный счёт</p>
                    <input class="editable-text__input" id="checking_account" type="number" maxlength="20" name="checking-account" value="" required>
                </li>
            </ul>
        </li>
    </ul>
    <ul class="personal-page__form-buttons reg_item dont_show">
        <li>
            <button class="button button--primary" type="submit">Зарегистрировать компанию</button>
        </li>
    </ul>
</form>
<div class="spinner js-loader-element" style="display: none">
    <img src="/upload/images/spinners/spinner.gif" width="200" height="200" alt="Загрузка...">
</div>