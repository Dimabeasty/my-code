<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if(!empty($arResult['COMPANY']) && $arResult['COMPANY']['UF_ACTIVITY']):?>
    <header class="personal-page__grid-header">
        <h3 class="text-title text-blue mb-24">Мои заявки</h3>
        <a href="/personal_account/services/"><button class="button button--secondary" style="height: 50px;">Продление услуг</button></a>
        <button class="button button--primary button--round button--52x52 js-create-new-order" title="Добавить заявку">
            <span class="visually-hidden">Добавить заявку.</span>
            <svg width="24" height="24">
                <use href="<?=MARKUP_PATH?>/images/icons/sprite.svg#plus-icon"></use>
            </svg>
        </button>
    </header>
    <?php if($arResult['ITEMS']):?>
        <ul class="order-list">
            <?php foreach ($arResult['ITEMS'] as $item):?>
                <li class="order-list__item js-order-item" data-order-id="<?=$item['ID']?>">
                    <header class="order-list__header mb-12">
                        <ul class="order-list__tag-list">
                            <li class="tag tag--event tag--light">
                                <a href="<?=$item['DETAIL_PAGE_URL']?>" class="tag__name link-reset">
                                    Заявка #<?=$item['ID']?> от <?=$item['DATE_CREATE']?>
                                </a>
                            </li>
                            <?php if($item['STATUS']['XML_ID'] == 'draft'):?>
                                <li class="tag tag--event tag--danger">
                                    <span class="tag__name">Не отправлена</span>
                                </li>
                            <?php endif;?>
                            <?php if($item['STATUS']['XML_ID'] == 'send'):?>
                                <li class="tag tag--event tag--sender">
                                    <span class="tag__name">Отправлено</span>
                                </li>
                            <?php endif;?>
                            <?php if($item['STATUS']['XML_ID'] == 'success'):?>
                                <li class="tag tag--event tag--sender" style="background-color: #d4edd8; color: #03873c">
                                    <span class="tag__name">Доставлено</span>
                                </li>
                            <?php endif;?>
                        </ul>
                    </header>
                    <div class="order-list__content">
                        <ol class="simple-ol-list text-medium text-dark">
                            <?php if(!empty($arResult['SERVICE'][$item['ID']])):?>

                                <?php if($arResult['SERVICE'][$item['ID']]):?>
                                    <?php foreach ($arResult['SERVICE'][$item['ID']] as $service):?>
                                        <li class="mb-12">
                                            <?=$service['SERVICE']['NAME']?>
                                        </li>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <?php else:?>
                                <p class="text-regular text-dark mb-24">
                                    Для отправления необходимо добавить услуги
                                </p>
                            <?php endif;?>
                        </ol>
                    </div>
                    <footer class="order-list__buttons">
                        <?php if($item['STATUS']['XML_ID'] == 'draft' && !empty($arResult['SERVICE'][$item['ID']])):?>
                            <button class="button button--primary js-send-order">Отправить</button>
                            <button class="button button--secondary js-remove-order">Удалить</button>
                        <?php endif;?>
                    </footer>
                </li>
            <?php endforeach;?>
        </ul>
    <?php else:?>
        <p>Нет заявок </p>
    <?php endif;?>
<?php else:?>
    <p class="text-regular text-dark mb-24">
        Для создания заявки необходимо <a href="/personal_account/main_info/">зарегистрировать компанию</a>
    </p>
<?php endif;?>