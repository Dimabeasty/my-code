<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader,
    Cit\Main\Service\OrderService;

class OrderListComponent extends StandardElementListComponent {

    public function onPrepareComponentParams($params)
    {
        $result = parent::onPrepareComponentParams($params);
        $result = array_merge($result, array(
            'USER_ID' => intval($params['USER_ID']),
        ));
        return $result;
    }

    public function getResult()
    {
        $this->getCompanyUser();

        if(!empty($this->arResult['COMPANY']) && $this->arResult['COMPANY']['UF_ACTIVITY'])
        {
            $arOrdersIds = [];

            $arStatus = getArrayPropertyList($this->arParams['IBLOCK_ID'], 'STATUS');

            $this->prepareOrder();

            $this->prepareFilter();

            $select = [
                'ID',
                'IBLOCK_ID',
                'NAME',
                'DATE_CREATE',
                'DETAIL_PAGE_URL',
                'PROPERTY_STATUS',
            ];
            $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);
            while ($element = $iterator->getNext())
            {
                $this->arResult['ITEMS'][$element['ID']] = [
                    "ID" => $element['ID'],
                    "DETAIL_PAGE_URL" => $element['DETAIL_PAGE_URL'],
                    "DATE_CREATE" => $element['DATE_CREATE']?date('d.m.Y', strtotime($element['DATE_CREATE'])):'',
                    "NAME" => $element['NAME'],
                    "STATUS" => $arStatus[$element['PROPERTY_STATUS_ENUM_ID']],
                ];
                $arOrdersIds[] = $element['ID'];
            }

            $this->getOrderService($arOrdersIds);
        }
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "ACTIVE" => 'Y',
            "PROPERTY_USER_ID" => $this->arParams['USER_ID'],
        ];
    }

    function getOrderService(array $arOrdersIds = [])
    {
        if($arOrdersIds)
        {
            $this->arResult['SERVICE'] = (new OrderService)->getArElements($arOrdersIds);
        }
    }

    function getCompanyUser()
    {
        $this->arResult['COMPANY'] = (new App\Service\UserRegistration)->getLocalUserInfoByID($this->arParams['USER_ID']);
    }
}
