<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader,
    Cit\Main\Service\Service,
    Cit\Main\Service\Network,
    Cit\Main\Service\OsTypes,
    Cit\Main\Service\Nodes,
    Cit\Main\Service\OrderService,
    Cit\Main\Service\UserFieldEnum;

class OrderListComponent extends StandardElementListComponent {

    public function onPrepareComponentParams($params)
    {
        $result = parent::onPrepareComponentParams($params);
        $result = array_merge($result, array(
            'USER_ID' => intval($params['USER_ID']),
            'ORDER_ID' => intval($params['ORDER_ID']),
            'SERVICE_ELEMENT_ID' => intval($params['SERVICE_ELEMENT_ID']),
        ));
        return $result;
    }

    public function getResult()
    {
        $this->arResult['SERVICE'] = (new Service)->getArElements();

        $this->arResult['OS_TYPES'] = (new OsTypes)->getArElements();

        $this->arResult['NODES'] = (new Nodes)->getArElements('UF_FZ_TYPE');

        $this->arResult['FZ_TYPE'] = (new UserFieldEnum)->getArElements('UF_FZ_TYPE');

        $this->arResult['EQUIPMENT'] = (new UserFieldEnum)->getArElements('UF_EQUIPMENT');

        $this->arResult['NETWORK_LIST'] = (new UserFieldEnum)->getArElements('UF_NETWORK_LIST');

        $this->arResult['PO_NETWORK'] = (new UserFieldEnum)->getArElements('UF_PO_NETWORK');

        $this->arResult['PAK_LIST'] = (new UserFieldEnum)->getArElements('UF_PAK');

        $this->arResult['TYPE_INSTALATION'] = (new UserFieldEnum)->getArElements('UF_TYPE_INSTALATION');

        $this->arResult['SUPPORT_CERTIFICATE'] = (new UserFieldEnum)->getArElements('UF_SUPPORT_CERTIFICATE');

        $this->arResult['OPTION_SERVICE'] = (new UserFieldEnum)->getArElements('UF_OPTION_SERVICE');

        $this->arResult['ANTIVIRUSES'] = (new UserFieldEnum)->getArElements('UF_ANTIVIRUSES');

        $this->arResult['CIPF'] = (new UserFieldEnum)->getArElements('UF_CIPF');

        $this->arResult['OFFICE_PROGRAMS'] = (new UserFieldEnum)->getArElements('UF_OFFICE_PROGRAMS');
 
        $this->getServiceInfo();

        $this->getOrder();
    }

    function getServiceInfo()
    {
        if($this->arParams['ORDER_ID'] && $this->arParams['SERVICE_ELEMENT_ID'])
        {
            $this->arResult['ITEM'] = (new OrderService)->getServiceElement($this->arParams['ORDER_ID'], $this->arParams['SERVICE_ELEMENT_ID']);
        }
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "ACTIVE" => 'Y',
            "PROPERTY_USER_ID" => $this->arParams['USER_ID'],
        ];
    }


    public function getOrder()
    {
        $arStatus = getArrayPropertyList($this->arParams['IBLOCK_ID'], 'STATUS');

        $select = [
            'ID',
            'IBLOCK_ID',
            'PROPERTY_STATUS',
        ];
        $iterator = \CIBlockElement::GetList($this->order, [
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
            'ID' => $this->arParams['ORDER_ID']
        ], false, false, $select);
        if ($element = $iterator->getNext())
        {
            $this->arResult['ORDER'] = [
                "ID" => $element['ID'],
                "STATUS" => $arStatus[$element['PROPERTY_STATUS_ENUM_ID']],
            ];
        }
    }
}
