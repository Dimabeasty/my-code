<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$blockPath = $_SERVER['DOCUMENT_ROOT'] . $component->__template->GetFolder();
?>
<form action="/" method="post" class="js-new-order-form" enctype="multipart/form-data">
    <?php if($arParams['SERVICE_ELEMENT_ID']):?>
        <input type="hidden" name="form[service_element_id]" value="<?=$arParams['SERVICE_ELEMENT_ID']?>">
    <?php endif;?>
    <input type="hidden" name="form[order_id]" value="<?=$arParams['ORDER_ID']?>">
    <input type="hidden" name="back_url" value="/personal_account/orders/<?=$arParams['ORDER_ID']?>/">
    <section class="new-order">
        <ul class="new-order__list">
            <li class="new-order__item new-order__item--confirmed new-order__item--editing">
                <div class="new-order__content">
                    <ul class="new-order__content-list">

                        <? require_once $blockPath . '/blocks/step1.php'?>

                        <? require_once $blockPath . '/blocks/step2_po_vip_net.php'?>

                        <? require_once $blockPath . '/blocks/step2_pak_vip_net.php'?>

                        <? require_once $blockPath . '/blocks/step2_certification.php'?>

                        <? require_once $blockPath . '/blocks/step2_purchasing_software.php'?>

                        <? require_once $blockPath . '/blocks/step3.php'?>

                        <? require_once $blockPath . '/blocks/step4.php'?>

                    </ul>
                    <div class="grid-two-columns grid-two-columns--gap-8-16 mb-24-32">
                        <div>
                            <div class="filter-form__control-item">
                                <label class="control control--checkbox">
                                    <input class="control__input visually-hidden js-form-subject" type="checkbox" name="form[subject]" required>
                                    <span class="control__mark">
                                        <svg width="24" height="24">
                                            <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#checkbox"></use>
                                        </svg>
                                    </span>
                                    <span class="control__label text-regular text-grey">согласен на обработку персональных данных согласно <a
                                                class="text-link" href="/personal_account/documents/">политики конфиденциальности</a></span>
                                </label>
                            </div>
                            <div class="filter-form__control-item">
                                <label class="control control--checkbox">
                                    <input class="control__input visually-hidden js-form-subject-op" type="checkbox" name="form[subject]" required>
                                    <span class="control__mark">
                                        <svg width="24" height="24">
                                            <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#checkbox"></use>
                                        </svg>
                                    </span>
                                    <span class="control__label text-regular text-grey">oзнакомлен с <a
                                                class="text-link" href="/personal_account/documents/">требованиями к операционной системе</a></span>
                                </label>
                            </div>
                        </div>
                        <div></div>
                    </div>
                </div>
            </li>
        </ul>
    </section>
    <footer>
        <ul class="personal-page__form-buttons">
            <?php if($arResult['ORDER']['STATUS']['XML_ID'] == 'draft' || empty($arResult['ITEM'])):?>
                <li>
                    <button type="submit" class="button button--primary">Сохранить</button>
                </li>
            <?php endif;?>
            <li>
                <a href="/personal_account/orders/<?=$arParams['ORDER_ID']?>/" type="reset" class="button button--thirty">
                    Закрыть
                </a>
            </li>
        </ul>
    </footer>
</form>