<h5 class="text-large text-blue mb-16">Выберите сеть</h5>
<div class="mb-16-32">
    <ul class="new-order__net-radio-list">

        <?php foreach ($arResult['NETWORK_LIST'] as $network):?>
            <input type="hidden" name="form[network][<?=$network['ID']?>][element_save_id]" value="<?=($arResult['ITEM']['NETWORKS'][$network['ID']]['ID'])?>">

            <li>
                <label class="new-order__net-radio control control--radio control--with-border-mark">
                    <input class="control__input visually-hidden" type="checkbox" name="form[network][<?=$network['ID']?>][network_id]"
                           value="<?=$network['ID']?>" <?=(!empty($arResult['ITEM']['NETWORKS'][$network['ID']]))?'checked':''?>>
                    <span class="control__mark"></span>
                    <span class="control__border-mark"></span>
                    <span class="control__label">
                                                <?=$network['VALUE']?>
                                            </span>
                    <?php if($network['CODE'] == 'other'):?>
                        <input class="network_text" type="text" size="9" name="form[network][<?=$network['ID']?>][description]" value="<?=$arResult['ITEM']['NETWORKS'][$network['ID']]['DESCRIPTION']?>">
                    <?php endif;?>
                </label>
            </li>
        <?php endforeach;?>
    </ul>
</div>

<p class="text-medium text-blue mb-16">Нужен дистрибутив на носителе (CD-диск) </p>
<label class="switcher mb-16-32">
    <span class="switcher__text">Нет</span>
    <input class="switcher__input visually-hidden" type="checkbox" name="form[need-cd]"
        <?=$arResult['ITEM']['NEED_CD']?'checked':''?>>
    <span class="switcher__toggle"></span>
    <span class="switcher__text">Да</span>
</label>