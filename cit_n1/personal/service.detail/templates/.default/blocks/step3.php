<li class="new-order__content-item mb-24-32">
    <h5 class="text-large text-blue mb-16"></h5>
    <div class="new-order__content-wrapper">
        <p class="text-medium text-blue mb-16">По какому ФЗ планируется договор</p>
        <div class="select select--style-grey select--min-content select--border-radius-6 mb-16-32">
            <label>
                <select name="form[fz-type]">
                    <?php foreach ($arResult['FZ_TYPE'] as $fz_type):?>
                        <option value="<?=$fz_type['ID']?>" <?=($arResult['ITEM']['FZ_TYPE']['ID'] == $fz_type['ID'])?'selected':''?>>
                            <?=$fz_type['VALUE']?>
                        </option>
                    <?php endforeach;?>
                </select>
            </label>
        </div>
        <p class="text-medium text-blue mb-16">Плановая дата заключения договора</p>
        <div class="mb-16-32">
            <label>
                <input class="textfield textfield--date text-regular text-grey" type="date" name="form[date_contract]" value="<?=$arResult['ITEM']['DATE_CONTRACT']?>">
            </label>
        </div>
    </div>
</li>