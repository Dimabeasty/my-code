<li class="new-order__content-item mb-24-32">
    <h5 class="text-large text-blue mb-16">Выберите услугу</h5>
    <label class="select select--large">
        <select class="js-form-service" name="form[service]" required>
            <option disabled>Выберите услугу</option>
            <?php foreach ($arResult['SERVICE'] as $service):?>
                <option value="<?=$service['ID']?>" <?=($arResult['ITEM']['SERVICE']['ID'] == $service['ID'])?'selected':''?>
                        data-service-code="<?=$service['CODE']?>">
                    <?=$service['NAME']?>
                </option>
            <?php endforeach;?>
        </select>
    </label>
</li>