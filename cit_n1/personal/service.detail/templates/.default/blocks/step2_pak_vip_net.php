<li class="new-order__content-item mb-24-32" data-li-service-code="pak_vip_net">
    <? require $blockPath . '/blocks/step2_networks.php'?>

    <div class="new-order__content-wrapper">
        <p class="text-medium text-blue mb-16">Выберите ПАК</p>
        <div class="new-order__table mb-16-32">
            <table class="table text-regular text-grey">
                <thead>
                <tr>
                    <th class="text-small text-dark">#</th>
                    <th class="text-small text-dark">Наименование</th>
                    <th class="text-small text-dark">Количество</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($arResult['PAK_LIST'] as $pak):?>
                    <input type="hidden" name="form[equipment][<?=$pak['ID']?>][element_save_id]" value="<?=($arResult['ITEM']['PAK_VIP_NET'][$pak['ID']]['ID'])?>">

                    <tr>
                        <td>
                            <label class="control control--checkbox">
                                <input class="control__input visually-hidden" type="checkbox" name="form[pak][<?=$pak['ID']?>][pak_id]"
                                       value="<?=$pak['ID']?>" <?=(!empty($arResult['ITEM']['PAK_VIP_NET'][$pak['ID']]))?'checked':''?>>
                                <span class="control__mark">
                                      <svg width="24" height="24">
                                        <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#checkbox"></use>
                                      </svg>
                                    </span>
                            </label>
                        </td>
                        <td><?=$pak['VALUE']?></td>
                        <td>
                            <input class="textfield" type="number" name="form[pak][<?=$pak['ID']?>][count]" value="<?=$arResult['ITEM']['PAK_VIP_NET'][$pak['ID']]['COUNT']?>">
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>

        <p class="text-medium text-blue mb-16">Тип установки</p>
        <div class="select select--style-grey select--border-radius-6 mb-16-32">
            <label>
                <select name="form[type_instalation]">
                    <?php foreach ($arResult['TYPE_INSTALATION'] as $type):?>
                        <option value="<?=$type['ID']?>" <?=($arResult['ITEM']['TYPE_INSTALATION'] == $type['ID'])?'selected':''?>>
                            <?=$type['VALUE']?>
                        </option>
                    <?php endforeach;?>
                </select>
            </label>
        </div>

        <p class="text-medium text-blue mb-16">Вариант сертификата поддержки</p>
        <div class="select select--style-grey select--border-radius-6 mb-16-32">
            <label>
                <select name="form[support_certificate]">
                    <?php foreach ($arResult['SUPPORT_CERTIFICATE'] as $fz_type):?>
                        <option value="<?=$fz_type['ID']?>" <?=($arResult['ITEM']['SUPPORT_CERTIFICATE'] == $fz_type['ID'])?'selected':''?>>
                            <?=$fz_type['VALUE']?>
                        </option>
                    <?php endforeach;?>
                </select>
            </label>
        </div>
    </div>
</li>