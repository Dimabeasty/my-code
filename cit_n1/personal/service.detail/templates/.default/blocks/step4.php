<li class="new-order__content-item mb-8-24">
    <h5 class="text-large text-blue mb-16">Комментарии заказчика</h5>
    <div class="new-order__content-wrapper">
        <p class="text-medium text-blue mb-16">Любая дополнительная информация заказчика</p>
        <label>
            <textarea class="textfield textfield--textarea" name="form[comment]" rows="8"
                      placeholder="Введите текст"><?=$arResult['ITEM']['COMMENT']?></textarea>
        </label>
    </div>
</li>