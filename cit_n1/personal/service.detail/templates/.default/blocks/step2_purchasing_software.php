<li class="new-order__content-item mb-24-32" data-li-service-code="purchasing_software">

    <h5 class="text-large text-blue mb-16">Выберите Операционные системы</h5>
    <div class="mb-16-32">
        <ul class="new-order__net-radio-list">
            <?php foreach ($arResult['OS_TYPES'] as $osType):?>
                <li>
                    <label class="new-order__net-radio control control--radio control--with-border-mark">
                        <input class="control__input visually-hidden" type="checkbox" name="form[os][<?=$osType['ID']?>]"
                               value="<?=$osType['ID']?>" <?=in_array($osType['ID'], $arResult['ITEM']['OS']?:[])?'checked':''?>>
                        <span class="control__mark"></span>
                        <span class="control__border-mark"></span>
                        <span class="control__label">
                            <?=$osType['NAME']?>
                        </span>
                    </label>
                </li>
            <?php endforeach;?>
        </ul>
    </div>

    <h5 class="text-large text-blue mb-16">Выберите антивирусы</h5>
    <div class="mb-16-32">
        <ul class="new-order__net-radio-list">
            <?php foreach ($arResult['ANTIVIRUSES'] as $antiviruses):?>
                <li>
                    <label class="new-order__net-radio control control--radio control--with-border-mark">
                        <input class="control__input visually-hidden" type="checkbox" name="form[antiviruses][<?=$antiviruses['ID']?>]"
                               value="<?=$antiviruses['ID']?>" <?=in_array($antiviruses['ID'], $arResult['ITEM']['ANTIVIRUSES']?:[])?'checked':''?>>
                        <span class="control__mark"></span>
                        <span class="control__border-mark"></span>
                        <span class="control__label">
                            <?=$antiviruses['VALUE']?>
                        </span>
                    </label>
                </li>
            <?php endforeach;?>
        </ul>
    </div>

    <h5 class="text-large text-blue mb-16">Выберите СКЗИ</h5>
    <div class="mb-16-32">
        <ul class="new-order__net-radio-list">
            <?php foreach ($arResult['CIPF'] as $cipf):?>
                <li>
                    <label class="new-order__net-radio control control--radio control--with-border-mark">
                        <input class="control__input visually-hidden" type="checkbox" name="form[cipf][<?=$cipf['ID']?>]"
                               value="<?=$cipf['ID']?>" <?=in_array($cipf['ID'], $arResult['ITEM']['CIPF']?:[])?'checked':''?> >
                        <span class="control__mark"></span>
                        <span class="control__border-mark"></span>
                        <span class="control__label">
                            <?=$cipf['VALUE']?>
                        </span>
                    </label>
                </li>
            <?php endforeach;?>
        </ul>
    </div>

    <h5 class="text-large text-blue mb-16">Выберите Офисные программы</h5>
    <div class="mb-16-32">
        <ul class="new-order__net-radio-list">
            <?php foreach ($arResult['OFFICE_PROGRAMS'] as $office_programs):?>
                <li>
                    <label class="new-order__net-radio control control--radio control--with-border-mark">
                        <input class="control__input visually-hidden" type="checkbox" name="form[office_programs][<?=$office_programs['ID']?>]"
                               value="<?=$office_programs['ID']?>" <?=in_array($office_programs['ID'], $arResult['ITEM']['OFFICE_PROGRAMS']?:[])?'checked':''?>>
                        <span class="control__mark"></span>
                        <span class="control__border-mark"></span>
                        <span class="control__label">
                            <?=$office_programs['VALUE']?>
                        </span>
                    </label>
                </li>
            <?php endforeach;?>
        </ul>
    </div>

</li>