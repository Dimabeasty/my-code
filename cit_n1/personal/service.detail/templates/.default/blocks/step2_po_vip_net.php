<li class="new-order__content-item mb-24-32" data-li-service-code="po_vip_net">
    <? require $blockPath . '/blocks/step2_networks.php'?>

    <h5 class="text-large text-blue mb-16">3. Укажите информацию</h5>
    <div class="new-order__content-wrapper">
        <p class="text-medium text-blue mb-16">Выберите необходимые ОС</p>
        <div class="mb-24">
            <ul class="accordion-checkbox">
                <?php foreach ($arResult['OS_TYPES'] as $os_type):?>
                    <li>
                        <div class="accordion-checkbox__item">
                            <header class="accordion-checkbox__header">
                                <label class="control control--checkbox">
                                    <input class="control__input visually-hidden" type="checkbox" name="form[po_vip_net][<?=$os_type['ID']?>][os_type_id]"
                                           value="<?=$os_type['ID']?>" <?=(!empty($arResult['ITEM']['PO_VIP_NET'][$os_type['ID']]))?'checked':''?>>
                                    <span class="control__mark">
                                        <svg width="24" height="24">
                                            <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#checkbox"></use>
                                        </svg>
                                    </span>
                                    <span class="control__label"><?=$os_type['NAME']?></span>
                                </label>
                            </header>
                            <div class="accordion-checkbox__content">
                                <div class="accordion-checkbox__content-inner new-order__acc-check-list">
                                    <div class="grid-two-columns grid-two-columns--common grid-two-columns--align-center grid-two-columns--gap-8-16 mb-8">
                                        <div>
                                            <p class="text-regular text-grey">
                                                Сеть
                                            </p>
                                        </div>
                                        <div>
                                            <label>
                                                Количество
                                            </label>
                                        </div>
                                    </div>
                                    <?php foreach ($arResult['PO_NETWORK'] as $po_network):?>
                                        <input type="hidden" name="form[po_vip_net][<?=$os_type['ID']?>][network][<?=$po_network['ID']?>][network_id]"
                                               value="<?=$po_network['ID']?>">
                                        <div class="grid-two-columns grid-two-columns--common grid-two-columns--align-center grid-two-columns--gap-8-16 mb-8">
                                            <div>
                                                <p class="text-regular text-grey">
                                                    <?=$po_network['VALUE']?>
                                                </p>
                                            </div>
                                            <div>
                                                <label>
                                                    <input class="textfield textfield--number" type="number" name="form[po_vip_net][<?=$os_type['ID']?>][network][<?=$po_network['ID']?>][count]"
                                                           value="<?=$arResult['ITEM']['PO_VIP_NET'][$os_type['ID']][$po_network['ID']]['COUNT']?>">
                                                </label>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>

        <p class="text-medium text-blue mb-16">Опция к услуге</p>
        <div class="select select--style-grey select--border-radius-6 mb-16-32">
            <label>
                <select name="form[option_service]">
                    <?php foreach ($arResult['OPTION_SERVICE'] as $option):?>
                        <option value="<?=$option['ID']?>" <?=($arResult['ITEM']['OPTION_SERVICE'] == $option['ID'])?'selected':''?>>
                            <?=$option['VALUE']?>
                        </option>
                    <?php endforeach;?>
                </select>
            </label>
        </div>
    </div>
</li>