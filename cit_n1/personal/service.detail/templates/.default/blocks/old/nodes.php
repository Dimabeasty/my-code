<li class="new-order__content-item mb-24-32" data-li-service-code="key">
    <h5 class="text-large text-blue mb-16">3. Укажите информацию</h5>
    <div class="new-order__content-wrapper">
        <p class="text-medium text-blue mb-16">Выберите необходимые узлы</p>
        <div class="new-order__table mb-16-32">
            <table class="table text-regular text-grey">
                <thead>
                <tr>
                    <th class="text-small text-dark">#</th>
                    <th class="text-small text-dark">идентификатор узла</th>
                    <th class="text-small text-dark">сеть</th>
                    <th class="text-small text-dark">статус</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($arResult['NODES'] as $node):?>
                    <tr>
                        <td>
                            <label class="control control--checkbox">
                                <input class="control__input visually-hidden" type="checkbox" name="form[node-id][]"
                                       value="<?=$node['ID']?>" <?=(in_array($node['ID'], $arResult['ITEM']['NODES']?:[]))?'checked':''?>>
                                <span class="control__mark">
                                      <svg width="24" height="24">
                                        <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#checkbox"></use>
                                      </svg>
                                    </span>
                            </label>
                        </td>
                        <td><?=$node['NAME']?></td>
                        <td><?=$node['NET']?></td>
                        <?php if($node['STATUS_XML_ID'] == 'active'):?>
                            <td class="text-good">активен</td>
                        <?php else:?>
                            <td class="text-danger">неактивен</td>
                        <?php endif;?>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <p class="text-medium text-blue mb-16">Дополнительных рабочих мест для этой подсети</p>
        <div class="mb-16-32">
            <label>
                <input class="textfield" type="text" name="form[add_jobs_subnet]" value="<?=$arResult['ITEM']['ADD_JOBS_SUBNET']?>">
            </label>
        </div>
        <p class="text-medium text-blue mb-16">Нужен дистрибутив на носителе (CD-диск) </p>
        <label class="switcher mb-16-32">
            <span class="switcher__text">Нет</span>
            <input class="switcher__input visually-hidden" type="checkbox" name="form[need-cd]"
                <?=$arResult['ITEM']['NEED_CD']?'checked':''?>>
            <span class="switcher__toggle"></span>
            <span class="switcher__text">Да</span>
        </label>
        <div class="mb-16-32">
            <ul class="new-order__os-list">
                <?php foreach ($arResult['OS_TYPES'] as $os_type):?>
                    <li>
                        <label class="new-order__os-checkbox control control--checkbox control--with-border-mark">
                            <input class="control__input visually-hidden" type="radio" name="form[os-type]"
                                   value="<?=$os_type['ID']?>"
                                <?=($arResult['ITEM']['OS_TYPE']['ID'] == $os_type['ID'])?'checked':''?>>
                            <span class="common-picture">
                                <picture>
                                    <img src="<?=$os_type['IMG']?>" srcset="<?=$os_type['IMG']?> 2x" width="35" height="32" alt="<?=$os_type['NAME']?>">
                                </picture>
                            </span>
                            <span class="control__mark">
                                <svg width="24" height="24">
                                    <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#checkbox"></use>
                                </svg>
                            </span>
                            <span class="control__label text-regular text-grey">
                                <?=$os_type['NAME']?>
                            </span>
                            <span class="control__border-mark"></span>
                        </label>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
        <p class="text-medium text-blue mb-16">По какому ФЗ планируется договор</p>
        <div class="select select--style-grey select--min-content select--border-radius-6 mb-16-32">
            <label>
                <select name="form[fz-type]">
                    <?php foreach ($arResult['FZ_TYPE'] as $fz_type):?>
                        <option value="<?=$fz_type['ID']?>" <?=($arResult['ITEM']['FZ_TYPE']['ID'] == $fz_type['ID'])?'selected':''?>>
                            <?=$fz_type['VALUE']?>
                        </option>
                    <?php endforeach;?>
                </select>
            </label>
        </div>
        <p class="text-medium text-blue mb-16">Плановая дата заключения договора</p>
        <div class="mb-16-32">
            <label>
                <input class="textfield textfield--date text-regular text-grey" type="date" name="form[date_contract]" value="<?=$arResult['ITEM']['DATE_CONTRACT']?>">
            </label>
        </div>
        <div class="document">
            <img class="document__image" src="<?= MARKUP_PATH ?>/images/general/pdf-icon.svg" width="31" height="32"
                 alt="Устав ГУП КК «ЦИТ».">
            <h3 class="document__title text-regular text-dark">
                <?=$arResult['OS_FILE']['NAME']?>
            </h3>
            <span class="document__file-size">
                <?=$arResult['OS_FILE']['FILE']['FILE_SIZE']?>
            </span>
            <a class="document__button button button--secondary" type="button" href="<?=$arResult['OS_FILE']['FILE']['SRC']?>"
               download="<?=$arResult['OS_FILE']['FILE']['ORIGINAL_NAME']?>" title="Скачать документ.">
                <svg width="24" height="24">
                    <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#download"></use>
                </svg>
                <span>Скачать</span>
            </a>
        </div>
    </div>
</li>