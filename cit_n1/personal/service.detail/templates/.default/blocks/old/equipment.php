<li class="new-order__content-item mb-24-32" data-li-service-code="coordinator">
    <h5 class="text-large text-blue mb-16">3. Укажите информацию</h5>
    <div class="new-order__content-wrapper">
        <p class="text-medium text-blue mb-16">Выберите необходимое оборудование</p>
        <div class="mb-24">
            <ul class="accordion-checkbox">
                <?php foreach ($arResult['EQUIPMENT'] as $equipment):?>
                    <input type="hidden" name="form[equipment][<?=$equipment['ID']?>][element_save_id]" value="<?=($arResult['ITEM']['EQUIPMENTS'][$equipment['ID']]['ID'])?>">
                    <li>
                        <div class="accordion-checkbox__item">
                            <header class="accordion-checkbox__header">
                                <label class="control control--checkbox">
                                    <input class="control__input visually-hidden" type="checkbox" name="form[equipment][<?=$equipment['ID']?>][equipment_id]"
                                           value="<?=$equipment['ID']?>" <?=(!empty($arResult['ITEM']['EQUIPMENTS'][$equipment['ID']]))?'checked':''?>>
                                    <span class="control__mark">
                                        <svg width="24" height="24">
                                            <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#checkbox"></use>
                                        </svg>
                                    </span>
                                    <span class="control__label"><?=$equipment['VALUE']?></span>
                                </label>
                            </header>
                            <div class="accordion-checkbox__content">
                                <div class="accordion-checkbox__content-inner new-order__acc-check-list">
                                    <div
                                            class="grid-two-columns grid-two-columns--common grid-two-columns--align-center grid-two-columns--gap-8-16 mb-8">
                                        <div>
                                            <p class="text-regular text-grey">Количество ПАК (шт.)</p>
                                        </div>
                                        <div>
                                            <label>
                                                <input class="textfield textfield--number" type="number" name="form[equipment][<?=$equipment['ID']?>][count]"
                                                       value="<?=$arResult['ITEM']['EQUIPMENTS'][$equipment['ID']]['COUNT']?>">
                                            </label>
                                        </div>
                                    </div>
                                    <div
                                            class="grid-two-columns grid-two-columns--common grid-two-columns--align-center grid-two-columns--gap-8-16 mb-8">
                                        <div>
                                            <p class="text-regular text-grey">Установка и настройка</p>
                                        </div>
                                        <div>
                                            <label class="switcher switcher--no-padding">
                                                <span class="switcher__text">Нет</span>
                                                <input class="switcher__input visually-hidden" type="checkbox" name="form[equipment][<?=$equipment['ID']?>][installation_configuration]"
                                                       <?=$arResult['ITEM']['EQUIPMENTS'][$equipment['ID']]['INSTALLATION_CONFIGURATION']?'checked':''?>>
                                                <span class="switcher__toggle"></span>
                                                <span class="switcher__text">Да</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div
                                            class="grid-two-columns grid-two-columns--common grid-two-columns--align-center grid-two-columns--gap-8-16 mb-8">
                                        <div>
                                            <p class="text-regular text-grey">Обслуживание и администрирование ПАК</p>
                                        </div>
                                        <div>
                                            <label class="switcher switcher--no-padding">
                                                <span class="switcher__text">Нет</span>
                                                <input class="switcher__input visually-hidden" type="checkbox" name="form[equipment][<?=$equipment['ID']?>][pac]"
                                                    <?=$arResult['ITEM']['EQUIPMENTS'][$equipment['ID']]['PAC']?'checked':''?>>
                                                <span class="switcher__toggle"></span>
                                                <span class="switcher__text">Да</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div
                                            class="grid-two-columns grid-two-columns--common grid-two-columns--align-center mb-8 grid-two-columns--gap-8-16">
                                        <div>
                                            <p class="text-regular text-grey">Обслуживание и администрирование ПО ViPNet
                                                Administrator</p>
                                        </div>
                                        <div>
                                            <label class="switcher switcher--no-padding">
                                                <span class="switcher__text">Нет</span>
                                                <input class="switcher__input visually-hidden" type="checkbox" name="form[equipment][<?=$equipment['ID']?>][vipnet_administrator]"
                                                    <?=$arResult['ITEM']['EQUIPMENTS'][$equipment['ID']]['VIPNET_ADMINISTRATOR']?'checked':''?>>
                                                <span class="switcher__toggle"></span>
                                                <span class="switcher__text">Да</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
        <p class="text-medium text-blue mb-16">По какому ФЗ планируется договор</p>
        <div class="select select--style-grey select--min-content select--border-radius-6 mb-16-32">
            <label>
                <select name="form[fz-type]">
                    <?php foreach ($arResult['FZ_TYPE'] as $fz_type):?>
                        <option value="<?=$fz_type['ID']?>" <?=($arResult['ITEM']['FZ_TYPE']['ID'] == $fz_type['ID'])?'selected':''?>>
                            <?=$fz_type['VALUE']?>
                        </option>
                    <?php endforeach;?>
                </select>
            </label>
        </div>
        <p class="text-medium text-blue mb-16">Плановая дата заключения договора</p>
        <div class="mb-16-32">
            <label>
                <input class="textfield textfield--date text-regular text-grey" type="date" name="form[date_contract]" value="<?=$arResult['ITEM']['DATE_CONTRACT']?>">
            </label>
        </div>
    </div>
</li>