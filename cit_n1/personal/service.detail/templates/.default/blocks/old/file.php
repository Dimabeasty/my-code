<input class="js-file-scan-id" type="hidden" name="form[file-scan-id]"
       value="<?=$arResult['ITEM']['SCAN_FILE']['ID']?>">

<li class="new-order__content-item mb-24-32" data-li-service-code="client">
    <h5 class="text-large text-blue mb-16">3. Укажите информацию</h5>
    <p class="text-medium text-blue mb-16">Прикрепите скан-запрос </p>
    <div class="new-order__content-wrapper new-order__content-wrapper--mobile-visible">
        <p class="text-regular text-grey mb-20 new-order__file-input-text">
            Нужно прикрепить скан письма за подписью уполномоченного лица о восстановлении доступа ключа
            шифрования (размером до 20 МБ)
        </p>
        <article class="input-file" data-file='{"maxFileSize": 20, "maxFileCount": 1, "initFile": "<?=$arResult['ITEM']['SCAN_FILE']['ORIGINAL_NAME']?>"}'>
            <span class="input-file__message"></span>
            <div class="input-file__drop-zone">
                <label class="input-file__button">+
                    <input class="visually-hidden js-form-file-scan" type="file" name="form[file-scan]" accept=".pdf, .png, .jpg"
                           value="<?=$arResult['ITEM']['SCAN_FILE']['SRC']?>">
                </label>
                <span class="input-file__text">Прикрепите файл</span>
            </div>
            <footer class="input-file__footer">
                <a class="input-file__name link-reset" href="<?=$arResult['ITEM']['SCAN_FILE']['SRC']?>" target="_blank">название файла</a>
                <button class="input-file__remove-button" type="button">удалить</button>
            </footer>
        </article>
    </div>
</li>