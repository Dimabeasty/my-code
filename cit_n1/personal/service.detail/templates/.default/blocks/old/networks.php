<li class="new-order__content-item mb-24-32" data-li-service-code="po_vip_net">
    <h5 class="text-large text-blue mb-16">2. Выберите сеть</h5>
    <ul class="new-order__net-radio-list">

        <?php foreach ($arResult['NETWORK_LIST'] as $network):?>
            <input type="hidden" name="form[network][<?=$network['ID']?>][element_save_id]" value="<?=($arResult['ITEM']['NETWORKS'][$network['ID']]['ID'])?>">

            <li>
                <label class="new-order__net-radio control control--radio control--with-border-mark">
                    <input class="control__input visually-hidden" type="checkbox" name="form[network][<?=$network['ID']?>][network_id]"
                           value="<?=$network['ID']?>" <?=(!empty($arResult['ITEM']['NETWORKS'][$network['ID']]))?'checked':''?>>
                    <span class="control__mark"></span>
                    <span class="control__border-mark"></span>
                    <span class="control__label">
                                                <?=$network['VALUE']?>
                                            </span>
                    <?php if($network['CODE'] == 'other'):?>
                        <input class="network_text" type="text" size="9" name="form[network][<?=$network['ID']?>][description]" value="<?=$arResult['ITEM']['NETWORKS'][$network['ID']]['DESCRIPTION']?>">
                    <?php endif;?>
                </label>
            </li>
        <?php endforeach;?>
    </ul>
</li>