<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

try
{
	if (!Main\Loader::includeModule('iblock'))
		throw new Main\LoaderException(Loc::getMessage('NEWS_PARAMETERS_IBLOCK_MODULE_NOT_INSTALLED'));
	
	$iblockTypes = \CIBlockParameters::GetIBlockTypes(Array("-" => " "));
	
	$iblocks = array(0 => " ");
	if (isset($arCurrentValues['IBLOCK_TYPE']) && strlen($arCurrentValues['IBLOCK_TYPE']))
	{
	    $filter = array(
	        'TYPE' => $arCurrentValues['IBLOCK_TYPE'],
	        'ACTIVE' => 'Y'
	    );
	    $rsIBlock = \CIBlock::GetList(array('SORT' => 'ASC'), $filter);
	    while ($arIBlock = $rsIBlock -> GetNext())
	    {
	        $iblocks[$arIBlock['CODE']] = $arIBlock['NAME'];
	    }
	}

    $iblocksCity = array(0 => " ");
	if (isset($arCurrentValues['IBLOCK_CITY_TYPE']) && strlen($arCurrentValues['IBLOCK_CITY_TYPE']))
	{
	    $filter = array(
	        'TYPE' => $arCurrentValues['IBLOCK_JK_TYPE'],
	        'ACTIVE' => 'Y'
	    );
	    $rsIBlock = \CIBlock::GetList(array('SORT' => 'ASC'), $filter);
	    while ($arIBlock = $rsIBlock -> GetNext())
	    {
            $iblocksCity[$arIBlock['CODE']] = $arIBlock['NAME'];
	    }
	}

	$sortFields = array(
		'ID' => Loc::getMessage('NEWS_PARAMETERS_SORT_ID'),
		'NAME' => Loc::getMessage('NEWS_PARAMETERS_SORT_NAME'),
		'ACTIVE_FROM' => Loc::getMessage('NEWS_PARAMETERS_SORT_ACTIVE_FROM'),
		'SORT' => Loc::getMessage('NEWS_PARAMETERS_SORT_SORT')
	);
	
	$sortDirection = array(
		'ASC' => Loc::getMessage('NEWS_PARAMETERS_SORT_ASC'),
		'DESC' => Loc::getMessage('NEWS_PARAMETERS_SORT_DESC')
	);
	
	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'IBLOCK_TYPE' => Array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_IBLOCK_TYPE'),
				'TYPE' => 'LIST',
				'VALUES' => $iblockTypes,
				'DEFAULT' => '',
				'REFRESH' => 'Y'
			),
			'IBLOCK_CODE' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_IBLOCK_ID'),
				'TYPE' => 'LIST',
				'VALUES' => $iblocks
			),
            'IBLOCK_CITY_TYPE' => Array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('NEWS_PARAMETERS_IBLOCK_CITY_TYPE'),
                'TYPE' => 'LIST',
                'VALUES' => $iblockTypes,
                'DEFAULT' => '',
                'REFRESH' => 'Y'
            ),
            'IBLOCK_CITY_CODE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('NEWS_PARAMETERS_IBLOCK_CITY_ID'),
                'TYPE' => 'LIST',
                'VALUES' => $iblocksCity
            ),
			'SHOW_NAV' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_SHOW_NAV'),
				'TYPE' => 'CHECKBOX',
				'DEFAULT' => 'N'
			),
			'PAGER_TEMPLATE' =>  array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_PAGER_TEMPLATE'),
				'TYPE' => 'STRING',
				'DEFAULT' => '0'
			),
			'SEC_NEWS' =>  array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_SEC_NEWS'),
				'TYPE' => 'STRING',
				'DEFAULT' => '0'
			),
			'COUNT' =>  array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_COUNT'),
				'TYPE' => 'STRING',
				'DEFAULT' => '0'
			),
			'SORT_FIELD1' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_SORT_FIELD1'),
				'TYPE' => 'LIST',
				'VALUES' => $sortFields
			),
			'SORT_DIRECTION1' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_SORT_DIRECTION1'),
				'TYPE' => 'LIST',
				'VALUES' => $sortDirection
			),
			'SORT_FIELD2' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_SORT_FIELD2'),
				'TYPE' => 'LIST',
				'VALUES' => $sortFields
			),
			'SORT_DIRECTION2' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('NEWS_PARAMETERS_SORT_DIRECTION2'),
				'TYPE' => 'LIST',
				'VALUES' => $sortDirection
			),
			'SEF_MODE' => array(
	            'index' => array(
	                'NAME' => GetMessage('NEWS_PARAMETERS_INDEX_PAGE'),
	                'DEFAULT' => 'index.php',
	                'VARIABLES' => array()
	            ),
	            'section' => array(
	            	"NAME" => GetMessage('NEWS_PARAMETERS_DETAIL_PAGE'),
	                "DEFAULT" => '#SECTION_CODE#/',
	                "VARIABLES" => array('SECTION_CODE'),
				),
	            'open_hearts' => array(
	            	"NAME" => GetMessage('NEWS_PARAMETERS_DETAIL_PAGE'),
	                "DEFAULT" => 'open_hearts/',
	                "VARIABLES" => array(),
				),
	            'detail' => array(
	            	"NAME" => GetMessage('NEWS_PARAMETERS_DETAIL_PAGE'),
	                "DEFAULT" => '#SECTION_CODE#/#ELEMENT_CODE#/',
	                "VARIABLES" => array('SECTION_CODE', 'ELEMENT_CODE'),
				)
	        ),
			'CACHE_TIME' => array(
				'DEFAULT' => 3600
			)
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e -> getMessage());
}
?>