<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$APPLICATION->IncludeComponent(
    "personal:service.detail",
    "",
    array(
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "IBLOCK_CODE" => $arParams['IBLOCK_CODE'],
        "ORDER_ID" => $arResult['VARIABLES']['ELEMENT_ID'],
        "SERVICE_ELEMENT_ID" => $arResult['VARIABLES']['SERVICE_ELEMENT_ID'],
        'CACHE_TIME' => $arParams['CACHE_TIME'],
        'CACHE_TYPE' => $arParams['CACHE_TYPE'],
        'SORT_FIELD1' => $arParams['SORT_FIELD1'],
        'SORT_DIRECTION1' => $arParams['SORT_DIRECTION1'],
        'SORT_FIELD2' => $arParams['SORT_FIELD2'],
        'SORT_DIRECTION2' => $arParams['SORT_DIRECTION2'],
        'USER_ID' => $arParams['USER_ID'],
    ),
    false
);