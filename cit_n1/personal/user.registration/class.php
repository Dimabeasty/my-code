<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule('highloadblock');
\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;
use Bitrix\Iblock;
Loader::IncludeModule("iblock");

class UserRegistrationComponent extends StandardElementListComponent {

    protected function checkParams()
    {}

    public function getResult()
    {

    }
}
