<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */
?>

<form action="" name="regform" method="post" class="js-registration-form" id="reg_form_event">
	<p class="text-regular text-dark mb-24-32" >На указанный в форме email придет запрос на подтверждение
		регистрации.</p>
	<ul class="simple-list container mb-24-32">
		<li class="mb-8">
			<div class="grid-two-columns grid-two-columns--gap-8-16">
				<p class="text-regular text-dark">Email:*</p>
				<label>
					<input class="textfield js-validate-email" type="text" name="email" required id="email-reg">
					<p class="text-small text-danger js-validate-email-error"></p>
				</label>
			</div>
		</li>
		<li class="mb-8">
			<div class="grid-two-columns grid-two-columns--gap-8-16">
				<p class="text-regular text-dark">Пароль:*</p>
				<label>
					<input class="textfield js-validate-password" type="password" name="password" required id="pass-reg">
					<p class="text-small text-danger js-validate-password-error"></p>
				</label>
			</div>
		</li>
		<li class="mb-8">
			<div class="grid-two-columns grid-two-columns--gap-8-16">
				<p class="text-regular text-dark">Подтверждение пароля:*</p>
				<label>
					<input class="textfield js-validate-password-confirm" type="password" name="confirm_password" required id="confirm-reg">
					<p class="text-small text-danger js-validate-password-confirm-error"></p>
				</label>
			</div>
		</li>
		<li class="mb-8">
			<div class="grid-two-columns grid-two-columns--gap-8-16">
				<p class="text-regular text-dark">Имя:*</p>
				<label>
					<input class="textfield js-validate-name" type="text" name="name" required id="name-reg">
					<p class="text-small text-danger js-validate-name-error"></p>
				</label>
			</div>
		</li>
		<li class="mb-8">
			<div class="grid-two-columns grid-two-columns--gap-8-16">
				<p class="text-regular text-dark">Фамилия:*</p>
				<label>
					<input class="textfield js-validate-last-name" type="text" name="last_name" required id="last-reg">
					<p class="text-small text-danger js-validate-last-name-error"></p>
				</label>
			</div>
		</li>
	</ul>
	<div class="grid-two-columns grid-two-columns--gap-8-16">
		<div class="mb-24-32">
			<p class="text-regular text-dark mb-8">Пароль должен быть не менее 6 символов длиной.</p>
			<p class="text-regular text-dark">*Поля, обязательные для заполнения.</p>
		</div>
		<div>
			<button class="button button--primary" id="reg_user" type="submit">Зарегистрироваться</button>
		</div>
	</div>
</form>
