<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader,
    Cit\Main\Service\OrderService;

class OrderDetailComponent extends StandardElementListComponent {

    public function onPrepareComponentParams($params)
    {
        $result = parent::onPrepareComponentParams($params);
        $result = array_merge($result, array(
            'USER_ID' => intval($params['USER_ID']),
            'ORDER_ID' => intval($params['ORDER_ID']),
        ));
        return $result;
    }

    public function getResult()
    {
        $arStatus = getArrayPropertyList($this->arParams['IBLOCK_ID'], 'STATUS');


        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DATE_CREATE',
            'PROPERTY_STATUS',
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);
        if ($element = $iterator->getNext())
        {
            $this->arResult['ITEM'] = [
                "ID" => $element['ID'],
                "DETAIL_PAGE_URL" => $element['DETAIL_PAGE_URL'],
                "DATE_CREATE" => $element['DATE_CREATE']?date('d.m.Y', strtotime($element['DATE_CREATE'])):'',
                "NAME" => $element['NAME'],
                "STATUS" => $arStatus[$element['PROPERTY_STATUS_ENUM_ID']],
            ];

            $this->getServiceList();
        }
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "ACTIVE" => 'Y',
            "ID" => $this->arParams['ORDER_ID'],
            "PROPERTY_USER_ID" => $this->arParams['USER_ID'],
        ];
    }

    function getServiceList()
    {
        $arService = (new OrderService)->getArElements([$this->arParams['ORDER_ID']]);
        $this->arResult['ITEM']['SERVICE'] = $arService[$this->arParams['ORDER_ID']];
    }
}
