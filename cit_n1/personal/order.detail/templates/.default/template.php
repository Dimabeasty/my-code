<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<form action="/" method="post" class="js-order-item" data-order-id="<?=$arResult['ITEM']['ID']?>">
    <header class="personal-page__grid-header personal-page__grid-header--with-date mb-24-16">
        <h3 class="text-title text-blue mb-8">Заявка #<?=$arResult['ITEM']['ID']?></h3>
        <span class="personal-page__order-date">от <?=$arResult['ITEM']['DATE_CREATE']?></span>
    </header>

    <p>Статус: <?=$arResult['ITEM']['STATUS']['VALUE']?></p>

    <?php if(empty($arResult['ITEM']['SERVICE'])):?>
        <p class="text-regular text-dark mb-24">
            Не заполнены услуги
        </p>
    <?php endif;?>
    <section class="new-order">
        <ul class="new-order__list">
            <?php foreach ($arResult['ITEM']['SERVICE'] as $service):?>
                <li class="new-order__item new-order__item--confirmed" data-service-id="<?=$service['ID']?>">
                    <header class="new-order__header">
                        <h4 class="new-order__title text-large">
                            <?=$service['SERVICE']['NAME']?>
                        </h4>
                            <a href="service/<?=$service['ID']?>/" class="button new-order__button new-order__button--edit" type="button" title="Редактировать">
                                <span class="visually-hidden">Редактировать.</span>
                                <svg width="24" height="24">
                                    <use href="<?=MARKUP_PATH?>/images/icons/sprite.svg#edit-icon"></use>
                                </svg>
                            </a>
                        <?php if($arResult['ITEM']['STATUS']['XML_ID'] == 'draft'):?>
                            <button class="button new-order__button new-order__button--delete js-service-remove" type="button" title="Удалить">
                                <span class="visually-hidden">Удалить.</span>
                                <svg width="24" height="24">
                                    <use href="<?=MARKUP_PATH?>/images/icons/sprite.svg#trash-icon"></use>
                                </svg>
                            </button>
                        <?php endif;?>
                    </header>
                </li>
            <?php endforeach;?>
        </ul>
    </section>
    <footer>
        <ul class="personal-page__form-buttons">
            <?php if($arResult['ITEM']['STATUS']['XML_ID'] == 'draft'):?>

                <li>
                    <a href="service/create/" type="button" class="button button--secondary">Добавить услугу</a>
                </li>
                <?php if(!empty($arResult['ITEM']['SERVICE'])):?>
                    <li>
                        <button class="button button--primary js-send-order">Отправить</button>
                    </li>
                <?php endif;?>
            <?php endif;?>
            <li>
                <a href="/personal_account/orders/" class="button button--thirty js-order-go-back">Назад</a>
            </li>
        </ul>
    </footer>
</form>