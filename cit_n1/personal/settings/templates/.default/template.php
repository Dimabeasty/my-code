<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<div class="mb-24-32">
        <h3 class="text-title text-blue mb-24">Уведомления</h3>
        <div class="grid-two-columns grid-two-columns--common">
            <p class="text-medium text-dark">
                Уведомление об окончании действия текущего контракта/договора и о
                необходимости заключения на новый срок по электронной почте за 30 дней
            </p>
            <label class="switcher">
                <span class="switcher__text">Нет</span>
                <input class="switcher__input visually-hidden" type="checkbox" checked disabled>
                <span class="switcher__toggle"></span>
                <span class="switcher__text">Да</span>
            </label>
        </div>
</div>

<div class="mb-24-32">
    <h3 class="text-title text-blue mb-24">Ответственный от организации</h3>
    <div class="grid-two-columns grid-two-columns--common">
        <p class="text-medium text-dark">
            Вы можете изменить текущего ответственного от организации
        </p>
        <button class="button button--secondary change_otv_org" type="button">Изменить</button>
    </div>
    <div class="new_otv_org dont_show">
    <label class="label_password__input">
        <p class="text-medium text-dark">
            Введите ФИО Ответственного
        </p>
        <input class="login-form__input name_otv" type="text" name="name_otv">
    </label>
    <label class="label_password__input">
        <p class="text-medium text-dark">
            Введите новый контактный телефон
        </p>
        <input class="editable-text__input textfield js-validate-phone-number tel_otv" type="tel" name="number_otv">
        <p class="text-small text-danger js-validate-phone-number-error"></p>
    </label>
    <p class="text-medium text-dark last_otv">
    </p>
    <button class="button button--secondary ajax_btn_otv" type="button" style="margin-top: 10px;">Сохранить</button>
    </div>
</div>

<h3 class="text-title text-blue mb-24">Пароль</h3>
<div class="grid-two-columns grid-two-columns--common">
<p class="text-medium text-dark">
    Вы можете изменить текущий пароль
</p>
<button class="button button--secondary change_pass" type="button">Изменить</button>
</div>
<div class="new_password dont_show">
<label class="label_password__input">
    <p class="text-medium text-dark old_password_text">
        Введите свой старый пароль
    </p>
<input class="login-form__input password_input old_password" type="password" placeholder="Пароль" name="user-password" required="">
<input class="user_login" type="hidden" placeholder="Пароль" name="user-password" value="<?=$arResult['ITEM']['LOGIN'];?>">
<input class="check_word" type="hidden" placeholder="Пароль" name="user-password" value="<?=$arResult['ITEM']['CHECKWORD'];?>">
</label>
<label class="label_password__input">
    <p class="text-medium text-dark">
        Введите новый пароль
    </p>
    <input class="login-form__input password_input new_password_input" type="password" placeholder="Пароль" name="user-password" required="">
</label>
<label class="label_password__input">
    <p class="text-medium text-dark">
        Повторите новый пароль
    </p>
    <input class="login-form__input password_input confirm_password" type="password" placeholder="Пароль" name="user-password" required="">
</label>
<p class="text-medium text-dark last_password">
</p>
<button class="button button--secondary ajax_btn_pass" type="button" style="margin-top: 10px;">Сохранить</button>
</div>
