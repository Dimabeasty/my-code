const change_pass = document.querySelector('.change_pass')
const change_otv_org = document.querySelector('.change_otv_org')
const new_password = document.querySelector('.new_password')
const new_otv_org = document.querySelector('.new_otv_org')
const user_login = document.querySelector('.user_login')
const check_word = document.querySelector('.check_word')
const last_password = document.querySelector('.last_password')
const last_otv = document.querySelector('.last_otv')
const ajax_btn_pass = document.querySelector('.ajax_btn_pass')
const ajax_btn_otv = document.querySelector('.ajax_btn_otv')
const new_password_input = document.querySelector('.new_password_input')
const confirm_password = document.querySelector('.confirm_password')
const old_password_text = document.querySelector('.old_password_text')
const old_password = document.querySelector('.old_password')
const name_otv = document.querySelector('.name_otv')
const tel_otv = document.querySelector('.tel_otv')
const password_inputs = document.querySelectorAll('.password_input')

change_otv_org.addEventListener('click', function (){
    if(new_otv_org.classList[1] == 'dont_show'){
        new_otv_org.classList.remove('dont_show')
    } else {
        new_otv_org.classList.add('dont_show')
    }
})

change_pass.addEventListener('click', function (){
    if(new_password.classList[1] == 'dont_show'){
        new_password.classList.remove('dont_show')
    } else {
        new_password.classList.add('dont_show')
    }
})
password_inputs.forEach(item => {
    item.addEventListener('change', function (){
        let result = AjaxTruePass(user_login.value, old_password.value)
        }
    );
});


function AjaxTruePass(login, pass){
    let http = new XMLHttpRequest();
    let url = '/ajax/password_true.php';
    let params = 'login=' + login + '&password=' + pass;
    let result = ''

    http.open('POST', url, true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if(http.readyState == 4 && http.status == 200) {
            result = http.responseText
            if(result == 1){
                old_password_text.innerText = 'Введите свой старый пароль'
            } else {
                old_password_text.innerText = 'Введите свой старый пароль - пароль неверный !'
            }
        }
    }
    http.send(params);
    return result
}
ajax_btn_pass.addEventListener('click', function (){
         AjaxChangePass(user_login.value, new_password_input.value, confirm_password.value, check_word.value)
})
ajax_btn_otv.addEventListener('click', function (){
    AjaxChangeOtv(name_otv.value, tel_otv.value)
})


function AjaxChangePass(login, pass, confirm, checkword){
    let http = new XMLHttpRequest();
    let url = '/ajax/change_password.php';
    let params = 'login=' + login + '&password=' + pass + '&CONFIRM_PASSWORD=' + confirm + '&checkword=' + checkword;
    let result = ''

    http.open('POST', url, true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if(http.readyState == 4 && http.status == 200) {
            result = http.responseText
            if(result == 1){
                last_password.innerHTML = 'Изменения сохранены'
            } else {
                last_password.innerHTML = result
            }
        }
    }
    http.send(params);
    return result
}

function AjaxChangeOtv(fio, number){
    let http = new XMLHttpRequest();
    let url = '/ajax/change_otv.php';
    let params = 'name_otv=' + fio + '&number_otv=' + number;
    let result = ''

    http.open('POST', url, true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if(http.readyState == 4 && http.status == 200) {
            result = http.responseText
            last_otv.innerHTML = result
        }
    }
    http.send(params);
    return result
}
