<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule('highloadblock');
\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader,
    Cit\Main\Service\Users,
    Bitrix\Iblock;
Loader::IncludeModule("iblock");

class PersonalSettingsComponent extends StandardElementListComponent
{
    public function onPrepareComponentParams($params)
    {
        $result = parent::onPrepareComponentParams($params);
        $result = array_merge($result, array(
            'USER_ID' => intval($params['USER_ID']),
        ));
        return $result;
    }

    protected function checkParams()
    {}

    public function getResult()
    {
        $arUsersInfo = (new Users)->getUsersInfo([$this->arParams['USER_ID']]);

        $this->arResult['ITEM'] = [
            'LOGIN' => $arUsersInfo[$this->arParams['USER_ID']]['LOGIN'],
            'PASSWORD' => $arUsersInfo[$this->arParams['USER_ID']]['PASSWORD'],
            'NOTIFICATION' => $arUsersInfo[$this->arParams['USER_ID']]['NOTIFICATION'],
        ];
    }
}
