<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule('highloadblock');
\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;
use  App\Service\UserRegistration as URClass;

Loader::IncludeModule("iblock");

class CompanyServicesComponent extends StandardElementListComponent
{

    public function getResult()
    {
        global $USER;
        $user_id = $USER->getID();

        $this->prepareFilter($user_id);
        $this->prepareOrder();
        $this->prepareSelect();
        
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $this->select);

        while ($element = $iterator->getNext()) {
            $prom_array = [];
            echo '<pre>';
            //print_r($element);
            echo '</pre>';
            $this->arResult['ID'] =  $element['ID'];
            $this->arResult['INN'] =  $element['PROPERTY_P_INN_VALUE'];
            $this->arResult['SERVICE'] =  $element['PROPERTY_P_SERVICE_VALUE'];
            $this->arResult['NUMBER_SERVICE'] =  $element['PROPERTY_P_NUMBER_SERVICE_VALUE'];
            $this->arResult['START_SERVICE'] =  $element['PROPERTY_P_START_SERVICE_VALUE'];
            $this->arResult['ITEM']['NETWORK'] =  $element['PROPERTY_P_NETWORK_VALUE'];
            $this->arResult['ITEM']['DATE_END'] =  $element['PROPERTY_P_DATE_END_VALUE'];
            $this->arResult['ITEM']['DATE_WORK'] =  $element['PROPERTY_P_DATE_WORK_VALUE'];
            $this->arResult['ITEM']['STATUS'] =  $element['PROPERTY_P_STATUS_VALUE'];
            $this->arResult['ITEM']['TECH_STATUS'] =  $element['PROPERTY_P_TECH_STATUS_VALUE'];
            $this->arResult['ITEM']['P_UZEL'] =  $element['PROPERTY_P_UZEL_VALUE'];
        }
        $this->arResult['ITEM'] = self::prepareArray($this->arResult['ITEM']);
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter($user_id){
        $ur_class = new URClass();
        $inn = $ur_class->getLocalUserInfoByID($user_id)['UF_INN'];
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
            "PROPERTY_P_INN" => $inn
        ];
    }

    function prepareSelect(){
        $this->select = [
            'ID',
            'PROPERTY_P_INN',
            'PROPERTY_P_NETWORK',
            'PROPERTY_P_DATE_END',
            'PROPERTY_P_DATE_WORK',
            'PROPERTY_P_STATUS',
            'PROPERTY_P_TECH_STATUS',
            'PROPERTY_P_SERVICE',
            'PROPERTY_P_NUMBER_SERVICE',
            'PROPERTY_P_START_SERVICE',
            'PROPERTY_P_UZEL',
        ];
    }

    public function prepareArray($array){
        $result_arr = [];
        foreach ($array as $key=>$value){

            foreach ($value as $n=>$item){
                $a = 1;
                $result_arr[$a.$n][$key] = $item;
                $a++;
            }
        }
        return $result_arr;
    }
}
