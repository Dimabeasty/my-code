<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $USER;
?>
<header class="personal-page__grid-header personal-page__grid-header--with-date mb-24-16"><h3
            class="text-title text-blue mb-24"><?= $arResult['SERVICE']; ?></h3></header>
<form action="/ajax/services_add.php" method="post">
    <input type="hidden" value="<?=$arResult['INN'];?>" name="inn">
    <input type="hidden" value="<?php echo $USER->GetID();?>" name="id">
    <input type="hidden" value="<?=$arResult['ID'];?>" name="id_element">
    <input type="hidden" value="<?=$arResult['NUMBER_SERVICE'];?>" name="service_id">
<ul class="new-order__content-list">
    <?php
    $count = 0;
    foreach ($arResult['ITEM'] as $item){
        $count++;
        ?>
    <li class="new-order__content-item mb-16">
        <ul class="simple-list">

            <li class="mb-16 new-order__content-wrapper">
                <div class="grid-two-columns grid-two-columns--common mb-24">
                    <p class="requisites__content text-blue">Узел:</p>
                    <p class="text-medium text-dark">
                        <?= $item['P_UZEL']; ?>
                    </p>
                    <p class="requisites__content text-blue">Сеть:</p>
                    <p class="text-medium text-dark">
                        <?= $item['NETWORK']; ?>
                    </p>
                    <label class="switcher">
                        <span class="switcher__text">Нет</span>
                        <input class="switcher__input visually-hidden" type="checkbox" name="form_network[]" value="<?=$item['P_UZEL'].' - '.$item['NETWORK'];?>">
                        <span class="switcher__toggle"></span>
                        <span class="switcher__text">Продлить</span>
                    </label>
                </div>
                <div class="grid-two-columns grid-two-columns--common">
                    <p class="requisites__content text-blue mb-16">Действует до:</p>
                    <p class="text-medium text-dark"><?= $item['DATE_WORK']; ?></p>
                    <p class="requisites__content text-blue mb-16">Статус активации:</p>
                    <p class="text-medium text-dark">
                        <label class="control control--checkbox">
                            <input class="control__input visually-hidden" type="checkbox" name="form[pak][22][pak_id]" <?php if($item['STATUS'] == 'Активирована') echo 'checked';?> disabled value="22" wfd-id="id57">
                            <span class="control__mark">
                                      <svg width="24" height="24">
                                        <use href="/local/templates/.default/markup/build//images/icons/sprite.svg#checkbox"></use>
                                      </svg>
                                    </span>
                        </label>
                    </p>
                    <p class="requisites__content text-blue mb-16">Статус техподдержки:</p>
                    <p class="text-medium text-dark"><label class="control control--checkbox">
                            <input class="control__input visually-hidden" type="checkbox" name="form[pak][22][pak_id]" <?php if($item['TECH_STATUS'] == 'Активирована') echo 'checked';?> disabled value="22" wfd-id="id57">
                            <span class="control__mark">
                                      <svg width="24" height="24">
                                        <use href="/local/templates/.default/markup/build//images/icons/sprite.svg#checkbox"></use>
                                      </svg>
                                    </span>
                        </label></p>
                </div>
            </li>
        </ul>
    </li>
    <?php
    }

    ?>
    <li>
        <p class="text-medium text-blue mb-16">Опция к услуге</p>
        <div class="select select--style-grey select--border-radius-6 mb-16-32">
            <label>
                <select name="form[option_service]">
                    <option value="1">
                        Установка ПО с выездом к Заказчику
                    </option>
                    <option value="2">
                        Установка ПО не требуется
                    </option>
                    <option value="3">
                        Установка ПО в офисе ЦИТ
                    </option>
                </select>
            </label>
        </div>
    </li>
    <li class="new-order__content-item mb-24-32">
        <button class="button button--primary">Продлить услуги</button>
    </li>
</ul>
</form>