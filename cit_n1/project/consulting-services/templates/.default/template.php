<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<?php if (!empty($arResult)):
echo $arResult['DETAIL_TEXT'];
?>
<ul class="partners-list">
<?php
foreach ($arResult['PROPERTY_PARTNERS_LIST'] as $part):
?>
    <li class="partners-list__item">
        <a class="partners-list__link" href="https://academyit.ru" target="_blank">
            <div class="partners-list__image">
                <picture>
                    <img src="<?=$part['DETAIL_PICTURE'];?>" srcset="./images/partners/partners-20@2x.png 2x"
                         alt="Логотип компании-партнера.">
                </picture>
            </div>
            <div class="partners-list__content">
                <h3 class="partners-list__name"><?= $part['NAME'];?></h3>
                <span class="partners-list__website"><?= $part['PROPERTY_LINK_VALUE'];?></span>
            </div>
        </a>
    </li>
<?php
endforeach;
?>
</ul>
    <h2 class="page-title page-title--mt">галерея</h2>
    <ul class="gallery-slider slider" data-zoom="container">
<?php
foreach ($arResult['PROPERTY_IMAGES_LIST'] as $img):
?>
        <li class="gallery-slider__item slider__item image-zoom" data-zoom="button">
            <picture>
                <img src="<?=$img?>"
                     srcset="./images/gallery/gallery-1@2x.jpg 2x" alt="Фото из галереи.">
            </picture>
            <div data-zoom="full-image">
                <img src="<?=$img?>" alt="Фото из галереи.">
            </div>
        </li>
<?php
endforeach;
endif;
?>
    </ul>
</div>



