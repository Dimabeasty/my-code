<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class ConsultingListComponent extends StandardElementListComponent
{

    public function getResult()
    {
        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DETAIL_TEXT',
            'PROPERTY_IMAGES_LIST',
            'PROPERTY_PARTNERS_LIST',
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);
        $partners = [];
        while ($element = $iterator->getNext()) {
            foreach ($element['PROPERTY_PARTNERS_LIST_VALUE'] as $id):
                $prom_filter = [
                    "IBLOCK_ID" => 3,
                    "IBLOCK_TYPE" => 'content',
                    "ID" => $id,
                ];
                $prom_select = [
                    'ID',
                    'IBLOCK_ID',
                    'NAME',
                    'PROPERTY_LINK',
                    'DETAIL_PICTURE',
                ];
                $prom_res = \CIBlockElement::GetList($this->order, $prom_filter, false, $this->navParams, $prom_select);
                while ($elem = $prom_res->getNext()) {
                    $array_prom = [
                        "NAME" => $elem['NAME'],
                        "PROPERTY_LINK_VALUE" => $elem['~PROPERTY_LINK_VALUE'],
                        "DETAIL_PICTURE" => \CFile::GetPath($elem['DETAIL_PICTURE']),
                    ];
                    array_push($partners, $array_prom);
                }
            endforeach;

            $images_arr = [];
            foreach ($element['PROPERTY_IMAGES_LIST_VALUE'] as $img):
            $image = \CFile::GetPath($img);
            array_push($images_arr, $image);
            endforeach;

            $this->arResult = [
                "ID" => $element['ID'],
                "NAME" => $element['NAME'],
                "DETAIL_TEXT" => $element['DETAIL_TEXT'],
                "DETAIL_PICTURE" => \CFile::GetPath($element['DETAIL_PICTURE']),
                "PROPERTY_IMAGES_LIST" => $images_arr,
                "PROPERTY_PARTNERS_LIST" => $partners,
            ];
        }
    }

    function prepareOrder()
    {
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter()
    {
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
            "ACTIVE" => 'Y',
            "ID" => '41',
        ];
    }
}
