<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<?php if (!empty($arResult['ITEMS'])): ?>
<ul class="partners-list">
    <?php foreach ($arResult['ITEMS'] as $item):?>
        <li class="partners-list__item">
            <a class="partners-list__link" href="<?= $item['LINK'];?> ">
                <div class="partners-list__image">
                    <picture>
                        <img src="<?= $item['PREVIEW_PICTURE'];?>" srcset="<?= $item['PREVIEW_PICTURE'];?> 2x"
                             alt="Логотип <?= $item['NAME'];?>">
                    </picture>
                </div>
                <div class="partners-list__content">
                    <h3 class="partners-list__name"><?= $item['NAME'];?></h3>
                    <span class="partners-list__website"><?= $item['LINK'];?></span>
                </div>
            </a>
        </li>
    <?php endforeach; ?>
</ul>

<?php endif;

?>




