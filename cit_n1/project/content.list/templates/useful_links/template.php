<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>

<?php if(!empty($arResult['ITEMS'])):?>
    <div class="footer__slider-wrapper">
        <ul class="footer__slider slider">
            <?php foreach($arResult['ITEMS'] as $item):?>
                <li class="footer__slider-item">
                    <a class="footer__slider-link" href="<?=$item['LINK']?>" target="_blank">
                        <div class="footer__slider-image">
                            <picture>
                                
                                <img src="<?=$item['PREVIEW_PICTURE']?>" srcset="<?=$item['PREVIEW_PICTURE']?> 2x"
                                     width="140"
                                     height="140" alt="<?=$item['NAME']?>">
                            </picture>
                        </div>
                        <span class="footer__slider-text">
                            <?=$item['NAME']?>
                        </span>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
<?php endif;?>




