<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>

<?php if(!empty($arResult['ITEMS'])):?>
    <section class="brands">
        <div class="brands__container">
            <ul class="brands__slider slider">
                <?php foreach($arResult['ITEMS'] as $item):?>
                    <li class="brands__item">
                        <img src="<?=$item['PREVIEW_PICTURE']?>" width="203" height="84" alt="<?=$item['NAME']?>">
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </section>
<?php endif;?>




