<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>

<?php if(!empty($arResult['ITEMS'])): ?>
    <section class="company__section company__certificates">
        <h2 class="company__certificates-title page-title">лицензии и сертификаты</h2>
        <ul class="company__certificates-slider slider" data-zoom="container">
            <?php foreach($arResult['ITEMS'] as $item):?>
                <li class="company__certificates-slider-item image-zoom slider__item" data-zoom="button">
                    <picture>
                        <img src="<?=$item['DETAIL_PICTURE']?>"
                             srcset="<?=$item['DETAIL_PICTURE']?> 2x" alt="<?=$item['NAME']?>">
                    </picture>
                    <div data-zoom="full-image">
                        <img src="<?=$item['DETAIL_PICTURE']?>" alt="<?=$item['NAME']?>">
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
    </section>
<?php endif;?>




