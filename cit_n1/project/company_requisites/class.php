<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class CompanyRequisitesComponent extends StandardElementListComponent {

    protected function checkParams()
    {}

    function getSelectFields()
    {
        $this->arResult['FIELDS']['FIRST_BLOCK'] = [
            'REQUISITES_FIRST' => [
                'COMPANY_NAME',
                'COMPANY_UR_ADDRESS',
                'COMPANY_FACT_ADDRESS',
                'COMPANY_COR_ADDRESS',
            ],
            'REQUISITES_SECOND' => [
                'COMPANY_EMAIL',
                'COMPANY_SITE',
                'COMPANY_PHONE',
                'COMPANY_FAX',
            ],
        ];
        $this->arResult['FIELDS']['SECOND_BLOCK'] = [
            'REQUISITES_THIRD' => [
                'COMPANY_INN',
                'COMPANY_KPP',
                'COMPANY_OGRN',
                'COMPANY_OKPO',
                'COMPANY_OKVED',
                'COMPANY_OKTOMO',
            ],
            'REQUISITES_FOURTH' => [
                'COMPANY_PAYMENT_ACCOUNT',
                'COMPANY_BANK',
                'COMPANY_CORRESPONDENT_ACCOUNT',
                'COMPANY_BIK',
                'COMPANY_DIRECTOR',
            ],
        ];
    }

    public function getResult()
    {
        $this->getSelectFields();

        foreach ($this->arResult['FIELDS'] as $blockKey => $block)
        {
            foreach ($block as $keyPart => $arFields)
            {
                foreach ($arFields as $key => $selectFieldCode)
                {
                    $this->arResult['ITEMS'][$blockKey][$keyPart][$key] = getSettingValueByCode($selectFieldCode);
                }
            }
        }

        $this->arResult['COMPANY_FILE'] = getSettingValueByCode('COMPANY_FILE');
    }
}
