<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>


<section class="company__section company__requisites requisites">
    <header class="requisites__header">
        <h2 class="requisites__section-title page-title">реквизиты</h2>
        <?php if($arResult['COMPANY_FILE']['FILE']):?>
            <a class="requisites__download button button--secondary" href="<?=$arResult['COMPANY_FILE']['FILE']['SRC']?>"
               download="<?=$arResult['COMPANY_FILE']['FILE']['ORIGINAL_NAME']?>" title="Скачать документ.">
                <span class="visually-hidden">Скачать документ с реквизитами.</span>
                <svg width="24" height="24">
                    <use href="<?=MARKUP_PATH?>/images/icons/sprite.svg#download"></use>
                </svg>
            </a>
        <?php endif;?>
    </header>
    <div class="requisites__inner">
        <div>
            <?php if($arResult['ITEMS']['FIRST_BLOCK']):?>
                <?php foreach ($arResult['ITEMS']['FIRST_BLOCK'] as $firstBlock):?>
                    <ul class="requisites__table">
                        <?php foreach ($firstBlock as $field):?>
                            <?php if(!empty($field)):?>
                                <li>
                                    <p class="requisites__title">
                                        <?=$field['NAME']?>
                                    </p>
                                    <?php if($field['TYPE_LINK_XML_ID'] == 'email'):?>
                                        <p class="requisites__content">
                                            <a href="mailto:<?=$field['VALUE']?>">
                                                <?=$field['VALUE']?>
                                            </a>
                                        </p>
                                    <?php elseif($field['TYPE_LINK_XML_ID'] == 'phone'):?>
                                        <p class="requisites__content">
                                            <a href="tel:<?=$field['VALUE']?>">
                                                <?=$field['VALUE']?>
                                            </a>
                                        </p>
                                    <?php elseif($field['TYPE_LINK_XML_ID'] == 'external'):?>
                                        <p class="requisites__content">
                                            <a href="<?=$field['VALUE']?>">
                                                <?=$field['VALUE']?>
                                            </a>
                                        </p>
                                    <?php else:?>
                                        <p class="requisites__content">
                                            <?=$field['VALUE']?>
                                        </p>
                                    <?php endif;?>
                                </li>
                            <?php endif;?>
                        <?php endforeach;?>
                    </ul>
                <?php endforeach;?>
            <?php endif;?>
        </div>
        <div>
            <?php if($arResult['ITEMS']['SECOND_BLOCK']):?>
                <?php foreach ($arResult['ITEMS']['SECOND_BLOCK'] as $secondBlock):?>
                    <ul class="requisites__table">
                        <?php foreach ($secondBlock as $field):?>
                            <?php if(!empty($field)):?>
                                <li>
                                    <p class="requisites__title">
                                        <?=$field['NAME']?>
                                    </p>
                                    <p class="requisites__content">
                                        <?=$field['VALUE']?>
                                    </p>
                                </li>
                            <?php endif;?>
                        <?php endforeach;?>
                    </ul>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>
</section>






