<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class AntiCorruptionComponent extends StandardElementListComponent {

    protected function checkParams()
    {}

    public function getResult()
    {
        $this->arResult['ANTI_CORRUPTION_TEXT'] = getSettingValueByCode('ANTI_CORRUPTION_TEXT');
        $this->arResult['ANTI_CORRUPTION_FILE'] = getSettingValueByCode('ANTI_CORRUPTION_FILE');
    }
}
