<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>

<div class="grid-two-columns grid-two-columns--free">
    <div class="grid-two-columns__left">
        <p class="text-large text-dark mb-24">
            <?=$arResult['ANTI_CORRUPTION_TEXT']['VALUE']?>
        </p>
        <?php if($arResult['ANTI_CORRUPTION_FILE']['FILE']):?>
            <div class="document">
                <img class="document__image" src="<?=MARKUP_PATH?>/images/general/<?=$arResult['ANTI_CORRUPTION_FILE']['FILE']['EXT']?>-icon.svg" width="31" height="32"
                     alt="Устав ГУП КК «ЦИТ».">
                <h3 class="document__title">
                    <?=$arResult['ANTI_CORRUPTION_FILE']['VALUE']?>
                </h3>
                <span class="document__file-size"><?=$arResult['ANTI_CORRUPTION_FILE']['FILE']['FILE_SIZE']?></span>
                <a class="document__button button button--secondary" href="<?=$arResult['ANTI_CORRUPTION_FILE']['FILE']['SRC']?>"
                   download="<?=$arResult['ANTI_CORRUPTION_FILE']['FILE']['ORIGINAL_NAME']?>" title="Скачать документ.">
                    <svg width="24" height="24">
                        <use href="<?=MARKUP_PATH?>/images/icons/sprite.svg#download"></use>
                    </svg>
                    <span>Скачать</span>
                </a>
            </div>
        <?php endif;?>
    </div>
    <div class="grid-two-columns__right">
    </div>
</div>