<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class MainBannerElementComponent extends StandardElementListComponent {

    public function getResult()
    {
        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'PREVIEW_PICTURE',
            'DETAIL_PICTURE',
            'PROPERTY_CITE',
            'PROPERTY_CITE_AUTHOR',
            'PROPERTY_BUTTONS_BLOCK',
            'PROPERTY_SUBTITLE',
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, false, $select);
        if ($element = $iterator->getNext())
        {
            $this->arResult['ITEM'] = [
                'ID' => $element['ID'],
                'NAME' => $element['NAME'],
                'PREVIEW_PICTURE' => $element['PREVIEW_PICTURE']?\CFile::GetPath($element['PREVIEW_PICTURE']):'',
                'DETAIL_PICTURE' => $element['DETAIL_PICTURE']?\CFile::GetPath($element['DETAIL_PICTURE']):'',
                'CITE' => $element['~PROPERTY_CITE_VALUE']['TEXT'],
                'CITE_AUTHOR' => $element['PROPERTY_CITE_AUTHOR_VALUE'],
                'BUTTONS_BLOCK' => $element['~PROPERTY_BUTTONS_BLOCK_VALUE']['TEXT'],
                'SUBTITLE' => $element['PROPERTY_SUBTITLE_VALUE'],
            ];
        }
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "ACTIVE_DATE" => 'Y',
            "ACTIVE" => 'Y',
        ];
    }
}
