<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (!empty($arResult['ITEM'])): ?>
    <section class="main-intro">
        <div class="main-intro__container">
            <div class="main-intro__inner">
                <p class="main-intro__sup-title">
                    <?=$arResult['ITEM']['SUBTITLE']?>
                </p>
                <h1 class="main-intro__title">
                    <span>«</span><?=$arResult['ITEM']['NAME']?><span>»</span>
                </h1>
                <p class="main-intro__cite">
                    <?=$arResult['ITEM']['CITE']?>
                </p>
                <span class="main-intro__author">
                    <?=$arResult['ITEM']['CITE_AUTHOR']?>
                </span>
                <div class="main-intro__buttons">
                    <?=$arResult['ITEM']['BUTTONS_BLOCK']?>
                </div>
            </div>
            <div class="main-intro__image">
                <picture>
                    <source media="(min-width: 1260px)"
                            srcset="<?=$arResult['ITEM']['DETAIL_PICTURE']?> 1x, <?=$arResult['ITEM']['DETAIL_PICTURE']?> 2x">
                    <img src="<?=$arResult['ITEM']['PREVIEW_PICTURE']?>" srcset="<?=$arResult['ITEM']['PREVIEW_PICTURE']?> 2x"
                         alt="<?=$arResult['ITEM']['NAME']?>" width="768" height="512">
                </picture>
            </div>
        </div>
    </section>
<?php endif; ?>