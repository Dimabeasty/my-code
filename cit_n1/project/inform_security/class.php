<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class InfoPageComponent extends StandardElementListComponent {

    public function getResult()
    {
        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DETAIL_TEXT',
            'PREVIEW_PICTURE'
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);
        while ($element = $iterator->getNext()) {
            $img = '';
            if(!empty($element['PROPERTY_IMG_VALUE'])){
                $img = \CFile::GetPath($element['PROPERTY_IMG_VALUE']);
            }elseif($element['PREVIEW_PICTURE'])
            {
                $img = \CFile::GetPath($element['PREVIEW_PICTURE']);
            }

            $detailPicture = '';
            if($element['DETAIL_PICTURE'])
            {
                $detailPicture = \CFile::GetPath($element['DETAIL_PICTURE']);
            }

            $this->arResult['ITEMS'][] = [
                "ID" => $element['ID'],
                "NAME" => $element['NAME'],
                "DETAIL_TEXT" => $element['DETAIL_TEXT'],
                "PREVIEW_PICTURE" => $img,
                "DETAIL_PICTURE" => $detailPicture
            ];/*
            echo '<pre>';
            print_r ($this->arResult);
            echo '</pre>';*/
        }
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "ACTIVE" => 'Y',
            "NAME" => $this->arParams['NAME'],
        ];

    }
}
