<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class DocumentsListComponent extends StandardElementListComponent {

    public function onPrepareComponentParams($params)
    {
        $result = parent::onPrepareComponentParams($params);
        $result = array_merge($result, array(
            'SEARCH' => trim($params['SEARCH']),
        ));
        return $result;
    }

    public function executeEpilog()
    {
        if($this->arParams['SHOW_SECTION_HEAD'] && !empty($this->arResult['SECTION'])){

            global $APPLICATION;
            $APPLICATION->SetPageProperty("title", $this->arResult['SECTION']['NAME']);
            $APPLICATION->SetTitle('');
            $APPLICATION->AddChainItem($this->arResult['SECTION']['NAME']);
        }
    }

    public function getResult()
    {
        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'PREVIEW_TEXT',
            'DETAIL_TEXT',
            'PROPERTY_FILE',
            'PROPERTY_ADD_NAME',
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);
        while ($element = $iterator->getNext())
        {
            if(!empty($element['PROPERTY_FILE_VALUE']))
            {
                $this->arResult['ITEMS'][] = prepareFile($element['PROPERTY_FILE_VALUE'], $element);
            }
        }
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "ACTIVE" => 'Y',
        ];

        if(!empty($this->arParams['SEARCH'])){
            $this->filter['NAME'] = '%'.strip_tags($this->arParams['SEARCH']).'%';
        }
    }
}
