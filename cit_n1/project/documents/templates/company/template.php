<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (!empty($arResult['ITEMS'])): ?>
    <section class="company__section company__documents">
        <h2 class="page-title">ДОКУМЕНТЫ</h2>
        <ul class="company__documents-list">
            <?php foreach ($arResult['ITEMS'] as $item): ?>
                <li class="document">
                    <img class="document__image" src="<?=MARKUP_PATH?>/images/general/<?=$item['EXT']?>-icon.svg" width="31" height="32"
                         alt="<?=$item['NAME']?>">
                    <h3 class="document__title">
                       <?=$item['NAME']?>
                    </h3>
                    <span class="document__file-size">
                        <?=$item['FILE_SIZE']?>
                    </span>
                    <a class="document__button button button--secondary" href="<?=$item['SRC']?>" download="<?=$item['NAME']?>" title="Скачать документ.">
                        <svg width="24" height="24">
                            <use href="<?=MARKUP_PATH?>/images/icons/sprite.svg#download"></use>
                        </svg>
                        <span>Скачать</span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>
<?php endif; ?>