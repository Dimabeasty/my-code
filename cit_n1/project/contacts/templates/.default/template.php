<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<div class="grid-two-columns">
    <div>
        <div class="contacts-page__contacts grid-two-columns grid-two-columns--media-desktop">
            <div>
                <ul class="contacts-list">
                    <?php if($arResult['MAIN_FIELDS']):?>
                        <?php foreach ($arResult['MAIN_FIELDS'] as $field):?>
                            <?php if(!empty($field)):?>
                                <li class="contacts-list__item">
                                    <h3 class="contacts-list__title">
                                        <?=$field['NAME']?>
                                    </h3>
                                    <p class="contacts-list__text">
                                        <?=$field['VALUE']?>
                                    </p>
                                </li>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                </ul>
            </div>
            <div>
                <ul class="contacts-list">
                    <?php if($arResult['ADDITIONAL_FIELDS']):?>
                        <?php foreach ($arResult['ADDITIONAL_FIELDS'] as $field):?>
                            <?php if(!empty($field)):?>
                                <li class="contacts-list__item">
                                    <h3 class="contacts-list__title">
                                        <?=$field['NAME']?>
                                    </h3>
                                    <?php if($field['TYPE_LINK']):?>
                                        <a class="contacts-list__link text-link" href="mailto:<?=$field['VALUE']?>">
                                            <?=$field['VALUE']?>
                                        </a>
                                    <?php else:?>
                                        <p class="contacts-list__text">
                                            <?=$field['VALUE']?>
                                        </p>
                                    <?php endif;?>
                                </li>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
    <div>
        <div class="contacts-page__map map">
            <iframe class="map__element"
                    src="<?=$arResult['CONTACT_MAP_SRC']['VALUE']?>"
                    width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade">
            </iframe>
        </div>
    </div>
</div>






