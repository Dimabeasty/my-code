<?
$MESS['NEWS_LIST_IBLOCK_MODULE_NOT_INSTALLED'] = 'Модуль "Инфоблоки" не установлен';
$MESS['NEWS_LIST_PARAMETERS_SORT_ID'] = 'ID';
$MESS['NEWS_LIST_PARAMETERS_SORT_NAME'] = 'Название';
$MESS['NEWS_LIST_PARAMETERS_SORT_ACTIVE_FROM'] = 'Дата начала активности';
$MESS['NEWS_LIST_PARAMETERS_SORT_SORT'] = 'Индекс сортировки';
$MESS['NEWS_LIST_PARAMETERS_SORT_ASC'] = 'По возрастанию';
$MESS['NEWS_LIST_PARAMETERS_SORT_DESC'] = 'По убыванию';
$MESS['NEWS_LIST_PARAMETERS_IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['NEWS_LIST_PARAMETERS_IBLOCK_ID'] = 'Инфоблок';
$MESS['NEWS_LIST_PARAMETERS_SHOW_NAV'] = 'Постраничная навигация';
$MESS['NEWS_LIST_PARAMETERS_COUNT'] = 'Количество элементов';
$MESS['NEWS_LIST_PARAMETERS_SORT_FIELD1'] = 'Поле первой сортировки';
$MESS['NEWS_LIST_PARAMETERS_SORT_DIRECTION1'] = 'Направление первой сортировки';
$MESS['NEWS_LIST_PARAMETERS_SORT_FIELD2'] = 'Поле второй сортировки';
$MESS['NEWS_LIST_PARAMETERS_SORT_DIRECTION2'] = 'Направление второй сортировки';
$MESS['NEWS_LIST_PARAMETERS_SECTION_CODE'] = 'Код раздела';
$MESS['NEWS_LIST_PARAMETERS_GROUP_SORT_NAME'] = 'Параметры сортировки';
$MESS['NEWS_LIST_PARAMETERS_IBLOCK_CITY_ID'] = 'Код инфобока с городами';
$MESS['NEWS_LIST_PARAMETERS_IBLOCK_CITY_TYPE'] = 'Тип инфоблока с городами';
?>