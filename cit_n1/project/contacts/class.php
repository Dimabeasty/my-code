<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class ContactsComponent extends StandardElementListComponent {

    protected function checkParams()
    {}

    function getSelectFields()
    {
        $this->arResult['SELECT_MAIN_FIELDS'] = [
            'CONTACTS_FULL_NAME',
            'CONTACTS_UR_ADDRESS',
            'CONTACTS_FACT_ADDRESS',
            'CONTACTS_CORR_ADDRESS',
        ];

        $this->arResult['SELECT_ADDITIONAL_FIELDS'] = [
            'CONTACTS_WORK_TIME',
            'CONTACTS_EMAIL',
            'CONTACTS_DIRECTOR',
            'CONTACTS_PHONE',
            'CONTACTS_FAX',
        ];
    }

    public function getResult()
    {
        $this->getSelectFields();

        $this->getMainFields();

        $this->getAdditionalFields();

        $this->arResult['CONTACT_MAP_SRC'] = getSettingValueByCode('CONTACT_MAP_SRC');
    }

    function getMainFields()
    {
        foreach ($this->arResult['SELECT_MAIN_FIELDS'] as $selectFieldCode)
        {
            $this->arResult['MAIN_FIELDS'][$selectFieldCode] = getSettingValueByCode($selectFieldCode);
        }
    }

    function getAdditionalFields()
    {
        foreach ($this->arResult['SELECT_ADDITIONAL_FIELDS'] as $selectFieldCode)
        {
            $this->arResult['ADDITIONAL_FIELDS'][$selectFieldCode] = getSettingValueByCode($selectFieldCode);
        }
    }
}
