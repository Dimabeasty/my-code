<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class InfrastructureDetailComponent extends StandardElementListComponent {

    public function onPrepareComponentParams($params)
    {
        $result = parent::onPrepareComponentParams($params);
        $result = array_merge($result, array(
            'ELEMENT_CODE' => trim($params['ELEMENT_CODE']),
            'SEF_FOLDER' => trim($params['SEF_FOLDER']),
        ));
        return $result;
    }

    public function executeEpilog()
    {
        global $APPLICATION;

        if (isset($this->arResult['SEO_VALUES']['ELEMENT_META_TITLE']))
        {
            $APPLICATION->SetPageProperty("title", $this->arResult['SEO_VALUES']['ELEMENT_META_TITLE']);
            $APPLICATION->SetTitle('');
            $APPLICATION->AddChainItem($this->arResult['SEO_VALUES']['ELEMENT_META_TITLE']);
        }
        elseif ($this->arResult['NAME'])
        {
            $APPLICATION->SetPageProperty("title", $this->arResult['NAME']);
            $APPLICATION->SetTitle('');
            $APPLICATION->AddChainItem($this->arResult['NAME']);
        }

        if (isset($this->arResult['SEO_VALUES']['ELEMENT_META_KEYWORDS']))
        {
            $APPLICATION->SetPageProperty("keywords", $this->arResult['SEO_VALUES']['ELEMENT_META_KEYWORDS']);
        }
        if (isset($this->arResult['SEO_VALUES']['ELEMENT_META_DESCRIPTION']))
        {
            $APPLICATION->SetPageProperty("description", $this->arResult['SEO_VALUES']['ELEMENT_META_DESCRIPTION']);
        }
        if (isset($this->arResult['PICTURE']))
        {
            $APPLICATION->SetPageProperty('og:image', 'https://'.$_SERVER['HTTP_HOST'].$this->arResult['PICTURE']);
        }

        $APPLICATION->SetPageProperty("type", 'article');

        if(\Bitrix\Main\Loader::includeModule("iblock") && $this->arParams['IBLOCK_ID'])
        {
            $arButtons = \CIBlock::GetPanelButtons($this->arParams["IBLOCK_ID"], 0,
                0,
                array("SECTION_BUTTONS"=>false, "SESSID"=>false));

            if($APPLICATION->GetShowIncludeAreas())
                $this->AddIncludeAreaIcons(\CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
        }
    }

    public function getResult()
    {
        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DETAIL_TEXT',
            'IBLOCK_SECTION_ID',
            'PROPERTY_MAIN_HEAD',
            'PROPERTY_SUB_HEAD',
            'PROPERTY_WORK_FLOW',
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);

        if ($element = $iterator->getNext())
        {

            $this->arResult['ITEM'] = [
                "ID" => $element['ID'],
                "NAME" => $element['NAME'],
                "DETAIL_TEXT" => $element['DETAIL_TEXT'],
                "IBLOCK_SECTION_ID" => $element['IBLOCK_SECTION_ID'],
            ];

            $this->arResult["DETAIL_TEXT"] = $element['DETAIL_TEXT'];
            $this->arResult["NAME"] = $element['NAME'];
            $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($this->arParams["IBLOCK_ID"], $element["ID"]);
            $this->arResult["SEO_VALUES"] = $ipropValues->getValues();

        }

        if(empty($this->arResult["ITEM"]))
        {
            LocalRedirect(SITE_DIR . '404.php');
        }

        $this->cacheKeys[] = 'NAME';
        $this->cacheKeys[] = 'SEO_VALUES';
        $this->cacheKeys[] = 'DETAIL_PICTURE';
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
            "CODE" => $this->arParams['ELEMENT_CODE'],
            "ACTIVE" => 'Y',
        ];
    }
    /*
    public function getResult()
    {
        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DETAIL_TEXT',
            'PREVIEW_PICTURE',
            'CODE'
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);
        while ($element = $iterator->getNext()) {
            $img = '';
            if(!empty($element['PROPERTY_IMG_VALUE'])){
                $img = \CFile::GetPath($element['PROPERTY_IMG_VALUE']);
            }elseif($element['PREVIEW_PICTURE'])
            {
                $img = \CFile::GetPath($element['PREVIEW_PICTURE']);
            }

            $detailPicture = '';
            if($element['DETAIL_PICTURE'])
            {
                $detailPicture = \CFile::GetPath($element['DETAIL_PICTURE']);
            }

            $this->arResult['ITEMS'][] = [
                "ID" => $element['ID'],
                "NAME" => $element['NAME'],
                "DETAIL_TEXT" => $element['DETAIL_TEXT'],
                "PREVIEW_PICTURE" => $img,
                "DETAIL_PICTURE" => $detailPicture,
                "CODE" => $element['CODE'],
            ];
        }
    }
    */
/*
    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "ACTIVE" => 'Y',
        ];

    }
*/
}
