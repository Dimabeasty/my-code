<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<div class="grid-sticky mb-40-48">
<?php if(!empty($arResult)):?>
    <div class="grid-sticky__left">
                <?php echo $arResult['DETAIL_TEXT']; ?>
    </div>
<?php endif;?>
    <div class="grid-sticky__right">
        <div class="grid-sticky__right-content">
            <article class="free-consultation">
                <div class="free-consultation__inner">
                    <h3 class="free-consultation__title">Бесплатная консультация</h3>
                    <p class="free-consultation__description">
                        если&nbsp;у&nbsp;вас&nbsp;возникли вопросы&nbsp;— обратитесь в&nbsp;ЦИТ&nbsp;удобным
                        для&nbsp;вас&nbsp;способом. Наши специалисты помогут определиться с&nbsp;выбором и&nbsp;расскажут о&nbsp;всей линейке продуктов
                        и
                        сервисов
                    </p>
                    <div class="free-consultation__wrapper">
                        <div>
                            <p class="free-consultation__text">позвоните нам по номеру</p>
                            <a href="tel:78001000900" class="free-consultation__phone text-link">8 800 1000 900</a>
                        </div>
                        <div>
                            <p class="free-consultation__text">напишите на адрес электронной почты</p>
                            <a href="mailto:vipnet@cit.krasnodar.ru" class="free-consultation__button button button--primary">Написать
                                на
                                vipnet@cit.krasnodar.ru</a>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>
</div>




