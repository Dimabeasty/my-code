<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (!empty($arResult['ITEMS'])): ?>
    <div class="grid-two-columns">
        <div class="grid-two-columns__left">
            <?php if (!empty($arResult['ITEMS']['COMPANY_PAGE_MAIN_TITLE'])): ?>
                <p class="company__text">
                    <?=$arResult['ITEMS']['COMPANY_PAGE_MAIN_TITLE']['VALUE']?>
                </p>
            <?php endif; ?>

            <?php if (!empty($arResult['ITEMS']['COMPANY_PAGE_TEXT'])): ?>
                <p class="company__text">
                    <?=$arResult['ITEMS']['COMPANY_PAGE_TEXT']['VALUE']?>
                </p>
            <?php endif; ?>
        </div>
        <div class="grid-two-columns__right">
            <div class="company__description">
                <?php if (!empty($arResult['ITEMS']['COMPANY_PAGE_DESCRIPTION_1'])): ?>
                    <p class="text-regular text-grey">
                        <?=$arResult['ITEMS']['COMPANY_PAGE_DESCRIPTION_1']['VALUE']?>
                    </p>
                <?php endif; ?>

                <?php if (!empty($arResult['ITEMS']['COMPANY_PAGE_DESCRIPTION_2'])): ?>
                    <p class="text-regular text-grey">
                        <?=$arResult['ITEMS']['COMPANY_PAGE_DESCRIPTION_2']['VALUE']?>
                    </p>
                <?php endif; ?>
            </div>
            <ul class="company__features">

                <?php if (!empty($arResult['ITEMS']['COMPANY_PAGE_YEAR'])): ?>
                    <li class="company__features-item">
                        <p class="company__features-number">
                            <?=$arResult['ITEMS']['COMPANY_PAGE_YEAR']['VALUE']?>
                        </p>
                        <span class="company__features-text">
                            <?=$arResult['ITEMS']['COMPANY_PAGE_YEAR']['NAME']?>
                        </span>
                    </li>
                <?php endif; ?>
                <?php if (!empty($arResult['ITEMS']['COMPANY_PAGE_EXECUTED_CONTRACTS'])): ?>
                    <li class="company__features-item">
                        <p class="company__features-number"><span>></span>
                            <?=$arResult['ITEMS']['COMPANY_PAGE_EXECUTED_CONTRACTS']['VALUE']?>
                        </p>
                        <span class="company__features-text">
                            <?=$arResult['ITEMS']['COMPANY_PAGE_EXECUTED_CONTRACTS']['NAME']?>
                        </span>
                    </li>
                <?php endif; ?>
                <?php if (!empty($arResult['ITEMS']['COMPANY_PAGE_SPECIALISTS'])): ?>
                    <li class="company__features-item">
                        <p class="company__features-number"><span>></span>
                            <?=$arResult['ITEMS']['COMPANY_PAGE_SPECIALISTS']['VALUE']?>
                        </p>
                        <span class="company__features-text">
                            <?=$arResult['ITEMS']['COMPANY_PAGE_SPECIALISTS']['NAME']?>
                        </span>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

<?php endif; ?>