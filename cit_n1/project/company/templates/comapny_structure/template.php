<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (!empty($arResult['ITEMS']['COMPANY_PAGE_ORGANIZATIONAL_STRUCTURE']['FILE'])): ?>
    <section class="company__section company__structure">
        <h2 class="page-title">
            <?=$arResult['ITEMS']['COMPANY_PAGE_ORGANIZATIONAL_STRUCTURE']['NAME']?>
        </h2>
        <div class="company__structure-image">
            <picture>
                <img src="<?=$arResult['ITEMS']['COMPANY_PAGE_ORGANIZATIONAL_STRUCTURE']['FILE']['SRC']?>" srcset="<?=$arResult['ITEMS']['COMPANY_PAGE_ORGANIZATIONAL_STRUCTURE']['FILE']['SRC']?> 2x"
                     width="660" height="3529" alt="<?=$arResult['ITEMS']['COMPANY_PAGE_ORGANIZATIONAL_STRUCTURE']['NAME']?>">
            </picture>
        </div>
    </section>
<?php endif; ?>