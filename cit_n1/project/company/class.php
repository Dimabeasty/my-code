<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class СompanyDetailComponent extends StandardElementListComponent {

    protected function checkParams()
    {}

    function getSelectFields()
    {
        $this->arResult['FIELDS'] = [
            'COMPANY_PAGE_MAIN_TITLE',
            'COMPANY_PAGE_TEXT',
            'COMPANY_PAGE_DESCRIPTION_1',
            'COMPANY_PAGE_DESCRIPTION_2',
            'COMPANY_PAGE_YEAR',
            'COMPANY_PAGE_EXECUTED_CONTRACTS',
            'COMPANY_PAGE_SPECIALISTS',
            'COMPANY_PAGE_ORGANIZATIONAL_STRUCTURE',
        ];

    }

    public function getResult()
    {
        $this->getSelectFields();

        foreach ($this->arResult['FIELDS'] as $key => $field)
        {
            $this->arResult['ITEMS'][$field] = getSettingValueByCode($field);
        }
    }
}
