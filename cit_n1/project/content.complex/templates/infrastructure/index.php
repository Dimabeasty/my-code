<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?

$APPLICATION->IncludeComponent(
    "project:projects.list",
    "",
    Array(
        "IBLOCK_TYPE" => 'provider',
        "IBLOCK_CODE" => 'infrastructure',
        "SORT_FIELD1" => $arParams['SORT_FIELD1'],
        "SORT_DIRECTION1" => $arParams['SORT_DIRECTION1'],
        "SORT_FIELD2" => $arParams['SORT_FIELD2'],
        "SORT_DIRECTION2" => $arParams['SORT_DIRECTION2'],
        "CACHE_TYPE" => $arParams['CACHE_TYPE'],
        "CACHE_TIME" => $arParams['CACHE_TIME'],
    ),
);?>