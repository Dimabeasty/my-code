<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$APPLICATION->IncludeComponent(
    'project:projects.detail',
    '',
    [
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "IBLOCK_CODE" => $arParams['IBLOCK_CODE'],
        'CACHE_TIME' => '3600',
        "ELEMENT_CODE" => $arResult['VARIABLES']['ELEMENT_CODE'],
        "SHOW_SECTION_HEAD" => true,
        'CACHE_TYPE' => 'Y',
        'SORT_FIELD1' => 'SORT',
        'SORT_DIRECTION1' => 'ASC',
        'SORT_FIELD2' => 'ID',
        'SORT_DIRECTION2' => 'ASC',
    ],
    false
);


    ?>