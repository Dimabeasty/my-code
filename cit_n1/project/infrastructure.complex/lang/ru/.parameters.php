<?
$MESS['NEWS_PARAMETERS_IBLOCK_MODULE_NOT_INSTALLED'] = 'Модуль "Инфоблоки" не установлен';
$MESS['NEWS_PARAMETERS_SORT_ID'] = 'ID';
$MESS['NEWS_PARAMETERS_SORT_NAME'] = 'Название';
$MESS['NEWS_PARAMETERS_SORT_ACTIVE_FROM'] = 'Дата начала активности';
$MESS['NEWS_PARAMETERS_SORT_SORT'] = 'Индекс сортировки';
$MESS['NEWS_PARAMETERS_SORT_ASC'] = 'По возрастанию';
$MESS['NEWS_PARAMETERS_SORT_DESC'] = 'По убыванию';
$MESS['NEWS_PARAMETERS_IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['NEWS_PARAMETERS_IBLOCK_ID'] = 'Инфоблок';
$MESS['NEWS_PARAMETERS_IBLOCK_CITY_TYPE'] = 'Тип инфоблока для городов';
$MESS['NEWS_PARAMETERS_IBLOCK_CITY_ID'] = 'Инфоблок для городов';
$MESS['NEWS_PARAMETERS_PAGER_TEMPLATE'] = 'Название шаблона постраничной навигации';
$MESS['NEWS_PARAMETERS_SEC_NEWS'] = 'Код раздела с новостями';
$MESS['NEWS_PARAMETERS_SHOW_NAV'] = 'Постраничная навигация';
$MESS['NEWS_PARAMETERS_COUNT'] = 'Количество элементов';
$MESS['NEWS_PARAMETERS_SORT_FIELD1'] = 'Поле первой сортировки';
$MESS['NEWS_PARAMETERS_SORT_DIRECTION1'] = 'Направление первой сортировки';
$MESS['NEWS_PARAMETERS_SORT_FIELD2'] = 'Поле второй сортировки';
$MESS['NEWS_PARAMETERS_SORT_DIRECTION2'] = 'Направление второй сортировки';
$MESS['NEWS_PARAMETERS_INDEX_PAGE'] = 'Страница со списком элементов';
$MESS['NEWS_PARAMETERS_DETAIL_PAGE'] = 'Страница с детальным описанием элемента';
?>