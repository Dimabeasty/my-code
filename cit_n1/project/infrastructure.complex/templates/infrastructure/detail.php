<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$APPLICATION->IncludeComponent(
	"project:infrastructure.detail", 
	"",
	array(
		"IBLOCK_TYPE" => "provider",
		"IBLOCK_CODE" => "infrastructure",
		"CACHE_TIME" => "3600",
		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		"SHOW_SECTION_HEAD" => true,
		"CACHE_TYPE" => "N",
		"SORT_FIELD1" => "SORT",
		"SORT_DIRECTION1" => "ASC",
		"SORT_FIELD2" => "ID",
		"SORT_DIRECTION2" => "ASC",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_ID" => "infrastructure",
		"IBLOCK_CITY_TYPE" => "0",
		"IBLOCK_CITY_ID" => "0",
		"COUNT" => "0",
		"SHOW_NAV" => "Y"
	),
	false
);

?>