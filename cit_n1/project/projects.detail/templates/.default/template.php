<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (!empty($arResult['ITEM'])): ?>
    <div class="grid-two-columns">
        <div class="grid-two-columns__left">
            <h3 class="text-title grid-two-columns__content">
                <?= $arResult['ITEM']['PROPERTY_MAIN_HEAD'] ?>
            </h3>
            <p class="text-regular grid-two-columns__content">
                <?= $arResult['ITEM']['PROPERTY_SUB_HEAD'] ?>
            </p>
        </div>
        <div class="grid-two-columns__right">
            <h3 class="text-title">
                Состав работ:
            </h3>
            <ul class="regular-list">
                <?php foreach ($arResult['ITEM']['PROPERTY_WORK_FLOW'] as $prop): ?>
                    <li class="text-regular text-grey"><?= $prop ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>