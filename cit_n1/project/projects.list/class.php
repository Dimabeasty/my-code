<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class ProjectsListComponent extends StandardElementListComponent {

    public function getResult()
    {
        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DETAIL_TEXT',
            'DETAIL_PAGE_URL',
            'PREVIEW_TEXT',
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);

        while ($element = $iterator->getNext()) {
            $this->arResult['ITEMS'][] = [
                "ID" => $element['ID'],
                "NAME" => $element['NAME'],
                "DETAIL_TEXT" => $element['DETAIL_TEXT'],
                "DETAIL_PAGE_URL" => $element['DETAIL_PAGE_URL'],
                "IBLOCK_SECTION_ID" => $element['IBLOCK_SECTION_ID'],
                "PREVIEW_TEXT" => $element['PREVIEW_TEXT'],
            ];
        }
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
            "ACTIVE" => 'Y',
        ];
    }
}
