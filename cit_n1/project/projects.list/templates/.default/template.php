<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

?>

<?php if (!empty($arResult['ITEMS'])): ?>
    <div class="grid-two-columns">
        <?php
        $left_column = [];
        $right_column = [];
        foreach ($arResult['ITEMS'] as $key => $item):
            if ($key % 2 == 0) {
                array_push($right_column, $item);
            } else {
                array_push($left_column, $item);
            }
        endforeach;
        ?>

        <div class="grid-two-columns__left projects__grid-element" style="gap: 40px;">
            <?php
            foreach ($left_column as $item):
            ?>

            <div class="projects__grid-element">
            <h3 class="text-title text-dark">
                <?= $item['NAME']; ?>
            </h3>
            <p class="text-regular text-grey">
                <?= $item['PREVIEW_TEXT']; ?>
            </p>
            <a class="text-link text-link--with-icon" href="<?= $item['DETAIL_PAGE_URL'] ?>">
                Узнать подробнее
                <svg width="24" height="24">
                    <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#arrow"></use>
                </svg>
            </a>
            </div>

            <?php endforeach; ?>
        </div>

        <div class="grid-two-columns__right projects__grid-element" style="gap: 40px;">
            <?php
            foreach ($right_column as $item):
                ?>

            <div class="projects__grid-element">
                <h3 class="text-title text-dark">
                    <?= $item['NAME']; ?>
                </h3>
                <p class="text-regular text-grey">
                    <?= $item['PREVIEW_TEXT']; ?>
                </p>
                <a class="text-link text-link--with-icon" href="<?= $item['DETAIL_PAGE_URL'] ?>">
                    Узнать подробнее
                    <svg width="24" height="24">
                        <use href="<?= MARKUP_PATH ?>/images/icons/sprite.svg#arrow"></use>
                    </svg>
                </a>
            </div>

            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>





