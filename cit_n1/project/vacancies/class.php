<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;

class VacanciesListComponent extends StandardElementListComponent {

    public function getResult()
    {
        $this->prepareOrder();

        $this->prepareFilter();

        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'PROPERTY_MONEY',
            'PROPERTY_REQUIREMENTS',
            'PROPERTY_RESPONS',
        ];
        $iterator = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams, $select);
        $props = [];
        $a = 1;
        while ($element = $iterator->getNext()) {
            $this->arResult['ITEMS'][] = [
                "ID" => $element['ID'],
                "NUMBER" => $a,
                "NAME" => $element['NAME'],
                "MONEY" => $element['PROPERTY_MONEY_VALUE'],
                "REQUIREMENTS" => $element['PROPERTY_REQUIREMENTS_VALUE'],
                "PROPS" => $element['PROPERTY_RESPONS_VALUE'],
            ];
            $a++;
        }
    }

    function prepareOrder(){
        $this->order = [
            $this->arParams["SORT_FIELD1"] => $this->arParams["SORT_DIRECTION1"],
            $this->arParams["SORT_FIELD2"] => $this->arParams["SORT_DIRECTION2"]
        ];
    }

    function prepareFilter(){
        $this->filter = [
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
            "ACTIVE" => 'Y',
        ];
    }
}
