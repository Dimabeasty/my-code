<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<?php if (!empty($arResult['ITEMS'])): ?>
    <div class="vacancies-page__inner">
        <ul class="accordion vacancies-page__accordion">
            <?php
            foreach ($arResult['ITEMS'] as $item):
                ?>
                <li class="accordion__item">
                    <header class="accordion__header">
                        <span class="accordion__number">/0<?= $item['NUMBER'] ?></span>
                        <h3 class="accordion__title"><?= $item['NAME'] ?></h3>
                    </header>
                    <div class="accordion__content">

                        <div class="vacancies-page__accordion-content-wrapper">
                            <div class="vacancies-page__accordion-content">
                                <h4 class="vacancies-page__accordion-title">Требования</h4>
                                <p class="text-regular text-grey mb-24"><?= $item['REQUIREMENTS'] ?></p>
                                <h4 class="vacancies-page__accordion-title">Должностные обязанности</h4>
                                <ul class="regular-list">
                                    <?php
                                    foreach ($item['PROPS'] as $prop):
                                        ?>
                                        <li class="text-regular text-grey"><?= $prop ?>
                                        </li>
                                    <?php
                                    endforeach;
                                    ?>

                                </ul>
                            </div>
                            <div class="vacancies-page__accordion-content">
                                <p class="vacancies-page__offer">зарплата от</p>
                                <p class="vacancies-page__offer-price"><?= $item['MONEY'] ?> ₽</p>
                                <a href="mailto:m.korablina@cit.krasnodar.ru"
                                   class="vacancies-page__button button button--primary">Откликнуться
                                    на вакансию</a>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>




