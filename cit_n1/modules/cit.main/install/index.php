<?php
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Entity\Base;

Loc::loadLanguageFile(__FILE__);

/**
 * Class cit_main
 */
class cit_main extends CModule
{
    public $MODULE_ID = "cit.main";

    public $MODULE_NAME;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public $MODULE_GROUP_RIGHTS = 'Y';

    public function __construct()
    {
        /**
         * @global array $arModuleVersion
         */
        include(__DIR__ . '/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('MAIN_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MAIN_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('MAIN_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('MAIN_PARTNER_URI');
    }

    /**
     * Module installation function
     */
    public function DoInstall()
    {
        Application::getConnection()->startTransaction();

        try {
            RegisterModule($this->MODULE_ID);
            Application::getConnection()->commitTransaction();
        }
        catch (Exception $ex)
        {
            Application::getConnection()->rollbackTransaction();
            throw $ex;
        }

    }

    /**
     * Module uninstallation function
     */
    public function DoUninstall()
    {
        if (!check_bitrix_sessid())
            return false;

        UnRegisterModule($this->MODULE_ID);
    }
}
