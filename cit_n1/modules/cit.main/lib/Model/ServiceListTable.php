<?php

namespace Cit\Main\Model;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Query\Query;

class ServiceListTable extends DataManager implements Model
{
    public static function getTableName()
    {
        return 'service_list';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\TextField(
                'UF_NAME',
                []
            ),
            new Entity\TextField(
                'UF_CODE',
                []
            ),
        ];
    }

    const SELECT_FIELDS = [
        'ID',
        'UF_NAME',
        'UF_CODE',
    ];

    protected array $selectFields = self::SELECT_FIELDS;

    public function setSelect(array $ar, bool $merge = true): Model
    {
        if($merge === true)
        {
            $this->selectFields = array_merge($this->selectFields, $ar);
        }else{
            $this->selectFields = $ar;
        }

        return $this;
    }

    public function queryObject(): Query
    {
        return parent::query()
            ->setSelect(self::SELECT_FIELDS);
    }

    /**
     * @throws \Exception
     */
    public function dataObjectBuilder(array $codes = []): Query
    {
        return $this->queryObject()
            ->whereIn('UF_CODE', $codes);
    }
}