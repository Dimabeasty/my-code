<?php

namespace Cit\Main\Model;
use Bitrix\Highloadblock\HighloadBlock;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Engine\Response;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\Web\Json;

class BaseHlBlockRepository
{
    public const HL_CODE = '';
    /**
     * @var DataManager $dataClass
     */
    protected $dataClass;
    /**
     * @var string
     */
    protected $hlEntityId;
    public function __construct()
    {
        Loader::includeModule('highloadblock');

        $hlId = HighloadBlockTable::getRow(
            [
                'filter' => [
                    'NAME' => self::HL_CODE
                ],
                'select' => ['ID']
            ]
        )['ID'];

        $this->dataClass = HighloadBlock::wakeUp($hlId)->getEntityDataClass();
        $this->hlEntityId = HighloadBlockTable::compileEntityId($hlId);
    }

    /**
     * @return string
     */
    public function getHlEntityId()
    {
        return $this->hlEntityId;
    }

    public function addNewUserInfo($data){

    }
}