<?php

namespace Cit\Main\Model;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Query\Query;

class OsTypesTable extends DataManager implements Model
{
    public static function getTableName()
    {
        return 'os_types';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\TextField(
                'UF_NAME',
                []
            ),
            new Entity\IntegerField(
                'UF_IMAGE',
                []
            ),
        ];
    }

    const SELECT_FIELDS = [
        'ID',
        'UF_NAME',
        'UF_IMAGE',
    ];

    protected array $selectFields = self::SELECT_FIELDS;

    public function setSelect(array $ar, bool $merge = true): Model
    {
        if($merge === true)
        {
            $this->selectFields = array_merge($this->selectFields, $ar);
        }else{
            $this->selectFields = $ar;
        }

        return $this;
    }

    public function queryObject(): Query
    {
        return parent::query()
            ->setSelect(self::SELECT_FIELDS);
    }

    /**
     * @throws \Exception
     */
    public function dataObjectBuilder(array $codes = []): Query
    {
        return $this->queryObject()
            ->whereIn('UF_CODE', $codes);
    }
}