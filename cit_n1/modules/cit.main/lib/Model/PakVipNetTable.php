<?php

namespace Cit\Main\Model;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Query\Query,
    Cit\Main\Service\Helpers;

class PakVipNetTable extends DataManager implements Model
{
    public static function getTableName()
    {
        return 'pak_vip_net';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\IntegerField(
                'UF_SERVICE_ID',
                []
            ),
            new Entity\IntegerField(
                'UF_COUNT',
                []
            ),
            new Entity\IntegerField(
                'UF_PAK',
                []
            ),
        ];
    }

    const SELECT_FIELDS = [
        'ID',
        'UF_SERVICE_ID',
        'UF_COUNT',
        'UF_PAK',
    ];

    protected array $selectFields = self::SELECT_FIELDS;

    public function setSelect(array $ar, bool $merge = true): Model
    {
        if($merge === true)
        {
            $this->selectFields = array_merge($this->selectFields, $ar);
        }else{
            $this->selectFields = $ar;
        }

        return $this;
    }

    public function queryObject(): Query
    {
        return parent::query()
            ->setSelect(self::SELECT_FIELDS);
    }


    public function dataObjectBuilder(array $serviceIds = []): Query
    {
        return $this->queryObject()
            ->whereIn('UF_SERVICE_ID', $serviceIds);
    }

    public function createElement(int $serviceId, array $formData)
    {
        $arAddParams = $this->prepareElementArray($serviceId, $formData);

        $result = self::add($arAddParams);

        if (!$result->isSuccess())
        {
            $errors = $result->getErrorMessages();
            throw new \Exception(implode("\n", $errors));
        }

        return $result->getId();
    }

    function prepareElementArray(int $serviceId, array $formData): array
    {
        $arResult = [];

        $arResult['UF_SERVICE_ID'] = $serviceId;

        $arResult['UF_PAK'] = $formData['pak_id'];

        $arResult['UF_COUNT'] = (new Helpers)->prepareString($formData['count']?:'');

        return $arResult;
    }

    public function deleteElement(int $elementId)
    {
        self::delete($elementId);
    }
}