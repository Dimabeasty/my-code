<?php

namespace Cit\Main\Model;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Query\Query;

class BUserFieldTable extends DataManager implements Model
{
    public static function getTableName()
    {
        return 'b_user_field';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                ]
            ),
            new Entity\StringField(
                'ENTITY_ID',
                []
            ),
            new Entity\StringField(
                'FIELD_NAME',
                []
            ),
            new Entity\StringField(
                'USER_TYPE_ID',
                []
            ),
            new Entity\StringField(
                'XML_ID',
                []
            ),
            new Entity\IntegerField(
                'SORT',
                []
            ),
        ];
    }

    public const SELECT_FIELDS = [
        'ID',
        'ENTITY_ID',
        'FIELD_NAME',
        'USER_TYPE_ID',
        'XML_ID',
        'SORT',
    ];

protected array $selectFields = self::SELECT_FIELDS;

    public function setSelect(array $ar, bool $merge = true): self
    {
        // TODO: Implement setSelect() method.
    }

    public function queryObject(): Query
    {
        return parent::query()
            ->setSelect($this->selectFields);
    }

    public function dataObjectBuilder(array $ids = []): Query
    {
        return $this->queryObject()
            ->whereIn('ID', $ids);
    }
}