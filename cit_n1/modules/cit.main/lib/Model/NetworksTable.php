<?php

namespace Cit\Main\Model;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Query\Query,
    Cit\Main\Service\Helpers;

class NetworksTable extends DataManager implements Model
{
    public static function getTableName()
    {
        return 'networks';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\IntegerField(
                'UF_SERVICE_ID',
                []
            ),
            new Entity\TextField(
                'UF_DESCRIPTION',
                []
            ),
            new Entity\IntegerField(
                'UF_NETWORK_LIST',
                []
            ),
        ];
    }

    const SELECT_FIELDS = [
        'ID',
        'UF_SERVICE_ID',
        'UF_DESCRIPTION',
        'UF_NETWORK_LIST',
    ];

    protected array $selectFields = self::SELECT_FIELDS;

    public function setSelect(array $ar, bool $merge = true): Model
    {
        if($merge === true)
        {
            $this->selectFields = array_merge($this->selectFields, $ar);
        }else{
            $this->selectFields = $ar;
        }

        return $this;
    }

    public function queryObject(): Query
    {
        return parent::query()
            ->setSelect(self::SELECT_FIELDS);
    }


    public function dataObjectBuilder(array $serviceIds = []): Query
    {
        return $this->queryObject()
            ->whereIn('UF_SERVICE_ID', $serviceIds);
    }

    public function createElement(int $serviceId, array $formData)
    {
        $arAddParams = $this->prepareElementArray($serviceId, $formData);

        $result = self::add($arAddParams);

        if (!$result->isSuccess())
        {
            $errors = $result->getErrorMessages();
            throw new \Exception(implode("\n", $errors));
        }

        return $result->getId();
    }

    function prepareElementArray(int $serviceId, array $formData): array
    {
        $arResult = [];

        $arResult['UF_SERVICE_ID'] = $serviceId;

        $arResult['UF_NETWORK_LIST'] = $formData['network_id'];

        $arResult['UF_DESCRIPTION'] = (new Helpers)->prepareString($formData['description']?:'');

        return $arResult;
    }

    public function deleteElement(int $elementId)
    {
        self::delete($elementId);
    }
}