<?php

namespace Cit\Main\Model;

use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Iblock\PropertyTable;

class PropertyEnum
{
    public static function getPropertyIdByCode(int $iblockId, string $code): ?int
    {
        return PropertyTable::query()
            ->where('IBLOCK_ID', $iblockId)
            ->where('CODE', $code)
            ->setSelect(['*'])
            ->fetch()['ID'];
    }

    public static function getListById(?int $id, array $arOrder = []): array
    {
        return PropertyEnumerationTable::query()
            ->where('PROPERTY_ID', $id)
            ->setSelect(['*'])
            ->setOrder($arOrder)
            ->fetchAll();
    }

    public static function getElementByXmlId(int $id, $xmlId): ?array
    {
        return PropertyEnumerationTable::query()
            ->where('PROPERTY_ID', $id)
            ->where('XML_ID', $xmlId)
            ->setSelect(['*'])
            ->fetch();
    }

    function prepareListProperty($arItems, $xmlKey = false, $xmlValue = false, $returnId = false): array
    {
        $arResult = [];
        if(!empty($arItems)){
            foreach ($arItems as $item) {
                $value = $xmlValue?$item['XML_ID']:$item['VALUE'];
                if($returnId)
                {
                    $value = $item['ID'];
                }
                $arResult[$xmlKey?$item['XML_ID']:$item['ID']] = $value;
            }
        }
        return $arResult;
    }
}