<?php

namespace Cit\Main\Model;

use Bitrix\Iblock\Iblock;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Query\Query;

abstract class BaseIblockRepository implements Model
{
    public const IBLOCK_CODE = '';

    public const SELECT_FIELDS = ['*'];

    protected array $selectFields = self::SELECT_FIELDS;

    protected string $dataClass;

    public function __construct()
    {
        if(!Loader::includeModule('iblock')) {
            throw new \Exception('Не удалось подключить модуль инфоблоков');
        }

        $iblockId = self::getIblockId();

        $this->dataClass = Iblock::wakeUp($iblockId)->getEntityDataClass();
    }

    public function getIblockId(): int
    {
        if(!Loader::includeModule('iblock')) {
            throw new \Exception('Не удалось подключить модуль инфоблоков');
        }

        return IblockTable::getRow([
            'filter' => [
                'CODE' => static::IBLOCK_CODE,
            ],
            'select' => [
                'ID'
            ]
        ])['ID'];
    }

    public function setSelect(array $ar, bool $merge = true): self
    {
        if($merge === true)
        {
            $this->selectFields = array_merge($this->selectFields, $ar);
        }else{
            $this->selectFields = $ar;
        }

        return $this;
    }

    public function queryObject(): Query
    {
        return $this->dataClass::query()
            ->setSelect(
                $this->selectFields
            );
    }

    abstract public function dataObjectBuilder(): Query;
}