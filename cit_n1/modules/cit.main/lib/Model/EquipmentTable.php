<?php

namespace Cit\Main\Model;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Query\Query;

class EquipmentTable extends DataManager implements Model
{
    public static function getTableName()
    {
        return 'equipment';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\IntegerField(
                'UF_SERVICE_ID',
                []
            ),
            new Entity\IntegerField(
                'UF_EQUIPMENT',
                []
            ),
            new Entity\IntegerField(
                'UF_COUNT',
                []
            ),
            new Entity\IntegerField(
                'UF_INSTALLATION_CONFIGURATION',
                []
            ),
            new Entity\IntegerField(
                'UF_PAC',
                []
            ),
            new Entity\IntegerField(
                'UF_VIPNET_ADMINISTRATOR',
                []
            ),
        ];
    }

    const SELECT_FIELDS = [
        'ID',
        'UF_SERVICE_ID',
        'UF_EQUIPMENT',
        'UF_COUNT',
        'UF_INSTALLATION_CONFIGURATION',
        'UF_PAC',
        'UF_VIPNET_ADMINISTRATOR',
    ];

    protected array $selectFields = self::SELECT_FIELDS;

    public function setSelect(array $ar, bool $merge = true): Model
    {
        if($merge === true)
        {
            $this->selectFields = array_merge($this->selectFields, $ar);
        }else{
            $this->selectFields = $ar;
        }

        return $this;
    }

    public function queryObject(): Query
    {
        return parent::query()
            ->setSelect(self::SELECT_FIELDS);
    }


    public function dataObjectBuilder(array $serviceIds = []): Query
    {
        return $this->queryObject()
            ->whereIn('UF_SERVICE_ID', $serviceIds);
    }

    public function createElement(int $serviceId, array $formData)
    {
        $arAddParams = $this->prepareElementArray($serviceId, $formData);

        $result = self::add($arAddParams);

        if (!$result->isSuccess())
        {
            $errors = $result->getErrorMessages();
            throw new \Exception(implode("\n", $errors));
        }

        return $result->getId();
    }

    function prepareElementArray(int $serviceId, array $formData): array
    {
        $arResult = [];

        $arResult['UF_SERVICE_ID'] = $serviceId;

        $arResult['UF_EQUIPMENT'] = $formData['equipment_id'];

        $arResult['UF_COUNT'] = $formData['count'];

        $arResult['UF_INSTALLATION_CONFIGURATION'] = (bool) $formData['installation_configuration']?true:false;

        $arResult['UF_PAC'] = (bool) $formData['pac']?true:false;

        $arResult['UF_VIPNET_ADMINISTRATOR'] = (bool) $formData['vipnet_administrator']?true:false;

        return $arResult;
    }

    public function deleteElement(int $elementId)
    {
        self::delete($elementId);
    }
}