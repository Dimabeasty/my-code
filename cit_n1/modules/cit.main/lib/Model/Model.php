<?php

namespace Cit\Main\Model;
use \Bitrix\Main\ORM\Query\Query;

interface Model
{
    //Стандартный набор полей для выборок
    public const SELECT_FIELD = [];

    /**
     * Функция для переопределения переменной с изначально стандартным набором полей для выборок
     * @param array $ar массив полей для select
     * @param bool $merge при true должно дополнять поля
     * @return $this
     */
    public function setSelect(array $ar, bool $merge = true): self;

    /**
     * @return Query
     * Задаем в нем базовые настройки из переменных класса
     */
    public function queryObject(): Query;

    /**
     * @return Query
     * Делаем строгую типизацаю параметров, метод должен удовлетварять 90% выборок,
     * если передаем даты, то с типизацией Bitrix DateTime
     */
    public function dataObjectBuilder(): Query;
}