<?php

namespace Cit\Main\Model;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Query\Query;

class NodesTable extends DataManager implements Model
{
    public static function getTableName()
    {
        return 'nodes';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\TextField(
                'UF_NODE_ID',
                []
            ),
            new Entity\TextField(
                'UF_NET',
                []
            ),
            new Entity\IntegerField(
                'UF_STATUS',
                []
            ),
            new \Bitrix\Main\Entity\ReferenceField(
                'STATUS_PROP',
                \App\Orm\Model\BUserFieldEnumTable::class,
                ['=this.UF_STATUS' => 'ref.ID']
            ),
        ];
    }

    const SELECT_FIELDS = [
        'ID',
        'UF_NODE_ID',
        'UF_NET',
        'UF_STATUS',
        'STATUS_PROP_' => 'STATUS_PROP',
    ];

    protected array $selectFields = self::SELECT_FIELDS;

    public function setSelect(array $ar, bool $merge = true): Model
    {
        if($merge === true)
        {
            $this->selectFields = array_merge($this->selectFields, $ar);
        }else{
            $this->selectFields = $ar;
        }

        return $this;
    }

    public function queryObject(): Query
    {
        return parent::query()
            ->setSelect(self::SELECT_FIELDS);
    }

    /**
     * @throws \Exception
     */
    public function dataObjectBuilder(array $codes = []): Query
    {
        return $this->queryObject()
            ->whereIn('UF_CODE', $codes);
    }
}