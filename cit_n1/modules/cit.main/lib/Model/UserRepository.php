<?php

namespace Cit\Main\Model;

use \Bitrix\Main\UserTable;
use Bitrix\Main\ORM\Query\Query;
use Bitrix\Main\Entity\ExpressionField;

class UserRepository
{
    function getAllUsers(?array $arId = []): object
    {
        return \Bitrix\Main\UserTable::query()
            ->setSelect(['UF_*', '*'])
            ->whereIn('ID', $arId)
            ->fetchCollection();
    }

    function updateNotification(int $userId, string $notification)
    {
        $user = new \CUser;
        $user->Update($userId, [
            'UF_NOTIFICATION' => (bool) $notification?true:false
        ]);
    }
}