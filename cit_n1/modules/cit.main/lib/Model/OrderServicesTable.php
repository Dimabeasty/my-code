<?php

namespace Cit\Main\Model;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Query\Query,
    Cit\Main\Service\Helpers;

class OrderServicesTable extends DataManager implements Model
{
    public static function getTableName()
    {
        return 'order_services';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new Entity\IntegerField(
                'UF_ORDER_ID',
                []
            ),
            new Entity\IntegerField(
                'UF_SERVICE',
                []
            ),
            new Entity\DateField(
                'UF_DATE_CONTRACT',
                []
            ),
            new Entity\IntegerField(
                'UF_SCAN_FILE',
                []
            ),
            new Entity\TextField(
                'UF_ADD_JOBS_SUBNET',
                []
            ),
            new Entity\IntegerField(
                'UF_NEED_CD',
                []
            ),
            new Entity\IntegerField(
                'UF_FZ_TYPE',
                []
            ),
            new Entity\TextField(
                'UF_COMMENT',
                []
            ),
            new Entity\TextField(
                'UF_NODES',
                []
            ),
            new Entity\IntegerField(
                'UF_TYPE_INSTALATION',
                []
            ),
            new Entity\IntegerField(
                'UF_SUPPORT_CERTIFICATE',
                []
            ),
            new Entity\IntegerField(
                'UF_OPTION_SERVICE',
                []
            ),
            new Entity\TextField(
                'UF_OS',
                []
            ),
            new Entity\TextField(
                'UF_ANTIVIRUSES',
                []
            ),
            new Entity\TextField(
                'UF_CIPF',
                []
            ),
            new Entity\TextField(
                'UF_OFFICE_PROGRAMS',
                []
            ),
            new \Bitrix\Main\Entity\ReferenceField(
                'SERVICE_PROP',
                \Cit\Main\Model\ServiceListTable::class,
                ['=this.UF_SERVICE' => 'ref.ID']
            ),
            new \Bitrix\Main\Entity\ReferenceField(
                'FZ_TYPE_PROP',
                \App\Orm\Model\BUserFieldEnumTable::class,
                ['=this.UF_FZ_TYPE' => 'ref.ID']
            ),
        ];
    }

    const SELECT_FIELDS = [
        'ID',
        'UF_ORDER_ID',
        'UF_SERVICE',
        'UF_DATE_CONTRACT',
        'UF_SCAN_FILE',
        'UF_ADD_JOBS_SUBNET',
        'UF_NEED_CD',
        'UF_FZ_TYPE',
        'UF_COMMENT',
        'UF_NODES',
        'UF_TYPE_INSTALATION',
        'UF_SUPPORT_CERTIFICATE',
        'UF_OPTION_SERVICE',
        'SERVICE_PROP_' => 'SERVICE_PROP',
        'FZ_TYPE_PROP_' => 'FZ_TYPE_PROP',
        'UF_OS',
        'UF_ANTIVIRUSES',
        'UF_CIPF',
        'UF_OFFICE_PROGRAMS',
    ];

    protected array $selectFields = self::SELECT_FIELDS;

    public function setSelect(array $ar, bool $merge = true): Model
    {
        if($merge === true)
        {
            $this->selectFields = array_merge($this->selectFields, $ar);
        }else{
            $this->selectFields = $ar;
        }

        return $this;
    }

    public function queryObject(): Query
    {
        return parent::query()
            ->setSelect(self::SELECT_FIELDS);
    }

    /**
     * @throws \Exception
     */
    public function dataObjectBuilder(array $orderIds = [], array $ids = []): Query
    {
        return $this->queryObject()
            ->whereIn('UF_ORDER_ID', $orderIds)
            ->whereIn('ID', $ids);
    }


    public function createElement(array $formData)
    {
        $arAddParams = $this->prepareElementArray($formData);

        $result = self::add($arAddParams);

        if (!$result->isSuccess())
        {
            $errors = $result->getErrorMessages();
            throw new \Exception(implode("\n", $errors));
        }
        return $result->getId();
    }

    /**
     * @throws \Exception
     */
    public function updateElement(int $elementId, array $formData)
    {
        $arAddParams = $this->prepareElementArray($formData);

        $result = self::update($elementId, $arAddParams);

        if (!$result->isSuccess())
        {
            $errors = $result->getErrorMessages();
            throw new \Exception(implode("\n", $errors));
        }
        return $result->getId();
    }

    function prepareElementArray(array $formData): array
    {
        $arResult = [];

        if($formData['order_id'])
            $arResult['UF_ORDER_ID'] = (int) $formData['order_id'];

        $arResult['UF_SERVICE'] = (int) $formData['service'];

        $arResult['UF_DATE_CONTRACT'] = $formData['date_contract']?new \Bitrix\Main\Type\DateTime(date('d.m.Y', strtotime($formData['date_contract']))):'';

        $arResult['UF_SCAN_FILE'] = (int) $formData['file-scan-id'];

        $arResult['UF_ADD_JOBS_SUBNET'] = (new Helpers)->prepareString($formData['add_jobs_subnet']);

        $arResult['UF_NEED_CD'] = (bool) $formData['need-cd']?true:false;

        $arResult['UF_FZ_TYPE'] = (int) $formData['fz-type'];

        $arResult['UF_TYPE_INSTALATION'] = (int) $formData['type_instalation'];

        $arResult['UF_SUPPORT_CERTIFICATE'] = (int) $formData['support_certificate'];

        $arResult['UF_OPTION_SERVICE'] = (int) $formData['option_service'];

        $arResult['UF_OS_TYPE'] = (int) $formData['os-type'];

        $arResult['UF_COMMENT'] = (new Helpers)->prepareString($formData['comment']);

        $arResult['UF_NODES'] = $formData['node-id']?serialize($formData['node-id']):'';

        $arResult['UF_OS'] = $formData['os']?serialize($formData['os']):'';

        $arResult['UF_ANTIVIRUSES'] = $formData['antiviruses']?serialize($formData['antiviruses']):'';

        $arResult['UF_CIPF'] = $formData['cipf']?serialize($formData['cipf']):'';

        $arResult['UF_OFFICE_PROGRAMS'] = $formData['office_programs']?serialize($formData['office_programs']):'';

        return $arResult;
    }

    public function deleteElement(int $elementId)
    {
        self::delete($elementId);
    }
}