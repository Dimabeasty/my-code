<?php

namespace Cit\Main\Service;

use Cit\Main\Model\PoVipNetTable;

class PoVipNet
{
    protected PoVipNetTable $poVipNetTable;

    public function __construct()
    {
        $this->poVipNetTable = new PoVipNetTable();
    }

    /**
     * @throws \Exception
     */
    function getArElements(int $serviceId): array
    {
        $arResult = [];

        $poObjects = $this->poVipNetTable->dataObjectBuilder([$serviceId])->fetchCollection();

        foreach ($poObjects as $poObject)
        {
            $osType = $poObject->getUfOsList();
            $network = $poObject->getUfPoNetwork();

            $arResult[$osType][$network] = [
                'ID' => $poObject->getId(),
                'COUNT' => $poObject->getUfCount(),
            ];
        }

        return $arResult;
    }

    function createElements(int $serviceId, array $arElements = [])
    {
        if(!empty($arElements))
        {
            foreach ($arElements as $element)
            {
                if($element['os_type_id'])
                {
                    foreach ($element['network'] as $network)
                    {
                        if($network['network_id'] && $network['count'])
                        {
                            $this->poVipNetTable->createElement($serviceId, [
                                'os_type_id' => $element['os_type_id'],
                                'network_id' => $network['network_id'],
                                'count' => $network['count'],
                            ]);
                        }
                    }
                }
            }
        }
    }

    function deleteElements(int $serviceId)
    {
        $pakObjects = $this->poVipNetTable->dataObjectBuilder([$serviceId])->fetchCollection();

        foreach ($pakObjects as $pakObject)
        {
            $this->poVipNetTable->deleteElement($pakObject->getId());
        }
    }
}