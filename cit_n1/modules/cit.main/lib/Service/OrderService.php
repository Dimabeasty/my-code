<?php

namespace Cit\Main\Service;

use Cit\Main\Model\OrderServicesTable,
    Cit\Main\Model\OrdersRepository,
    Bitrix\Main\UserTable;

class OrderService
{
    protected OrderServicesTable $orderServicesTable;
    protected OrdersRepository $ordersRepository;

    public function __construct()
    {
        $this->orderServicesTable = new OrderServicesTable();
        $this->ordersRepository = new OrdersRepository();
    }

    /**
     * @throws \Exception
     */
    function getServiceElement(int $orderId, int $serviceId): array
    {
        $arResult = [];

        $arOrder = (new Orders)->getOrderElement($orderId);

        if($arOrder)
        {
            $serviceObject = $this->orderServicesTable->dataObjectBuilder([$orderId], [$serviceId])->fetchObject();

            $arResult = $this->prepareServiceArray($serviceObject);
        }

        return $arResult;
    }

    /**
     * @throws \Exception
     */
    function getArElements(array $orderIds): array
    {
        $arResult = [];

        $serviceObjects = $this->orderServicesTable->dataObjectBuilder($orderIds)->fetchCollection();

        foreach ($serviceObjects as $serviceObject)
        {
            $orderId = $serviceObject->getUfOrderId();

            $arResult[$orderId][] = $this->prepareServiceArray($serviceObject);
        }

        return $arResult;
    }

    function prepareServiceArray(object $serviceObject)
    {
        $serviceId = $serviceObject->getId();

        $fzType = [];

        if($serviceObject->get('FZ_TYPE_PROP'))
        {
            $fzType = [
                'ID' => $serviceObject->get('FZ_TYPE_PROP')->getId(),
                'NAME' => $serviceObject->get('FZ_TYPE_PROP')->getValue(),
            ];
        }

        $scanFile = [];
        if($serviceObject->getUfScanFile())
            $scanFile = prepareFile($serviceObject->getUfScanFile());

        $arNodes = [];
        if($serviceObject->getUfNodes())
            $arNodes = unserialize($serviceObject->getUfNodes());

        $arEquipments = (new Equipment)->getArElements($serviceId);

        $arNetworks = (new Network)->getArElements($serviceId);

        $arPakVipNet = (new PakVipNet)->getArElements($serviceId);

        $arPoVipNet = (new PoVipNet)->getArElements($serviceId);

        $arOs = [];
        if($serviceObject->getUfOs())
        {
            $arOs = unserialize($serviceObject->getUfOs());
        }

        $arAntiviruses = [];
        if($serviceObject->getUfAntiviruses())
        {
            $arAntiviruses = unserialize($serviceObject->getUfAntiviruses());
        }

        $arCipf = [];
        if($serviceObject->getUfCipf())
        {
            $arCipf = unserialize($serviceObject->getUfCipf());
        }

        $arOfficePrograms = [];
        if($serviceObject->getUfOfficePrograms())
        {
            $arOfficePrograms = unserialize($serviceObject->getUfOfficePrograms());
        }


        $arResult = [
            'ID' => $serviceId,
            'SERVICE' => [
                'ID' => $serviceObject->get('SERVICE_PROP')->getId(),
                'NAME' => $serviceObject->get('SERVICE_PROP')->getUfName(),
                'XML_ID' => $serviceObject->get('SERVICE_PROP')->getUfCode(),
            ],
            'NEED_CD' => $serviceObject->getUfNeedCd(),
            'SCAN_FILE' => $scanFile,
            'FZ_TYPE' => $fzType,
            'NODES' => $arNodes,
            'DATE_CONTRACT' => $serviceObject->getUfDateContract()?$serviceObject->getUfDateContract()->format("Y-m-d"):'',
            'ADD_JOBS_SUBNET' => $serviceObject->getUfAddJobsSubnet(),
            'COMMENT' => $serviceObject->getUfComment(),
            'TYPE_INSTALATION' => $serviceObject->getUfTypeInstalation(),
            'SUPPORT_CERTIFICATE' => $serviceObject->getUfSupportCertificate(),
            'OPTION_SERVICE' => $serviceObject->getUfOptionService(),
            'EQUIPMENTS' => $arEquipments,
            'NETWORKS' => $arNetworks,
            'PAK_VIP_NET' => $arPakVipNet,
            'PO_VIP_NET' => $arPoVipNet,
            'OS' => $arOs,
            'ANTIVIRUSES' => $arAntiviruses,
            'CIPF' => $arCipf,
            'OFFICE_PROGRAMS' => $arOfficePrograms,
        ];

        return $arResult;
    }


    function createElement(array $formData)
    {
        global $USER;
        $orderId = $formData['order_id'];

        $orderObject = $this->ordersRepository->dataObjectBuilder([$orderId], [$USER->getId()])->fetchObject();

        if($orderObject)
        {
            $serviceId = $this->orderServicesTable->createElement($formData);

            if(!empty($formData['equipment']))
                (new Equipment)->createElements($serviceId, $formData['equipment']);

            if(!empty($formData['network']))
                (new Network)->createElements($serviceId, $formData['network']);

            if(!empty($formData['pak']))
                (new PakVipNet)->createElements($serviceId, $formData['pak']);

        }else{
            (new Helpers)->setError('Элемент не найден', 422);
        }
    }

    function updateElement(int $serviceId, array $formData)
    {
        $check = $this->checkElementRightUser($serviceId);

        if($check)
        {
            $this->orderServicesTable->updateElement($serviceId, $formData);

            //при сохранении что бы не проверять данные просто удаляем всё что было и перезаписываем на новое
            (new Equipment)->deleteElements($serviceId);
            if($formData['equipment'])
                (new Equipment)->createElements($serviceId, $formData['equipment']);

            (new Network)->deleteElements($serviceId);
            if($formData['network'])
                (new Network)->createElements($serviceId, $formData['network']);

            (new PakVipNet)->deleteElements($serviceId);
            if($formData['pak'])
                (new PakVipNet)->createElements($serviceId, $formData['pak']);

            (new PoVipNet)->deleteElements($serviceId);
            if($formData['po_vip_net'])
                (new PoVipNet)->createElements($serviceId, $formData['po_vip_net']);

        }else{
            (new Helpers)->setError('Элемент не найден', 422);
        }
    }

    function deleteElement(int $elementId)
    {
        $check = $this->checkElementRightUser($elementId);

        if($check)
        {
            $this->orderServicesTable->deleteElement($elementId);
        }else{
            (new Helpers)->setError('Элемент не найден', 422);
        }
    }

    function checkElementRightUser(int $elementId)
    {
        $orderServiceObject = $this->orderServicesTable->dataObjectBuilder([], [$elementId], [])->fetchObject();

        if($orderServiceObject)
        {
            global $USER;
            $orderId = $orderServiceObject->getUfOrderId();

            $orderObject = $this->ordersRepository->dataObjectBuilder([$orderId], [$USER->getId()])->fetchObject();

            if($orderObject)
            {
                return true;
            }
        }
        return false;
    }
}