<?php

namespace Cit\Main\Service;

use Cit\Main\Model\ServiceListTable;

class Service
{
    protected ServiceListTable $serviceListTable;

    public function __construct()
    {
        $this->serviceListTable = new ServiceListTable();
    }

    /**
     * @throws \Exception
     */
    function getArElements(): array
    {
        $arResult = [];

        $serviceObjects = $this->serviceListTable->dataObjectBuilder([])->fetchCollection();

        foreach ($serviceObjects as $serviceObject)
        {
            $arResult[] = [
                'ID' => $serviceObject->getId(),
                'NAME' => $serviceObject->getUfName(),
                'CODE' => $serviceObject->getUfCode(),
            ];
        }

        return $arResult;
    }
}