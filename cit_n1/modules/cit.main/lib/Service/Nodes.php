<?php

namespace Cit\Main\Service;

use Cit\Main\Model\NodesTable;

class Nodes
{
    protected NodesTable $nodesTable;

    public function __construct()
    {
        $this->nodesTable = new NodesTable();
    }

    /**
     * @throws \Exception
     */
    function getArElements(): array
    {
        $arResult = [];

        $nodesObjects = $this->nodesTable->dataObjectBuilder([])->fetchCollection();

        foreach ($nodesObjects as $nodesObject)
        {
            $arResult[] = [
                'ID' => $nodesObject->getId(),
                'NAME' => $nodesObject->getUfNodeId(),
                'NET' => $nodesObject->getUfNet(),
                'STATUS' => $nodesObject->getUfStatus(),
                'STATUS_XML_ID' => $nodesObject->get('STATUS_PROP')->getXmlId(),
            ];
        }

        return $arResult;
    }
}