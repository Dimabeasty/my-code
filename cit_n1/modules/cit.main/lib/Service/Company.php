<?php

namespace Cit\Main\Service;

class Company
{
    function getCompanyByInn(string $inn)
    {
        $arCitCompany = $this->getCompanyInCit($inn);

        $arDaDataCompany = $this->getCompanyInDaData($inn);

        return array_merge($arCitCompany, $arDaDataCompany);
    }

    function getCompanyInCit(string $inn)
    {
        $arResult = [];

        $arParams = $this->getInterSoftSettings();

        $userPwd = $arParams['USER_NAME'] . ':' . $arParams['PASSWORD'];

        $companyRequest = self::sendCurl($arParams['HOST'] . $inn, $userPwd);

        if($companyRequest['result']['#value'])
        {
            $arResult = [
                'short_name' => $companyRequest['result']['#value']['Наименование'],
                'full_name' => $companyRequest['result']['#value']['НаименованиеПолное'],
                'inn' => $companyRequest['result']['#value']['ИНН'],
                'kpp' => $companyRequest['result']['#value']['КПП'],
                'kod' => $companyRequest['result']['#value']['Код'],
                'kod_po_okpo' => $companyRequest['result']['#value']['КодПоОКПО'],
                'management_name' => $companyRequest['result']['#value']['РуководительКонтрагента'],
            ];

            foreach ($companyRequest['result']['#value']['КонтактнаяИнформация'] as $value)
            {
                switch ($value['#value']['Вид']) {
                    case 'Юридический адрес':
                        $arResult['ur_address'] = $value['#value']['Представление'];
                        break;
                    case 'Фактический адрес':
                        $arResult['fact_address'] = $value['#value']['Представление'];
                        break;
                    case 'Телефон':
                        $arResult['tel_number'] = $value['#value']['Представление'];
                        break;
                }
            }
        }

        return $arResult;
    }

    function getCompanyInDaData(string $inn)
    {
        $arResult = [];

        $daDataResult = (new DadataService)->searchByInn($inn);

        if(!empty($daDataResult))
        {
            if($daDataResult['data']['name']['short']){
                $short_name = $daDataResult['data']['name']['short'];
            } else {
                $short_name = $daDataResult['data']['name']['short_with_opf'];
            }

            $arResult = [
                'inn' => $daDataResult['data']['inn'],
                'short_with_opf' => $daDataResult['data']['name']['short_with_opf'],
                'oktmo' => $daDataResult['data']['oktmo'],
                'okved' => $daDataResult['data']['okved'],
                'ogrn' => $daDataResult['data']['ogrn'],
                'kod_po_okpo' => $daDataResult['data']['okpo'],
                'kpp' => $daDataResult['data']['kpp'],
                'full_name' => $daDataResult['data']['name']['full_with_opf'],
                'short_name' => $short_name,
                'management_name' => $daDataResult['data']['management']['name'],
                'ur_address' => $daDataResult['data']['address']['unrestricted_value'],
            ];
        }

        return $arResult;
    }

    function getCompanyByBik(string $bik)
    {
        $arResult = [];

        $dadataResult = (new DadataService)->searchByBik($bik);

        if(!empty($dadataResult))
        {
            $arResult = [
                'name_bik' => $dadataResult['unrestricted_value'],
                'correspondent_account' => $dadataResult['data']['correspondent_account'],
                'checking_account' => '',
            ];
        }
        return $arResult;
    }

    function sendCurl(string $url, string $userpwd): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return [
            'code'   => $code,
            'result' => json_decode($response, true)
        ];
    }

    function getInterSoftSettings()
    {
        return [
            'USER_NAME' => getSettingValueByCode('INTER_SOFT_USER_NAME')['VALUE'],
            'PASSWORD' => getSettingValueByCode('INTER_SOFT_PASSWORD')['VALUE'],
            'HOST' => getSettingValueByCode('INTER_SOFT_HOST')['VALUE'],
        ];
    }

    function addNewCompany(array $form_data)
    {
        echo '<pre>Item '; print_r($arResult); echo '</pre>';die();
    }
}