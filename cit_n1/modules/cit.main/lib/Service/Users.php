<?php

namespace Cit\Main\Service;

use Cit\Main\Model\UserRepository;

class Users
{
    protected UserRepository $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    function getUsersInfo(array $userIds = [])
    {
        $arResult = [];

        $userObjects = $this->userRepository->getAllUsers($userIds);

        foreach ($userObjects as $userObject)
        {
            $arResult[$userObject->getId()] = [
                'ID' => $userObject->getId(),
                'LOGIN' => $userObject->getLogin(),
                'PASSWORD' => $userObject->getPassword(),
                'FULL_NAME' => $userObject->getLastName() . ' ' . $userObject->getName() . ' ' . $userObject->getSecondName(),
                'LAST_NAME' => $userObject->getLastName(),
                'NAME' => $userObject->getName(),
                'INITIALS_NAME' =>  self::createInitialsName($userObject),
                'POSITION' => $userObject->getWorkPosition(),
                'NOTIFICATION' => $userObject->get('UF_NOTIFICATION'),
            ];
        }

        return $arResult;
    }

    function createInitialsName(object $userObj): string
    {
        return $userObj->getLastName() . ' ' .
            mb_substr($userObj->getName(), 0,1) . '. ' .
            mb_substr($userObj->getSecondName(), 0,1).'.';
    }

    /**
     * @throws \Exception
     */
    function setNotification(string $notification)
    {
        global $USER;

        $this->userRepository->updateNotification($USER->getId(), $notification);
    }

    public static function addNewUser(string $email, string $password, string $confirm_password, string $name, string $last_name)
    {
        global $USER;

        $arResult = $USER->Register($email, $name, $last_name, $password, $confirm_password, $email);

        if($arResult['TYPE'] == 'ERROR')
        {

            (new Helpers)->setError($arResult['MESSAGE'], 422);
        }

        return [
            'back_url'=> '/personal_account/registration/?confirm=email'
        ];
    }
}