<?php

namespace Cit\Main\Service;

use Cit\Main\Model\OsTypesTable;

class OsTypes
{
    protected OsTypesTable $osTypesTable;

    public function __construct()
    {
        $this->osTypesTable = new OsTypesTable();
    }

    /**
     * @throws \Exception
     */
    function getArElements(): array
    {
        $arResult = [];

        $osTypeObjects = $this->osTypesTable->dataObjectBuilder([])->fetchCollection();

        foreach ($osTypeObjects as $osTypeObject)
        {
            $arResult[] = [
                'ID' => $osTypeObject->getId(),
                'NAME' => $osTypeObject->getUfName(),
                'IMG' => $osTypeObject->getUfImage()?\CFile::GetPath($osTypeObject->getUfImage()):'',
            ];
        }

        return $arResult;
    }
}