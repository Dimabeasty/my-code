<?php

namespace Cit\Main\Service;

use Cit\Main\Model\EquipmentTable;

class Equipment
{
    protected EquipmentTable $equipmentTable;

    public function __construct()
    {
        $this->equipmentTable = new EquipmentTable();
    }

    /**
     * @throws \Exception
     */
    function getArElements(int $serviceId): array
    {
        $arResult = [];

        $equipmentObjects = $this->equipmentTable->dataObjectBuilder([$serviceId])->fetchCollection();

        foreach ($equipmentObjects as $equipmentObject)
        {
            $equipmentId = $equipmentObject->getUfEquipment();

            $arResult[$equipmentId] = [
                'ID' => $equipmentObject->getId(),
                'EQUIPMENT' => $equipmentId,
                'COUNT' => $equipmentObject->getUfCount(),
                'INSTALLATION_CONFIGURATION' => (bool) $equipmentObject->getUfInstallationConfiguration(),
                'PAC' => (bool) $equipmentObject->getUfPac(),
                'VIPNET_ADMINISTRATOR' => (bool) $equipmentObject->getUfVipnetAdministrator(),
            ];
        }

        return $arResult;
    }

    function createElements(int $serviceId, array $arElements = [])
    {
        if(!empty($arElements))
        {
            foreach ($arElements as $element)
            {
                if($element['equipment_id'])
                {
                    $this->equipmentTable->createElement($serviceId, $element);
                }
            }
        }
    }

    function deleteElements(int $serviceId)
    {
        $equipmentObjects = $this->equipmentTable->dataObjectBuilder([$serviceId])->fetchCollection();

        foreach ($equipmentObjects as $equipmentObject)
        {
            $this->equipmentTable->deleteElement($equipmentObject->getId());
        }
    }
}