<?php

namespace Cit\Main\Service;

class DadataService
{
    const INN_SEARCH_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party';
    const BIK_SEARCH_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/bank';

    function searchByInn(int $inn): array
    {
        $arSendParams = [
            'query' => $inn,
        ];
        $arFindInfo =  self::sendCurl(self::INN_SEARCH_URL, $arSendParams);

        return current($arFindInfo['result']['suggestions'])?:[];
    }

    function searchByBik(string $bik): array
    {
        $arSendParams = [
            'query' => $bik,
        ];
        $arFindInfo =  self::sendCurl(self::BIK_SEARCH_URL, $arSendParams);

        return current($arFindInfo['result']['suggestions'])?:[];
    }

    public static function sendCurl(string $url, array $arData = []): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Token ' . getSettingValueByCode('DADATA_SERVICE_TOKEN')['VALUE']
        ));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arData));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return [
            'code'   => $code,
            'result' => json_decode($response, true)
        ];
    }
}