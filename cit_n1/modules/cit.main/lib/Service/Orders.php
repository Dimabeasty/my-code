<?php

namespace Cit\Main\Service;

use Cit\Main\Model\OrdersRepository,
    Bitrix\Main\UserTable;

class Orders
{
    protected OrdersRepository $ordersRepository;

    public function __construct()
    {
        $this->ordersRepository = new OrdersRepository();
    }

    function getOrderElement(int $orderId)
    {
        $arResult = [];

        global $USER;

        $orderObject = $this->ordersRepository->dataObjectBuilder([$orderId], [$USER->getId()])->fetchObject();

        if($orderObject)
        {
            $arResult = [
                'ID' => $orderObject->getId()
            ];

        }
        return $arResult;
    }

    /**
     * @throws \Exception
     */
    function createElement(): array
    {
        $elementId = $this->ordersRepository->createElement();

        return [
            'id' => $elementId,
            'url_redirect' => '/personal_account/orders/' . $elementId . '/'
        ];
    }

    /**
     * @throws \Exception
     */
    function sendElement(int $orderId)
    {
        $arOrder = $this->getOrderElement($orderId);
        if($arOrder)
        {
            $this->ordersRepository->sendElement($orderId);
        }else{
            (new Helpers)->setError('Элемент не найден', 422);
        }
    }

    /**
     * @throws \Exception
     */
    function deactivateElement(int $orderId)
    {
        $arOrder = $this->getOrderElement($orderId);

        if($arOrder)
        {
            $this->ordersRepository->deactivateElement($orderId);
        }else{
            (new Helpers)->setError('Элемент не найден', 422);
        }
    }
}