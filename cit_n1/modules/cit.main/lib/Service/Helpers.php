<?php

namespace Cit\Main\Service;

class Helpers
{
    function setError(string $errorStr, int $code)
    {
        \CHTTP::SetStatus($code);
        throw new \Exception($errorStr);
    }

    function prepareString(?string $string = null)
    {
        if($string)
        {
            $string = trim($string);
            $string = strip_tags($string);
            $string = htmlspecialchars($string);
        }

        return $string;
    }

}