<?php

namespace Cit\Main\Service;

class File
{
    function getFileArrayById(int $id): array
    {
        $arFile = \CFile::GetFileArray($id);
        if($arFile){
            $info = pathinfo($arFile['SRC']);
            return [
                'id' => (int) $arFile['ID'],
                'name' => $arFile['FILE_NAME'],
                'extension' => $info['extension'],
                'link' => $arFile['SRC'],
                'url' => $arFile['SRC'],
            ];
        }
        return [];
    }

    function saveFile(array $arFile): int
    {
        return (int) \CFile::SaveFile($arFile, "save_files");
    }

    function fileGetPathById(int $id)
    {
        return \CFile::getPath($id);
    }

    function deleteFileById(int $id): int
    {
        return (int) \CFile::Delete($id);
    }
}