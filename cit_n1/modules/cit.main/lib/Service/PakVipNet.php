<?php

namespace Cit\Main\Service;

use Cit\Main\Model\PakVipNetTable;

class PakVipNet
{
    protected PakVipNetTable $pakVipNetTable;

    public function __construct()
    {
        $this->pakVipNetTable = new PakVipNetTable();
    }

    /**
     * @throws \Exception
     */
    function getArElements(int $serviceId): array
    {
        $arResult = [];

        $pakObjects = $this->pakVipNetTable->dataObjectBuilder([$serviceId])->fetchCollection();

        foreach ($pakObjects as $pakObject)
        {
            $pakId = $pakObject->getUfPak();

            $arResult[$pakId] = [
                'ID' => $pakObject->getId(),
                'NETWORK_ID' => $pakId,
                'COUNT' => $pakObject->getUfCount(),
            ];
        }

        return $arResult;
    }

    function createElements(int $serviceId, array $arElements = [])
    {
        if(!empty($arElements))
        {
            foreach ($arElements as $element)
            {
                if($element['pak_id'])
                {
                    $this->pakVipNetTable->createElement($serviceId, $element);
                }
            }
        }
    }

    function deleteElements(int $serviceId)
    {
        $pakObjects = $this->pakVipNetTable->dataObjectBuilder([$serviceId])->fetchCollection();

        foreach ($pakObjects as $pakObject)
        {
            $this->pakVipNetTable->deleteElement($pakObject->getId());
        }
    }
}