<?php

namespace Cit\Main\Service;

use App\Orm\Model\BUserFieldEnumTable;

class UserFieldEnum
{
    protected BUserFieldEnumTable $bUserFieldEnumTable;

    public function __construct()
    {
        $this->bUserFieldEnumTable = new BUserFieldEnumTable();
    }

    /**
     * @throws \Exception
     */
    function getArElements(string $fieldName): array
    {
        $arResult = [];

        $fieldEnumList = $this->bUserFieldEnumTable->dataObjectBuilder([] , '', $fieldName)->fetchCollection();

        foreach ($fieldEnumList as $fieldEnum)
        {
            $arResult[] = [
                'ID' => $fieldEnum->getId(),
                'VALUE' => $fieldEnum->getValue(),
                'CODE' => $fieldEnum->getXmlId(),
            ];
        }

        return $arResult;
    }
}