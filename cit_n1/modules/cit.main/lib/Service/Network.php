<?php

namespace Cit\Main\Service;

use Cit\Main\Model\NetworksTable;

class Network
{
    protected NetworksTable $networksTable;

    public function __construct()
    {
        $this->networksTable = new NetworksTable();
    }

    /**
     * @throws \Exception
     */
    function getArElements(int $serviceId): array
    {
        $arResult = [];

        $networkObjects = $this->networksTable->dataObjectBuilder([$serviceId])->fetchCollection();

        foreach ($networkObjects as $networkObject)
        {
            $networkId = $networkObject->getUfNetworkList();

            $arResult[$networkId] = [
                'ID' => $networkObject->getId(),
                'NETWORK_ID' => $networkId,
                'DESCRIPTION' => $networkObject->getUfDescription(),
            ];
        }

        return $arResult;
    }

    function createElements(int $serviceId, array $arElements = [])
    {
        if(!empty($arElements))
        {
            foreach ($arElements as $element)
            {
                if($element['network_id'])
                {
                    $this->networksTable->createElement($serviceId, $element);
                }
            }
        }
    }

    function deleteElements(int $serviceId)
    {
        $equipmentObjects = $this->networksTable->dataObjectBuilder([$serviceId])->fetchCollection();

        foreach ($equipmentObjects as $equipmentObject)
        {
            $this->networksTable->deleteElement($equipmentObject->getId());
        }
    }
}