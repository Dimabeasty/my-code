<?php

namespace Cit\Main\Controller;

use Cit\Main\Service\Orders;

class OrdersController extends BaseController
{
    /**
     * @return array
     */
    public function configureActions():array
    {
        return [
            'createElement' => $this->getDefaultConfigureForPost(),
            'sendElement' => $this->getDefaultConfigureForPost(),
            'deactivateElement' => $this->getDefaultConfigureForPost(),
        ];
    }

    /**
     * @throws \Exception
     */
    public function createElementAction(): array
    {
        return (new Orders)->createElement();
    }

    /**
     * @throws \Exception
     */
    public function sendElementAction(int $order_id)
    {
        (new Orders)->sendElement($order_id);
    }

    /**
     * @throws \Exception
     */
    public function deactivateElementAction(int $order_id)
    {
        (new Orders)->deactivateElement($order_id);
    }
}