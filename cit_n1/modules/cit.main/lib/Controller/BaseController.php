<?php


namespace Cit\Main\Controller;

use Bitrix\Main\Engine\ActionFilter;

class BaseController extends \Bitrix\Main\Engine\Controller
{
    public function configureActions(): array
    {
        return [];
    }

    protected function getDefaultConfigureForGet(): array
    {
        return [
            'prefilters' => $this->getDefaultPreFilters(),
            'postfilters' => [],
        ];
    }

    protected function getDefaultConfigureForPost(): array
    {
        return [
            'prefilters' => [
                new ActionFilter\HttpMethod(
                    [ActionFilter\HttpMethod::METHOD_POST]
                ),
                new ActionFilter\Authentication(),
                new ActionFilter\ContentType([ActionFilter\ContentType::JSON]),
            ],
            '-prefilters' => [
                ActionFilter\Csrf::class,
            ],
            'postfilters' => [],
        ];
    }

    protected function getDefaultConfigureForPostWithoutAuth(): array
    {
        return [
            'prefilters' => [
                new ActionFilter\HttpMethod(
                    [ActionFilter\HttpMethod::METHOD_POST]
                ),
            ],
            '-prefilters' => [
                ActionFilter\Csrf::class,
            ],
            'postfilters' => [],
        ];
    }

    protected function getDefaultConfigureForPostSecond(): array
    {
        return [
            'prefilters' => [
                new ActionFilter\HttpMethod(
                    [ActionFilter\HttpMethod::METHOD_POST]
                ),
                new ActionFilter\Authentication(),
            ],
            '-prefilters' => [
                ActionFilter\Csrf::class,
            ],
            'postfilters' => [],
        ];
    }

    protected function getDefaultConfigureForPostFile(): array
    {
        return [
            'prefilters' => [
                new ActionFilter\HttpMethod(
                    [ActionFilter\HttpMethod::METHOD_POST]
                ),
                new ActionFilter\Authentication(),
            ],
            '-prefilters' => [
                ActionFilter\Csrf::class,
            ],
            'postfilters' => [],
        ];
    }

    protected function getDefaultPreFilters(): array
    {
        return [
            new ActionFilter\HttpMethod(
                [ActionFilter\HttpMethod::METHOD_GET]
            ),
            new ActionFilter\Authentication(),
        ];
    }
}