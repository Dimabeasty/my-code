<?php

namespace Cit\Main\Controller;

use Cit\Main\Service\Company;

class CompanyController extends BaseController
{
    /**
     * @return array
     */
    public function configureActions():array
    {
        return [
            'getCompanyByInn' => $this->getDefaultConfigureForGet(),
            'getCompanyByBik' => $this->getDefaultConfigureForGet(),
            'addNewCompany' => $this->getDefaultConfigureForPost(),
        ];
    }

    public function getCompanyByInnAction(string $inn): array
    {
        return (new Company)->getCompanyByInn($inn);
    }

    public function getCompanyByBikAction(string $bik): array
    {
        return (new Company)->getCompanyByBik($bik);
    }

    public function addNewCompanyAction(array $form_data): array
    {
        return (new Company)->addNewCompany($form_data);
    }
}