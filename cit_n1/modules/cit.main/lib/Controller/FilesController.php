<?php

namespace Cit\Main\Controller;

use Cit\Main\Service\File;
use Bitrix\Main\Error;

class FilesController extends BaseController
{
    public function configureActions(): array
    {
        return [
            'saveFile' => $this->getDefaultConfigureForPostFile(),
            'deleteFile' => $this->getDefaultConfigureForPost(),
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function saveFileAction(): array
    {
        $arFile = self::getRequestFile();
        if(empty($arFile)){
            $this->addError(new Error('Нет параметров для запроса', 404, ''));
        }

        $fileId = (new File)->saveFile($arFile);
        if(empty($fileId)){
            $this->addError(new Error('Не удалось сохранить файл', 404, ''));
        }

        return [
            'id' => $fileId,
            'url' => (new File)->fileGetPathById($fileId),
        ];
    }

    /**
     * @return int
     * @throws Exception
     */
    public function deleteFileAction(int $file_id): int
    {
        return (new File)->deleteFileById($file_id);
    }

    function getRequestFile(): array
    {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        return $request->getFileList()->get('file');
    }
}