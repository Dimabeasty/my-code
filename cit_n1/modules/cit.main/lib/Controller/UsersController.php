<?php

namespace Cit\Main\Controller;

use Cit\Main\Service\Users;

class UsersController extends BaseController
{
    /**
     * @return array
     */
    public function configureActions():array
    {
        return [
            'setNotification' => $this->getDefaultConfigureForPost(),
            'createUser' => $this->getDefaultConfigureForPostWithoutAuth(),
        ];
    }

    /**
     * @throws \Exception
     */
    public function setNotificationAction(string $notification)
    {
        (new Users)->setNotification($notification);
    }

    public function createUserAction(string $email, string $password, string $confirm_password, string $name, string $last_name): array
    {
        return (new Users)->addNewUser($email, $password, $confirm_password, $name, $last_name);
    }
}