<?php

namespace Cit\Main\Controller;

use Cit\Main\Service\OrderService;

class OrderServiceController extends BaseController
{
    /**
     * @return array
     */
    public function configureActions():array
    {
        return [
            'createElement' => $this->getDefaultConfigureForPostSecond(),
            'deleteElement' => $this->getDefaultConfigureForPost(),
        ];
    }

    /**
     * @throws \Exception
     */
    public function createElementAction(array $form)
    {
        if($form['service_element_id'])
        {
            (new OrderService)->updateElement($form['service_element_id'], $form);
        }else{

            (new OrderService)->createElement($form);
        }
    }

    /**
     * @throws \Exception
     */
    public function deleteElementAction(int $service_id)
    {
        (new OrderService)->deleteElement($service_id);
    }
}