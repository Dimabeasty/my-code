<?php
//include ('/home/bitrix/www/local/modules/sender.statistic/lib/apioptions.php');
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\SystemException;
use \Bitrix\Main\Loader;
use \Rover\Fadmin\Layout\Admin\Form;

if (!Loader::includeModule($mid)
    || !Loader::includeModule('rover.fadmin'))
    throw new SystemException('module "' . $mid . '" not found!');

Loc::loadMessages(__FILE__);
Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");

$form = new Form(Sender\Statistic\ApiOptions::getInstance());
$form->show();
?>
<script>
    const btn_sub = document.querySelector('#s1__fact_run');
    btn_sub.setAttribute('type', 'button');
    btn_sub.addEventListener('click', function (){
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "/local/modules/sender.statistic/end_points/run_script.php", true);
        xhttp.send();
    })
</script>




