<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
include("/home/bitrix/www/local/modules/sender.statistic/lib/SenderLog.php");
include("/home/bitrix/www/local/modules/sender.statistic/lib/senderstatistic.php");
use \Sender\Statistic\SenderStatistic as SC;
use \Sender\Statistic\SenderLog as SL;


$start_time = microtime( true );
$result = SC::send();
print_r($result);
$data = self::prepareLogMessage($result['STATUS'], $result['QUANTITY_IR'], $result['QUANTITY_LETTERS']);
$result = SL::addMessageToLog($start_time, $data);

print_r($result);


