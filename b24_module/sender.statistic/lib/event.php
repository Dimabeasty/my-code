<?php

namespace Sender\Statistic;

use Sender\Statistic\DBClass as DB;
use Sender\Statistic\SenderLog as SL;
use Sender\Statistic\SenderStatistic as SC;

class Event
{
    public static function changeSettings($arFields)
    {
        $start_time = microtime(true);
        $status = 'Настройки запуска изменены';
        $quantity_ir = '0';
        $quantity_letters = '0';
        $data = SC::prepareLogMessage($status, $quantity_ir, $quantity_letters);
        SL::addMessageToLog($start_time, $data);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/test_log.txt", date("Y-m-d H:i:s") . "; " . print_r($arFields, 1) . ";\n", FILE_APPEND);
    }
}