<?php

namespace Sender\Statistic;
use \Rover\Fadmin\Inputs\Text;
use \Rover\Fadmin\Inputs\Date;
use \Rover\Fadmin\Options\Settings;
use \Rover\Fadmin\Inputs\Checkbox;
use \Rover\Fadmin\Inputs\Submit;
use \Rover\Fadmin\Options;


class ApiOptions extends Options
{
    const MODULE_ID = 'sender.statistic';

    const OPTION__PRESET_COLOR = 'preset_color';
    const OPTION__TEXTAREA = 'input_textarea';

    /**
     * @param string $moduleId
     * @return static
     * @throws \Bitrix\Main\ArgumentNullException
     * @author Pavel Shulaev (https://rover-it.me)
     */
    public static function getInstance($moduleId = self::MODULE_ID)
    {
        return parent::getInstance(self::MODULE_ID);
    }

    /**
     * @return array
     * @author Pavel Shulaev (http://rover-it.me)
     */
    public function getConfig()
    {
        return array(
            'tabs' => self::getTabs(),
            'settings' => array(
                Settings::BOOL_CHECKBOX => true,
                Settings::USE_SORT => true,
                Settings::GROUP_RIGHTS => true,
            )
        );
    }

    /**
     * @return array
     * @author Pavel Shulaev (https://rover-it.me)
     */
    protected static function getTabs()
    {
        return [
            [
                'name' => 'first_tab',
                'label' => 'Основные настройки',
                'default' => 'настройки',
                'siteId' => 's1',
                'inputs' => [
                    [
                        'type' => Checkbox::getType(),
                        'name' => 'fact_not',
                        'multiple' => false,
                        'label' => 'Активировать выгрузку статистики',
                        'sort' => '10'
                    ],
                    [
                        'type' => Submit::getType(),
                        'name' => 'fact_run',
                        'multiple' => false,
                        'label' => 'Выгрузить статистику',
                        'sort' => '30'
                    ],
                    [
                        'type' => Date::getType(),
                        'name' => 'fact_date',
                        'multiple' => false,
                        'label' => 'Дата и время запуска агента',
                        'sort' => '30'
                    ],
                    [
                        'type' => Text::getType(),
                        'name' => 'postgre_ip',
                        'multiple' => false,
                        'label' => 'IP Postgre',
                        'sort' => '30'
                    ],
                    [
                        'type' => Text::getType(),
                        'name' => 'postgre_port',
                        'multiple' => false,
                        'label' => 'Порт Postgre',
                        'sort' => '30'
                    ],
                    [
                        'type' => Text::getType(),
                        'name' => 'postgre_login',
                        'multiple' => false,
                        'label' => 'Логин Postgre',
                        'sort' => '30'
                    ],
                    [
                        'type' => Text::getType(),
                        'name' => 'postgre_pass',
                        'multiple' => false,
                        'label' => 'Пароль Postgre',
                        'sort' => '30'
                    ],
                ],
            ],
        ];
    }

    /**
     * @param bool $reload
     * @return mixed
     * @throws \Bitrix\Main\SystemException
     * @author Pavel Shulaev (https://rover-it.me)
     */
    public function getTextareaValueS1($reload = false)
    {
        return $this->getNormalValue(self::OPTION__TEXTAREA, 's1', $reload);
    }

    /**
     * @param      $presetId
     * @param bool $reload
     * @return mixed
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\SystemException
     * @author Pavel Shulaev (https://rover-it.me)
     */
    public function getS1PresetColor($presetId, $reload = false)
    {
        return $this->getPresetValue(self::OPTION__PRESET_COLOR, $presetId, 's1', $reload);
    }
}