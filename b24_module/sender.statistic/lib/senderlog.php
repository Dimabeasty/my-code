<?php

namespace Sender\Statistic;

use Bitrix\Main\Loader;
Loader::includeModule("iblock");
class SenderLog
{
    public static function addMessageToLog($start_time, $data){
        GLOBAL $USER;
        if($USER->GetID() > 0){
            $user = $USER->GetID();
        } else {
            $user = 1;
        }
        $name = "Запуск скрипта от ".date('d.m.y');
        $el = new \CIBlockElement;
        $PROP = array();
        $PROP['FACT_RUN'] = $data['FACT_RUN'];
        $PROP['USER'] = $user;
        $PROP['TIME_END'] = self::getRunTime($start_time);
        $PROP['QUANTITY_IR'] = $data['QUANTITY_IR']; //из метода
        $PROP['QUANTITY_LETTERS'] = $data['QUANTITY_LETTERS'];  //из метода
        $PROP['STATUS'] = $data['STATUS'];
        $arLoadProductArray = Array(
            'MODIFIED_BY' => $user,
            'IBLOCK_SECTION_ID' => false,
            'IBLOCK_ID' => 71,
            'PROPERTY_VALUES' => $PROP,
            'NAME' => $name,
            'ACTIVE' => 'Y',
        );
        if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            echo 'New ID: '.$PRODUCT_ID;
        } else {
            echo 'Error: '.$el->LAST_ERROR;
        }
    }

    public static function getRunTime($start_time){
        $diff = sprintf( '%.6f sec.', microtime( true ) - $start_time );
        return $diff;
    }
}
