<?php


namespace Sender\Statistic;

use Bitrix\Main\Loader;
use Sender\Statistic\DBClass as DB;
use Sender\Statistic\SenderLog as SL;
Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;


class SenderStatistic
{
    public static function start()
    {
        $options = DB::getInfoFromTable();
        $start_time = microtime( true );
        if($options['fact_not']['VALUE'] == 'Y'){
            $result = self::send();
            $data = self::prepareLogMessage($result['STATUS'], $result['QUANTITY_IR'], $result['QUANTITY_LETTERS']);
            SL::addMessageToLog($start_time, $data);
        } else {
            $status = 'Скрипт не запущен';
            $quantity_ir = '0';
            $quantity_letters = '0';
            $data = self::prepareLogMessage($status, $quantity_ir, $quantity_letters);
            SL::addMessageToLog($start_time, $data);
        }
        return __METHOD__ . '();';
    }
    public static function send()
    {

        // соединение с базой данных
        $connection = \Bitrix\Main\Application::getConnection();



        $project_id = 14;
        $letter_sendings = [];

        $query2 = "SELECT `ID`, `PROJECT_ID`, `LETTER_TYPE_ID`, `LETTER_ID` FROM `pr_letter_additional_data` WHERE `PROJECT_ID` = '".$project_id."'";
        $recordset2 = $connection->query($query2);

        $ids = [];
        while ($postadd = $recordset2->fetch()) {
            $ids[] = $postadd["LETTER_ID"];
        }



        $query = "SELECT `ID`, `DATE_CREATE`, `DATE_SENT`, `STATUS`, `COUNT_SEND_ALL`, `COUNT_READ`, `COUNT_CLICK`, `COUNT_UNSUB`, `MAILING_CHAIN_ID` FROM `b_sender_posting`  WHERE `STATUS` = 'S' OR `STATUS` = 'E' ";
        $recordset = $connection->query($query);


        while ($posting = $recordset->fetch()) {

            if(!in_array($posting["MAILING_CHAIN_ID"], $ids)){
                continue;
            }
            //2023-10-19 21:30:54.238679+03

            $time = new \DateTime($posting["DATE_CREATE"]);
            $posting["DATE_CREATE"]= $time->format("Y-m-d H:i:s");

            $time2 = new \DateTime($posting["DATE_SENT"]);
            $posting["DATE_SENT"]= $time2->format("Y-m-d H:i:s");

            $query3 = "SELECT `ID`, `PROJECT_ID`, `LETTER_TYPE_ID`, `LETTER_ID` FROM `pr_letter_additional_data` WHERE `LETTER_ID` = '".$posting['MAILING_CHAIN_ID']."'";
            $recordset3 = $connection->query($query3);

            if ($postadd2 = $recordset3->fetch()) {


                $project = $postadd2["PROJECT_ID"];
                $type = $postadd2["LETTER_TYPE_ID"];
                if($project == $project_id){
                    $flag = true;
                    $entity = HL\HighloadBlockTable::compileEntity("LetterProjectList");
                    $entity_data_class = $entity->getDataClass();

                    $rsData = $entity_data_class::getList(array(
                        "select" => array("*"),
                        "order" => array("ID" => "ASC"),
                        "filter" => array("ID"=>$project)  // Задаем параметры фильтра выборки
                    ));

                    if($arData = $rsData->Fetch()){
                        $projname = $arData["UF_NAME"];
                    }

                    $posting["PROJECT_ID"] = $projname;


                    $entity = HL\HighloadBlockTable::compileEntity("LetterType");
                    $entity_data_class = $entity->getDataClass();

                    $rsData = $entity_data_class::getList(array(
                        "select" => array("*"),
                        "order" => array("ID" => "ASC"),
                        "filter" => array("ID"=>$type)  // Задаем параметры фильтра выборки
                    ));

                    if($arData = $rsData->Fetch()){
                        $typelet = $arData["UF_NAME"];
                    }

                    $posting["LETTER_TYPE_ID"] = $typelet;

                    $mailingchain = $posting["MAILING_CHAIN_ID"];

                    $query4 = "SELECT `ID`, `CREATED_BY`, `MESSAGE_ID`, `TEMPLATE_ID`, `TITLE`  FROM `b_sender_mailing_chain` WHERE `ID` = '".$mailingchain."'";
                    $recordset4 = $connection->query($query4);

                    if ($chain = $recordset4->fetch()) {
                        $message_id = $chain["MESSAGE_ID"];
                        $created_by = $chain["CREATED_BY"];
                        $template = $chain["TEMPLATE_ID"];
                        $title = $chain["TITLE"];

                        $rsUser = \CUser::GetByID($created_by);
                        $arUser = $rsUser->Fetch();

                        $posting["created_by"] = $arUser["NAME"]." ".$arUser["LAST_NAME"];
                        $posting["title"] = $title;

                        $query5 = "SELECT * FROM `b_sender_message_field` WHERE `MESSAGE_ID` = '".$message_id."'";
                        $recordset5 = $connection->query($query5);

                        while ($message = $recordset5->fetch()) {

                            if($message["CODE"] == "EMAIL_FROM"){
                                $email_from = htmlspecialchars($message["VALUE"]);
                                $posting["email_from"] = $email_from;
                            }
                            if($message["CODE"] == "SUBJECT"){
                                $subject = $message["VALUE"];
                                $posting["subject"] = $subject;
                            }
                            if($message["CODE"] == "MESSAGE"){
                                //$blob = self::string_to_blob($message["VALUE"]);
                                $blob = $message["VALUE"];
                                $posting["template"] = $blob;
                            }
                        }
                    }

                }
            }
            if($flag){
                $letter_sendings[] = $posting;
                unset($flag);
                unset($posting);
            }

        }


        $dbconn2 = pg_connect("host=172.33.2.98 port=5432 dbname=bitrix_mailing user=btr24_nlmstat password=oofax9Aivaigh5ej");
        $status_return = [];
        foreach ($letter_sendings as $let_send) {

            $id = $let_send["MAILING_CHAIN_ID"];
            $create_date = $let_send["DATE_CREATE"];
            $finish_date = $let_send["DATE_SENT"];
            $status = $let_send["STATUS"];
            $status_return[] = $let_send["STATUS"];
            $sent_amount = $let_send["COUNT_SEND_ALL"];
            $read_amount = $let_send["COUNT_READ"];
            $clicked_amount = $let_send["COUNT_CLICK"];
            $unsubscribed_amount = $let_send["COUNT_UNSUB"];
            $project = $let_send["PROJECT_ID"];
            $sm_type = $let_send["LETTER_TYPE_ID"];
            $mail_template = $let_send["template"];
            $author = $let_send["created_by"];
            $event_name = $let_send["title"];
            $sender_email = $let_send["email_from"];
            $mail_theme = $let_send["subject"];

            $query = "INSERT INTO letter_sendings(id, create_date, sender_email, event_name, author, finish_date, status, sent_amount, read_amount, clicked_amount, unsubscribed_amount, mail_theme, mail_template, project, sm_type) 
			 values ('$id','$create_date','$sender_email','$event_name','$author','$finish_date','$status','$sent_amount','$read_amount','$clicked_amount','$unsubscribed_amount','$mail_theme','$mail_template','$project','$sm_type')";
            if ($result = pg_send_query($dbconn2, $query)) {
                echo "Data Added Successfully.";
                $res1 = pg_get_result($dbconn2);
                echo pg_result_error($res1);
            } else {
                echo "Error.";
            }

        }

        // Обновление
        /*foreach ($letter_sendings as $let_send) {

            $id = $let_send["MAILING_CHAIN_ID"];
            $create_date = $let_send["DATE_CREATE"];
            $finish_date = $let_send["DATE_SENT"];
            $status = $let_send["STATUS"];
            $sent_amount = $let_send["COUNT_SEND_ALL"];
            $read_amount = $let_send["COUNT_READ"];
            $clicked_amount = $let_send["COUNT_CLICK"];
            $unsubscribed_amount = $let_send["COUNT_UNSUB"];
            $project = $let_send["PROJECT_ID"];
            $sm_type = $let_send["LETTER_TYPE_ID"];
            $mail_template = $let_send["template"];
            $author = $let_send["created_by"];
            $event_name = $let_send["title"];
            $sender_email = $let_send["email_from"];
            $mail_theme = $let_send["subject"];

            $query = "UPDATE letter_sendings
			SET
			create_date='" . $create_date . "',
			sender_email='" . $sender_email . "',
			event_name='" . $event_name . "',
			author='" . $author . "',
			finish_date='" . $finish_date . "',
			status='" . $status . "',
			sent_amount='" . $sent_amount . "',
			read_amount='" . $read_amount . "',
			clicked_amount='" . $clicked_amount . "',
			unsubscribed_amount='" . $unsubscribed_amount . "',
			mail_theme='" . $mail_theme . "',
			mail_template='" . $mail_template . "',
			project='" . $project . "',
			sm_type='" . $sm_type . "'
			WHERE id='" . $id . "'
			";
            if ($result = pg_send_query($dbconn2, $query)) {
                echo "Data Added Successfully.";
                $res1 = pg_get_result($dbconn2);
                echo pg_result_error($res1);
            } else {
                echo "Error.";
            }

        }*/



        $letters = [];
        foreach($letter_sendings as $letter_Send) {
            $query6 = "SELECT * FROM `b_sender_posting_recipient` WHERE `POSTING_ID` = '".$letter_Send["ID"]."'";
            $recordset6 = $connection->query($query6);

            $query7 = "SELECT * FROM `b_sender_posting_read` WHERE `POSTING_ID` = '".$letter_Send["ID"]."'";
            $recordset7 = $connection->query($query7);
            $recipientRead = [];
            while($posting_read = $recordset7->fetch()){
                $recipientRead[$posting_read["RECIPIENT_ID"]] = $posting_read["DATE_INSERT"]->format("Y-m-d H:i:s");
            }

            $query8 = "SELECT * FROM `b_sender_posting_click` WHERE `POSTING_ID` = '".$letter_Send["ID"]."'";
            $recordset8 = $connection->query($query8);
            $recipientClick = [];
            while($posting_click = $recordset8->fetch()){
                $recipientClick[$posting_click["RECIPIENT_ID"]] = $posting_click["DATE_INSERT"]->format("Y-m-d H:i:s");
            }

            $query9 = "SELECT * FROM `b_sender_posting_unsub` WHERE `POSTING_ID` = '".$letter_Send["ID"]."'";
            $recordset9 = $connection->query($query9);
            $recipientUnsub = [];
            if($posting_unsub = $recordset9->fetch()){
                $recipientUnsub[$posting_unsub["RECIPIENT_ID"]] = $posting_unsub["DATE_INSERT"]->format("Y-m-d H:i:s");
            }

            while ($recipient = $recordset6->fetch()) {
                $letter["id"] = $recipient["ID"];
                $letter["letter_sending_id"] = $letter_Send["MAILING_CHAIN_ID"];
                $letter["status"] = $recipient["STATUS"];
                $letter["send_date"] = $recipient["DATE_SENT"]->format("Y-m-d H:i:s");
                if($recipient["IS_READ"] == "Y"){
                    if(array_key_exists($recipient["ID"], $recipientRead)){
                        $letter["read_date"] = $recipientRead[$recipient["ID"]];
                    }
                } else {
                    continue;
                }
                if($recipient["IS_CLICK"] == "Y"){
                    if(array_key_exists($recipient["ID"], $recipientClick)){
                        $letter["clicked_date"] = $recipientClick[$recipient["ID"]];
                    }
                } else {
                    $letter["clicked_date"] = null;
                }
                if($recipient["IS_UNSUB"] == "Y"){
                    if(array_key_exists($recipient["ID"], $recipientUnsub)){
                        $letter["unsubscribed_date"] = $recipientUnsub[$recipient["ID"]];
                    }
                } else {
                    $letter["unsubscribed_date"] = null;
                }
                $query10 = "SELECT * FROM `b_sender_contact` WHERE `ID` = '".$recipient["CONTACT_ID"]."'";
                $recordset10 = $connection->query($query10);
                if($sender_contact = $recordset10->fetch()){
                    $letter["addressee_email"] = $sender_contact["CODE"];
                    $letter["addressee_name"] = $sender_contact["NAME"];
                }
                $letters[] = $letter;
                unset($letter);
            }
        }




        $dbconn2 = pg_connect("host=172.33.2.98 port=5432 dbname=bitrix_mailing user=btr24_nlmstat password=oofax9Aivaigh5ej");

        foreach ($letters as $letter) {

            $id = $letter["id"];
            $letter_sending_id = $letter["letter_sending_id"];
            $addressee_email = $letter["addressee_email"];
            $addressee_name = $letter["addressee_name"];
            $status = $letter["status"];
            $send_date = $letter["send_date"];
            $read_date = $letter["read_date"];
            $clicked_date = $letter["clicked_date"];
            $unsubscribed_date = $letter["unsubscribed_date"];
            if(empty($send_date)) {
                $send_date = NULL;
            }
            if(empty($read_date)) {
                $read_date = NULL;
            }
            if(empty($clicked_date)) {
                $clicked_date = NULL;
            }
            if(empty($unsubscribed_date)) {
                $unsubscribed_date = NULL;
            }


            $query = "INSERT INTO letters(id, letter_sending_id, addressee_email, addressee_name, status, send_date, read_date, clicked_date, unsubscribed_date) ";
            $query .= "values ('$id','$letter_sending_id','$addressee_email','$addressee_name','$status',";
            if(empty($send_date)){
                $query .= "NULL,";
            } else {
                $query .= "'$send_date',";
            }
            if(empty($read_date)){
                $query .= "NULL,";
            } else {
                $query .= "'$read_date',";
            }
            if(empty($clicked_date)){
                $query .= "NULL,";
            } else {
                $query .= "'$clicked_date',";
            }
            if(empty($unsubscribed_date)){
                $query .= "NULL)";
            } else {
                $query .= "'$unsubscribed_date')";
            }

            if ($result = pg_send_query($dbconn2, $query)) {
                echo "Data Added Successfully.";
                $res1 = pg_get_result($dbconn2);
                $pos = strpos(pg_result_error($res1), "duplicate key");

                if ($pos !== false) {
                    $query = "UPDATE letters
						SET
						letter_sending_id='" . $letter_sending_id . "',
						addressee_email='" . $addressee_email . "',
						addressee_name='" . $addressee_name . "',
						status='" . $status . "',";

                    if(empty($send_date)){
                        $query .= "send_date=NULL,";
                    } else {
                        $query .= "send_date='" . $send_date . "',";
                    }
                    if(empty($read_date)){
                        $query .= "read_date=NULL,";
                    } else {
                        $query .= "read_date='" . $read_date . "',";
                    }
                    if(empty($clicked_date)){
                        $query .= "clicked_date=NULL,";
                    } else {
                        $query .= "clicked_date='" . $clicked_date . "',";
                    }
                    if(empty($unsubscribed_date)){
                        $query .= "unsubscribed_date=NULL";
                    } else {
                        $query .= "unsubscribed_date='" . $unsubscribed_date . "'";
                    }
                    $query .= " WHERE id='" . $id . "'";

                    if ($result = pg_send_query($dbconn2, $query)) {
                        echo "Data Updated Successfully.";
                        $res1 = pg_get_result($dbconn2);
                        echo pg_result_error($res1);
                        echo "<br>";
                    } else {
                        echo "Error.";
                    }
                }
            } else {
                echo "Error.";
            }

        }
        $data_return = [];
        $data_return['QUANTITY_IR'] = count($letter_sendings);
        $data_return['QUANTITY_LETTERS'] = count($letters);
        $data_return['STATUS'] = json_encode($status_return);

        return $data_return;
    }

    public static function prepareLogMessage($status, $quantity_ir, $quantity_letters){
        $arr_res = DB::getInfoFromTable();
        $result = [];
        if($arr_res['fact_not']['VALUE'] == 'Y'){
            $res_run = 'Запуск скрипта разрешен';
        } elseif ($arr_res['fact_not']['VALUE'] == 'N'){
            $res_run = 'Запуск скрипта запрещен';
        }else {
            $res_run = 'Запуск скрипта по кнопке';
        }
        $result['FACT_RUN'] = $res_run;
        $result['STATUS'] = $status;
        $result['QUANTITY_IR'] = $quantity_ir;
        $result['QUANTITY_LETTERS'] = $quantity_letters;
        return $result;
    }
}