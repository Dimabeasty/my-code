<?php
namespace SenderStatistic\Api;

use SenderStatistic\Api\SenderStatisticClass as Sender;
class AgentStart
{
    public static function addNewAgent(){
        CAgent::AddAgent(
            Sender::start(), // имя функции
            "sender.statistic",
            "N",
            86400,
            self::getTimeStart(),
            "Y",
            self::getTimeStart(),
            );
    }

    public static function getTimeStart(){
        $result = date('d.m.Y').' 00:00:00';
        return $result;
    }

    public static function deleteAgent(){
        CAgent::RemoveAgent(
            Sender::start(),
            "sender.statistic",
        );
    }

    public static function addAgentUserTime($start_time){
        self::deleteAgent();
        CAgent::AddAgent(
            Sender::start(),
            "sender.statistic",
            "N",
            86400,
            $start_time,
            "Y",
            $start_time,
        );
    }
}